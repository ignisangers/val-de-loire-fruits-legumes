<div id="languages-list">
    <h1>Choisissez une langue de traduction :</h1>
    <ul class="clearfix">
        <?php
        $replace = array();
        foreach($languages as $code => $language):
            $replace[] = '/'. $code .'/';
        endforeach;
            
        foreach($languages as $code => $language):
            echo '<li>';
                $class = ($code == $current_language) ? 'active': '';
                $url = str_replace($replace, '/'.$code.'/', $url);
                echo $this->Html->link($this->Html->image('admin/flags/'.$code.'L.png', array('alt' => $language)), $url, array('escape' => false, 'class' => $class));
            echo '</li>';
        endforeach;
        ?>
    </ul>
    <?php echo $this->Html->link('x', '#', array('class' => 'fermer')) ?>
</div>