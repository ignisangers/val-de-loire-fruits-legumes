<div class="modal-header">
    <h3>Rognez votre image</h3>
</div>
<div class="modal-body">
    <?php echo $this->Form->create(null, array("class" => "modal-form")); ?>
        <div class="row-fluid">
            <div class="span2">
                <?php
                echo $this->Html->image("../files/produits/gallerie/" . $this->request->data["GallerieProduit"]["filename"], array(
                    "alt" => "",
                    "id" => "GalleryEditThumb",
                    "style" => "width: 100%;"
                ));
                ?>
            </div>
            <div class="span10">
                <?php
                echo $this->Form->hidden("GallerieProduit.id");
                echo $this->Form->hidden("GallerieProduit.filename", array("class" => "crop-hidden", "value" => ""));
//                echo $this->Form->input("GallerieRealisation.caption", array(
//                    "label" => "Légende de votre photo (facultatif)",
//                    "class" => "modal-body__input"
//                ));
                echo $this->Form->input("GallerieProduit.filechange", array(
                    "label" => "Changer de photo",
                    "type" => "file",
                    "class" => "crop-change"
                ));
                ?>
            </div>
        </div>
        <div class="crop-image"></div>
    <?php echo $this->Form->end(); ?>
</div>
<div class="modal-footer">
    <button class="button modal-submit--edit" data-type="gallerie_produits">Valider l'opération</button>
    <button class="button button--secondary" onclick="Ignis.Modal.close(); return false;">Annuler</button>
</div>