<?php
// Titrage + boutons d'actions + sous-nav
echo $this->start('header');
    echo '<h1>Gestion des utilisateurs</h1>';
    echo '<nav class="clearfix">';
        echo '<ul id="sous-nav" class="clearfix">';
            /*echo '<li>';
                echo $this->Html->link('Liste complète', array(
                    'action' => 'index'
                ), array('class' => 'current'));
            echo '</li>';*/
            echo '<li>';
                echo '<p>Utilisateurs de l\'espace d\'administration</p>';
            echo '</li>';
        echo '</ul>';
        echo '<ul id="nav-actions" class="clearfix">';
            echo '<li>';
                echo $this->Html->link('Ajouter un utilisateur', array(
                    'action' => 'add'
                ));
            echo '</li>';
        echo '</ul>';
    echo '</nav>';
echo $this->end();
?>

<h2>Liste des utilisateurs</h2>
<?php
echo $this->Session->flash();

$i = 1;
$closed = true;
foreach($users as $user):
    if($i == 1) {
        echo '<div class="row-fluid">';
        $closed = false;
    }
    
    echo '<div class="span3 user">';
        echo "<small>". $user['Role']['name'] ."</small>";
        echo '<figure>';
            if(!empty($user['User']['avatar'])) {
                echo $this->Html->image("avatars/".$user['User']['avatar'], array("alt" => "avatar"));
            }
        echo '</figure>';
        echo "<p> <span>" . $user['User']['prenom'] . " <strong>" . $user['User']['nom'] . "</strong></span> </p>";
        echo "<ul>";
            echo "<li>";
                echo $this->Html->link('Modifier', array(
                    'action' => 'edit',
                    $user['User']['id']
                ));
            echo "</li>";
            echo "<li>";
                echo $this->Html->link('Dupliquer', array(
                    'action' => 'duplicate',
                    $user['User']['id']
                ), array(), "Dupliquer cet utilisateur ?");
            echo "</li>";
            echo "<li>";
                echo $this->Html->link('x', array(
                    'action' => 'delete',
                    $user['User']['id']
                ), array(), "Supprimer cet utilisateur ?");
            echo "</li>";
        echo "</ul>";
    echo '</div>';
    
    if($i == 4) {
        echo '</div>';
        $i = 1;
        $closed = true;
    } else {
        $i++;
    }
endforeach;
if(!$closed) {
    echo '</div>';
}
/*echo '<table cellspacing="0" cellpadding="0" border="0" width="100%" class="liste">';
    echo '<thead>';
        echo '<tr>';
            echo '<th width="18%">Nom</th>';
            echo '<th width="18%">Prénom</th>';
            echo '<th width="18%">Rôle</th>';
            echo '<th width="20%">Dernière connexion</th>';
            echo '<th>&nbsp;</th>';
            echo '<th>&nbsp;</th>';
            echo '<th>&nbsp;</th>';
        echo '</tr>';
    echo '</thead>';
    echo '<tbody>';
        foreach($users as $key => $user):
            echo '<tr>';
                echo '<td>'. $user['User']['nom'] .'</td>';
                echo '<td>'. $user['User']['prenom'] .'</td>';
                echo '<td><em>'. $user['Role']['name'] .'</em></td>';
                echo '<td>'. date('d/m/Y à H\hi', strtotime($user['User']['modified'])) .'</td>';
                echo '<td class="btn-action">'. $this->Html->link('Modifier', array(
                    'action' => 'edit',
                    $user['User']['id']
                )) .'</td>';
                echo '<td class="btn-action">'. $this->Html->link('Dupliquer', array(
                    'action' => 'duplicate',
                    $user['User']['id']
                )) .'</td>';
                echo '<td class="btn-action">'. $this->Html->link('Supprimer', array(
                    'action' => 'delete',
                    $user['User']['id']
                ), array(), "Supprimer cet utilisateur ?") .'</td>';
            echo '</tr>';
        endforeach;
    echo '</tbody>';
echo '</table>';*/
?>