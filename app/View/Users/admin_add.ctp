<?php echo $this->Form->create('User'); ?>
     <?php
    // Titrage + boutons d'actions + sous-nav
    echo '<header>';
        echo '<h1>Nouvel utilisateur</h1>';
        echo '<nav class="clearfix">';
            echo '<ul id="sous-nav" class="clearfix">';
                echo '<li>';
                    echo $this->Html->link('Création', array(
                        'action' => 'add'
                    ), array('class' => 'current'));
                echo '</li>';
            echo '</ul>';
            echo '<ul id="nav-actions" class="clearfix">';
                echo '<li class="other">';
                    echo $this->Html->link('Annuler', array(
                        'controller' => 'users',
                        'action' => 'index'
                    ));
                echo '</li>';
                echo '<li>';
                    echo $this->Form->submit('Enregistrer');
                echo '</li>';
            echo '</ul>';
        echo '</nav>';
    echo '</header>';
    ?>

    <?php
    echo $this->Form->input('User.id');
    echo '<div><div>';
    ?>
        <?php echo $this->Session->flash(); ?>
        <div class="row-fluid">
            <div class="span6">
                <h2>Identité de l'utilisateur</h2>
                <fieldset class="block">
                    <?php
                    echo $this->Form->input('User.nom', array('label' => 'Nom de famille', 'class' => 'span12'));
                    echo $this->Form->input('User.prenom', array('label' => 'Prénom', 'class' => 'span12'));
                    echo $this->Form->input('User.role', array(
                        'label' => 'Rôle',
                        'options' => $options,
                        'type' => 'select'
                    ));
                    ?>
                </fieldset>
            </div>
            <div class="span6">
                <h2>Informations de connexion</h2>
                <fieldset class="block">
                    <?php
                    echo $this->Form->input('User.username', array('label' => 'Nom d\'utilisateur', 'class' => 'span12'));
                    echo $this->Form->input('User.password', array('label' => 'Mot de passe', 'class' => 'span12'));
                    ?>
                </fieldset>
            </div>
        </div>
    <?php echo '</div></div>'; ?>
<?php echo $this->Form->end(); ?>