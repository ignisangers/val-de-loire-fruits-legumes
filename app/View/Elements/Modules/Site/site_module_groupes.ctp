<div class='groupe'>
    <h1><?php echo $module[$module['Content']['model']][0]['title']; ?></h1>

    <?php foreach ($module[$module['Content']['model']]['Contenu'] as $personne): ?>
        <div class='personne'>
            <?php echo $this->Html->image($personne['Groupe']['photo']) ?>
            <br>
            <span class='nom'><?php echo $personne['Groupe']['nom']; ?></span>
            <br>
            <span class='fonction'><?php echo $personne['Groupe']['fonction']; ?></span>
            <br>
            <span class='adresse'><?php echo $personne['Groupe']['adresse']; ?></span>
            <br>
            <span class='code_postal'><?php echo $personne['Groupe']['code_postal']; ?></span>&nbsp;
            <span class='ville'><?php echo $personne['Groupe']['ville']; ?></span>
            <br>
            <span class='telephone'>Tel. <?php echo $personne['Groupe']['telephone']; ?></span>
            <br>
            <span class='autres'><?php echo $personne['Groupe']['autres']; ?></span>
        </div>
    <?php endforeach; ?>
</div>