<?php
/**
 * Module Actualités
 * 
 * Afficher une ou plusieurs actualités
 * 
 * $module['Recettes'][x]['Recette']['name'] -> Titre
 * $module['Recettes'][x]['Recette']['content'] -> Contenu (rédigé avec Redactor)
 * $module['Recettes'][x]['Recette']['created'] -> Date de création (datetime SQL, ex. : 2015-01-01 00:00:00)
 * 
 * $module['Pagination']['links'] -> Liste des liens pour la pagination sous la forme : $numeroDePage => $hrefSousFormedArray (facultatif)
 * $module['Pagination']['current'] -> Lien de la pagination (numéro de page) à "activer"
 * 
 * $module['hrefPageNews'] -> Lien vers la page générale des actualités, si elle existe et qu'on ne veut pas de pagination
 */
?>
<div class="m-recettes">
    <div class="items_list__header">
        <div class="items_list__logo">
            <?php echo $this->Html->image('logo.png', array('alt' => '')); ?>
        </div>
        <div class="items_list__image-france">
            <?php echo $this->Html->image('france.png', array('alt' => 'pays-de-la-loire')); ?>
        </div>

        <h1 class="items_list__title">Les Recettes</h1>
        <?php if (isset($this->request->query['type'])): ?>
            <h1 class="items_list__title--small">
                <?php if ($this->request->query['type'] == 'entree'): ?>
                    Les Entrées
                <?php else: ?>
                    Les Plats
                <?php endif; ?>
            </h1>
        <?php endif; ?>        
    </div>
    <div class="items_list__resume">
        <!--Vous ne savez pas comment accommoder vos fruits et légumes du Val de Loire ? Vous être au bon endroit.-->
        Les recettes de saison sont signalées par le pictogramme <?php echo $this->Html->image('smiles.png', array('style' => '')) ?>
    </div>
    <section class="m-recettes-list">
        <?php if (!empty($module['Recettes'])) { ?>
            <ul class="">
                <span>Les entrées</span>
                <?php foreach ($module['Recettes'] as $recette): ?>
                    <?php if ($recette['Recette']['type'] == 'entrée'): ?>
                        <li>
                            <?php
                            //recette de saison ou non
                            if ($recette['Recette']['debut'] > $recette['Recette']['fin']) {
                                if (date('m') >= $recette['Recette']['debut'] || date('m') <= $recette['Recette']['fin']) {
                                    echo $this->Html->image('smiles.png', array('style' => '')) . "  " . $this->Html->link($recette['Recette']['name'], array(
                                        "controller" => "recettes",
                                        "action" => "display",
                                        $recette['Recette']['slug']
                                    ));
                                } else {
                                    echo $this->Html->link($recette['Recette']['name'], array(
                                        "controller" => "recettes",
                                        "action" => "display",
                                        $recette['Recette']['slug']
                                    ));
                                }
                            } else {
                                if (date('m') >= $recette['Recette']['debut'] && date('m') <= $recette['Recette']['fin']) {
                                    echo $this->Html->image('smiles.png', array('style' => '')) . "  " . $this->Html->link($recette['Recette']['name'], array(
                                        "controller" => "recettes",
                                        "action" => "display",
                                        $recette['Recette']['slug']
                                    ));
                                } else {
                                    echo $this->Html->link($recette['Recette']['name'], array(
                                        "controller" => "recettes",
                                        "action" => "display",
                                        $recette['Recette']['slug']
                                    ));
                                }
                            }
                            ?> 
                        </li>
                    <?php endif; ?>
                <?php endforeach; ?>
            </ul>
            <ul class="">
                <span>Les plats</span>
                <?php foreach ($module['Recettes'] as $recette): ?>
                    <?php if ($recette['Recette']['type'] == 'plat'): ?>
                        <li>
                            <?php
                            //recette de saison ou non
                            if ($recette['Recette']['debut'] > $recette['Recette']['fin']) {
                                if (date('m') >= $recette['Recette']['debut'] || date('m') <= $recette['Recette']['fin']) {
                                    echo $this->Html->image('smiles.png', array('style' => '')) . "  " . $this->Html->link($recette['Recette']['name'], array(
                                        "controller" => "recettes",
                                        "action" => "display",
                                        $recette['Recette']['slug']
                                    ));
                                } else {
                                    echo $this->Html->link($recette['Recette']['name'], array(
                                        "controller" => "recettes",
                                        "action" => "display",
                                        $recette['Recette']['slug']
                                    ));
                                }
                            } else {
                                if (date('m') >= $recette['Recette']['debut'] && date('m') <= $recette['Recette']['fin']) {
                                    echo $this->Html->image('smiles.png', array('style' => '')) . "  " . $this->Html->link($recette['Recette']['name'], array(
                                        "controller" => "recettes",
                                        "action" => "display",
                                        $recette['Recette']['slug']
                                    ));
                                } else {
                                    echo $this->Html->link($recette['Recette']['name'], array(
                                        "controller" => "recettes",
                                        "action" => "display",
                                        $recette['Recette']['slug']
                                    ));
                                }
                            }
                            ?>
                        </li>
                    <?php endif; ?>
                <?php endforeach; ?>
            </ul>
            <ul class="">
                <span>Les desserts</span>
                <?php foreach ($module['Recettes'] as $recette): ?>
                    <?php if ($recette['Recette']['type'] == 'dessert'): ?>
                        <li>
                            <?php
                            //recette de saison ou non
                            if ($recette['Recette']['debut'] > $recette['Recette']['fin']) {
                                if (date('m') >= $recette['Recette']['debut'] || date('m') <= $recette['Recette']['fin']) {
                                    echo $this->Html->image('smiles.png', array('style' => '')) . "  " . $this->Html->link($recette['Recette']['name'], array(
                                        "controller" => "recettes",
                                        "action" => "display",
                                        $recette['Recette']['slug']
                                    ));
                                } else {
                                    echo $this->Html->link($recette['Recette']['name'], array(
                                        "controller" => "recettes",
                                        "action" => "display",
                                        $recette['Recette']['slug']
                                    ));
                                }
                            } else {
                                if (date('m') >= $recette['Recette']['debut'] && date('m') <= $recette['Recette']['fin']) {
                                    echo $this->Html->image('smiles.png', array('style' => '')) . "  " . $this->Html->link($recette['Recette']['name'], array(
                                        "controller" => "recettes",
                                        "action" => "display",
                                        $recette['Recette']['slug']
                                    ));
                                } else {
                                    echo $this->Html->link($recette['Recette']['name'], array(
                                        "controller" => "recettes",
                                        "action" => "display",
                                        $recette['Recette']['slug']
                                    ));
                                }
                            }




//                            if ((date('m') >= $recette['Recette']['debut']) && (date('m') <= $recette['Recette']['fin'])) {
//                                echo $this->Html->image('smiles.png', array('style' => '')) . "  " . $this->Html->link($recette['Recette']['name'], array(
//                                    "controller" => "recettes",
//                                    "action" => "display",
//                                    $recette['Recette']['slug']
//                                ));
//                            } else {
//                                echo $this->Html->link($recette['Recette']['name'], array(
//                                    "controller" => "recettes",
//                                    "action" => "display",
//                                    $recette['Recette']['slug']
//                                ));
//                            }
                            ?>
                        </li>
                    <?php endif; ?>
                <?php endforeach; ?>
            </ul>
        <?php } ?>
        <?php if (isset($module['Pagination']["links"]) && !empty($module['Pagination']["links"]) && count($module['Pagination']["links"]) > 1) { ?>
            <?php debug($module['Pagination']); ?>
            <ol class="pagination clearfix m-produits-pagination">
                <?php foreach ($module['Pagination']["links"] as $page => $href): ?>
                    <li>
                        <?php
                        /**
                         * Si c'est la page courante : on affiche juste le numéro de page
                         * Si c'est une autre page : on affiche un lien
                         */
                        echo ($module['Pagination']['current'] === $page) ? $page : $this->Html->link($page, $href);
                        ?>
                    </li>
                <?php endforeach; ?>
            </ol>
            <?php
        }
//        else if (isset($module['hrefPageNews'])) {
//            // Lien vers la page "Actualités" (la variable doit être fournie par le template)
//            echo $this->Html->link("Voir toutes les recette", $module['hrefPageNews']);
//        }
        ?>
    </section>    
</div>
<?php echo $this->Html->script("responsiveslides.min", array("inline" => false)); ?>
<?php echo $this->Html->scriptStart(array("inline" => false)); ?>

$(function () {
//slider
$(".m-gallery").responsiveSlides();

$(".recette-slider").responsiveSlides({
auto: true,
speed: 2000,
timeout: 4000
});

});
<?php echo $this->Html->scriptEnd(); ?>