<?php
/**
 * Module Lien vers une page interne
 * 
 * $module['ModuleLien']['name'] -> Nom du lien
 * $module['ModuleLien']['content'] -> Description du lien
 * $module['Page']['alias'] -> Slug de la page liée
 * 
 * (tous les autres champs de la table "pages" sont également dispo dans $module['Page'])
 */
?>
<div class="m-lien">
    <h5 class="m-lien__title"><?php echo $module['ModuleLien']['name']; ?></h5>
    <div class="m-lien__content">
        <?php
        echo $module['ModuleLien']['content'];
        
        echo $this->Html->link(__('Lire la suite'), array(
            'admin' => false,
            'controller' => 'pages',
            'action' => 'display',
            'language' => Configure::read('Config.language'),
            $module['Page']['alias']
                ), array('class' => 'm-lien__button')) . '</p>';
        ?>
    </div>    
</div>