<?php
/**
 * Module Galerie d'images
 * 
 * $module['Galleries'][x]['Gallery']['filename'] -> Nom de fichier de l'image
 * $module['Galleries'][x]['Gallery']['caption'] -> Légende de l'image (facultative)
 */
?>
<div class="m-gallery">
    <?php if (!empty($module['Galleries'])) { ?>
        <?php foreach ($module['Galleries'] as $g): ?>
            <figure class="m-gallery-item">
                <?php
                echo $this->Html->image("../files/galleries/" . $g['Gallery']['filename'], array(
                    "alt" => $g['Gallery']['caption'],
                    "class" => "m-gallery-item__picture"
                ));
                ?>
                <?php if (!empty($g['Gallery']['caption'])) { ?>
                    <figcaption class="m-gallery-item__caption"><?php echo $g['Gallery']['caption']; ?></figcaption>
                <?php } ?>
            </figure>
        <?php endforeach; ?>
    <?php } ?>
</div>