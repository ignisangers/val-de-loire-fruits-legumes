<?php
/**
 * Module Actualités
 * 
 * Afficher une ou plusieurs actualités
 * 
 * $module['Produits'][x]['Produit']['name'] -> Titre
 * $module['Produits'][x]['Produit']['content'] -> Contenu (rédigé avec Redactor)
 * $module['Produits'][x]['Produit']['created'] -> Date de création (datetime SQL, ex. : 2015-01-01 00:00:00)
 * 
 * $module['Pagination']['links'] -> Liste des liens pour la pagination sous la forme : $numeroDePage => $hrefSousFormedArray (facultatif)
 * $module['Pagination']['current'] -> Lien de la pagination (numéro de page) à "activer"
 * 
 * $module['hrefPageNews'] -> Lien vers la page générale des actualités, si elle existe et qu'on ne veut pas de pagination
 */
?>
<div class="m-produits">
    <div class="items_list__header">
        <div class="items_list__logo">
            <?php echo $this->Html->image('logo.png', array('alt' => '')); ?>
        </div>
        <div class="items_list__image-france">
            <?php echo $this->Html->image('france.png', array('alt' => 'pays-de-la-loire')); ?>
        </div>

        <h1 class="items_list__title">Les Produits</h1>       

        <?php if (isset($this->request->query['type'])): ?>
            <h1 class="items_list__title--small">
                <?php if ($this->request->query['type'] == 'fruit'): ?>
                    Les Fruits
                <?php else: ?>
                    Les Légumes
                <?php endif; ?>
            </h1>
        <?php endif; ?>

    </div>
    <div class="items_list__resume">
        <!--Le Val de Loire est un bassin propice à la culture des Fruits et Légumes, qui font la richesse et la réputation de ce territoire.-->
        Le pictogramme <?php echo $this->Html->image('smiles.png', array('style' => '')); ?> vous indique les produits de saison.
    </div> 
    <section class="m-produits-list">        
        <?php if (!empty($module['Produits'])) { ?>            
            <ul class="">
                <span>Les fruits</span>
                <?php foreach ($module['Produits'] as $produit): ?>
                    <?php if ($produit['Produit']['type'] == 'fruit'): ?>
                        <li>                           
                            <?php
                            //produit de saison ou non
                            $produitSaison = false;
                            if ($produit['Produit']['debut'] > $produit['Produit']['fin']) {
                                if (date('m') >= $produit['Produit']['debut'] || date('m') <= $produit['Produit']['fin']) {
                                    echo $this->Html->image('smiles.png', array('style' => '')) . "  " . $this->Html->link($produit['Produit']['name'], array(
                                        "controller" => "produits",
                                        "action" => "display",
                                        $produit['Produit']['slug']
                                    ));
                                } else {
                                    echo $this->Html->link($produit['Produit']['name'], array(
                                        "controller" => "produits",
                                        "action" => "display",
                                        $produit['Produit']['slug']
                                    ));
                                }
                            } else {
                                if (date('m') >= $produit['Produit']['debut'] && date('m') <= $produit['Produit']['fin']) {
                                    echo $this->Html->image('smiles.png', array('style' => '')) . "  " . $this->Html->link($produit['Produit']['name'], array(
                                        "controller" => "produits",
                                        "action" => "display",
                                        $produit['Produit']['slug']
                                    ));
                                } else {
                                    echo $this->Html->link($produit['Produit']['name'], array(
                                        "controller" => "produits",
                                        "action" => "display",
                                        $produit['Produit']['slug']
                                    ));
                                }
                            }
                            ?>                           
                        </li>
                    <?php endif; ?>
                <?php endforeach; ?>
            </ul>
            <ul class="">                
                <span>Les légumes</span>
                <?php foreach ($module['Produits'] as $produit): ?>
                    <?php if ($produit['Produit']['type'] == 'légume'): ?>
                        <li>
                            <?php
                            //produit de saison ou non
                            $produitSaison = false;
                            if ($produit['Produit']['debut'] > $produit['Produit']['fin']) {
                                if (date('m') >= $produit['Produit']['debut'] || date('m') <= $produit['Produit']['fin']) {
                                    echo $this->Html->image('smiles.png', array('style' => '')) . "  " . $this->Html->link($produit['Produit']['name'], array(
                                        "controller" => "produits",
                                        "action" => "display",
                                        $produit['Produit']['slug']
                                    ));
                                } else {
                                    echo $this->Html->link($produit['Produit']['name'], array(
                                        "controller" => "produits",
                                        "action" => "display",
                                        $produit['Produit']['slug']
                                    ));
                                }
                            } else {
                                if (date('m') >= $produit['Produit']['debut'] && date('m') <= $produit['Produit']['fin']) {
                                    echo $this->Html->image('smiles.png', array('style' => '')) . "  " . $this->Html->link($produit['Produit']['name'], array(
                                        "controller" => "produits",
                                        "action" => "display",
                                        $produit['Produit']['slug']
                                    ));
                                } else {
                                    echo $this->Html->link($produit['Produit']['name'], array(
                                        "controller" => "produits",
                                        "action" => "display",
                                        $produit['Produit']['slug']
                                    ));
                                }
                            }
                            ?> 
                        </li>
                    <?php endif; ?>
                <?php endforeach; ?>
            </ul>
        <?php } ?>
        <?php if (isset($module['Pagination']["links"]) && !empty($module['Pagination']["links"]) && count($module['Pagination']["links"]) > 1) { ?>
            <ol class="pagination clearfix m-produits-pagination">
                <?php foreach ($module['Pagination']["links"] as $page => $href): ?>
                    <li>
                        <?php
                        /**
                         * Si c'est la page courante : on affiche juste le numéro de page
                         * Si c'est une autre page : on affiche un lien
                         */
                        echo ($module['Pagination']['current'] === $page) ? $page : $this->Html->link($page, $href);
                        ?>
                    </li>
                <?php endforeach; ?>
            </ol>
            <?php
        }
        ?>
    </section>
</div>
<?php echo $this->Html->script("responsiveslides.min", array("inline" => false)); ?>
<?php echo $this->Html->scriptStart(array("inline" => false)); ?>

$(function () {
//slider
$(".m-gallery").responsiveSlides();

$(".recette-slider").responsiveSlides({
auto: true,
speed: 2000,
timeout: 4000
});

});
<?php echo $this->Html->scriptEnd(); ?>