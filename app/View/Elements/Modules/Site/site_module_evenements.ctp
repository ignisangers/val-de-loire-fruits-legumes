<div class='evenement'>
    <?php foreach ($module[$module['Content']['model']]['Contenu'] as $evenement): ?>
        <article>
            <h1>
                <?php
                if (!empty($evenement['Evenement']['date_fin'])) {
                    echo "Du " . date('d/m/Y', strtotime($evenement['Evenement']['date_debut']));
                    if (date('H:i', strtotime($evenement['Evenement']['date_debut'])) !== '00:00') {
                        echo " à " . date('H:i', strtotime($evenement['Evenement']['date_debut']));
                    }
                    echo " au " . date('d/m/Y', strtotime($evenement['Evenement']['date_fin']));
                    if (date('H:i', strtotime($evenement['Evenement']['date_fin'])) !== '00:00') {
                        echo " à " . date('H:i', strtotime($evenement['Evenement']['date_fin']));
                    }
                } else {
                    echo "Le " . date('d/m/Y', strtotime($evenement['Evenement']['date_debut']));
                    if (date('H:i', strtotime($evenement['Evenement']['date_debut'])) !== '00:00') {
                        echo " à " . date('H:i', strtotime($evenement['Evenement']['date_debut']));
                    }
                }
                ?>
            </h1>
            <?php echo $evenement['Evenement']['content']; ?>
            <div style='clear:both;'></div>
        </article>
    <?php endforeach; ?>
</div>