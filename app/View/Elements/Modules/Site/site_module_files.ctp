<div class="items_list">
    <div class="items_list__header">
        <div class="items_list__logo">
            <?php echo $this->Html->image('logo.png', array('alt' => '')); ?>
        </div>
        <div class="items_list__image-france">
            <?php echo $this->Html->image('france.png', array('alt' => 'pays-de-la-loire')); ?>
        </div>

        <h1 class="items_list__title">Le coin des enfants</h1>
    </div>
    <div class="items_list__resume">
        <!--Parce que l’apprentissage des Fruits et Légumes peut aussi se faire de manière ludique …-->
    </div>
    <div class='files-list '>


        <?php if (isset($module['Files'])) { ?>
            <?php foreach ($module['Files'] as $file): ?>
                <article class="file grow">
                    <div class='file__img' data-image="mainImg<?php echo $file['File']['id'] ?>" onclick="printImg('mainImg<?php echo $file['File']['id'] ?>');">        
                        <a href="#"><img id="mainImg<?php echo $file['File']['id'] ?>" src="<?php echo "/" . $file['File']['fichier'] ?>"></a>
                    </div>                    
                    <!--<div class='file__content'>-->
                        <!--<h1><?php // echo $file['File']['nom'];         ?></h1>-->                        
                    <?php // echo $file['File']['description']; ?>
                    <?php // echo $this->Html->link(" (cliquez ici)", "/www/val-de-loire-fruits-legumes/".$file['File']['fichier']); ?>
        <!--                        <a href="/www/val-de-loire-fruits-legumes/<?php // echo $file['File']['fichier']         ?>" download="filename">Télécharger</a>
                        <a href="/www/val-de-loire-fruits-legumes/<?php // echo $file['File']['fichier']         ?>" >Voir</a>-->
                    <!--<div style='clear:both;'></div>-->
                    <!--</div>-->
                </article>
            <?php endforeach; ?>
        <?php } ?>
<!--<a href="/www/val-de-loire-fruits-legumes/<?php // echo $file['File']['fichier']           ?>" download="filename">Download link</a>-->
        <div class="addthis_inline_share_toolbox" style="margin-top: 25px;"></div>
    </div>
</div>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57d017caa3b9cce2"></script>
<script type="text/javascript">
                function printImg(imageId) {
                    image = new Image(600, 600);
                    image.src = document.getElementById(imageId).src;
                    console.log(image);
                    pwin = window.open(image.src, "_blank");
                    pwin.onload = function () {
                        pwin.print();
                    }
                }
</script>