<div class='trombinoscope'>
    <h1><?php echo $module[$module['Content']['model']][0]['title']; ?></h1>

    <?php foreach ($module[$module['Content']['model']]['Contenu'] as $personne): ?>
        <div class='personne'>
            <?php echo $this->Html->image($personne['Trombinoscope']['photo']) ?>
            <br>
            <span class='nom'><?php echo $personne['Trombinoscope']['nom']; ?></span>
            <br>
            <span class='fonction'><?php echo $personne['Trombinoscope']['fonction']; ?></span>
        </div>
    <?php endforeach; ?>
</div>