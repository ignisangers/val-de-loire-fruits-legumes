<?php
/**
 * Module Actualités
 * 
 * Afficher une ou plusieurs actualités
 * 
 * $module['Actualites'][x]['Actualite']['name'] -> Titre
 * $module['Actualites'][x]['Actualite']['content'] -> Contenu (rédigé avec Redactor)
 * $module['Actualites'][x]['Actualite']['created'] -> Date de création (datetime SQL, ex. : 2015-01-01 00:00:00)
 * 
 * $module['Pagination']['links'] -> Liste des liens pour la pagination sous la forme : $numeroDePage => $hrefSousFormedArray (facultatif)
 * $module['Pagination']['current'] -> Lien de la pagination (numéro de page) à "activer"
 * 
 * $module['hrefPageNews'] -> Lien vers la page générale des actualités, si elle existe et qu'on ne veut pas de pagination
 */
?>
<section class="m-news-list">
    <?php if (!empty($module['Actualites'])) { ?>
        <?php foreach ($module['Actualites'] as $actualite): ?>
            <article class="m-new">
                <h1 class="m-new__title"><?php echo $actualite['Actualite']['name']; ?></h1>
                <div class="m-new__content">
                    <?php echo $actualite['Actualite']['content']; ?>
                </div>
                <?php
                echo $this->Html->link("Lire la suite", array(
                    "controller" => "actualites",
                    "action" => "display",
                    $actualite['Actualite']['slug']
                ));
                ?>
            </article>
        <?php endforeach; ?>
    <?php } ?>
    <?php if (isset($module['Pagination']["links"]) && !empty($module['Pagination']["links"])) { ?>
        <ol class="pagination clearfix">
            <?php foreach ($module['Pagination']["links"] as $page => $href): ?>
                <li>
                    <?php
                    /**
                     * Si c'est la page courante : on affiche juste le numéro de page
                     * Si c'est une autre page : on affiche un lien
                     */
                    echo ($module['Pagination']['current'] === $page) ? $page : $this->Html->link($page, $href);
                    ?>
                </li>
            <?php endforeach; ?>
        </ol>
    <?php
    } else if (isset($module['hrefPageNews'])) {
        // Lien vers la page "Actualités" (la variable doit être fournie par le template)
        echo $this->Html->link("Voir toutes les actualités", $module['hrefPageNews']);
    }
    ?>
</section>
