<?php
/**
 * Module Article
 * 
 * $module['ModuleArticle']['name'] -> Titre de l'article
 * $module['ModuleArticle']['content'] -> Contenu de l'article (rédigé avec Redactor)
 * $module['ModuleArticle']['resume'] -> Résumé de l'article (sans mise en page), voué à disparaître ? (inutile en frontoffice)
 */
?>
<div class="items_list">
    <div class="items_list__header">
        <div class="items_list__logo">
            <?php echo $this->Html->image('logo.png', array('alt' => '')); ?>
        </div>
        <div class="items_list__image-france">
            <?php echo $this->Html->image('france.png', array('alt' => 'pays-de-la-loire')); ?>
        </div>

        <h1 class="items_list__title"><?php echo $page['Page']['name']; ?></h1>
    </div>
    <article class="m-article">
        <h1 class="m-article__title"><?php echo $module['ModuleArticle']['name']; ?></h1>
        <div class="m-article__content">
            <?php echo $module['ModuleArticle']['content']; ?>
        </div>
        <div class="addthis_inline_share_toolbox" style="margin-top: 25px;"></div>
    </article>
</div>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57d017caa3b9cce2"></script>