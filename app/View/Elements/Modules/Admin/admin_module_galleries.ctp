<h5>Galerie d'images</h5>
<?php if(!empty($module['Galleries'])) { ?>
    <ol class="list-thumbs clearfix">
        <?php foreach ($module['Galleries'] as $g): ?>
            <li>
                <?php
                echo $this->Html->image("../files/galleries/" . $g["Gallery"]["filename"], array(
                    "alt" => $g["Gallery"]["caption"]
                ));
                ?>
            </li>
        <?php endforeach; ?>
    </ol>
<?php }