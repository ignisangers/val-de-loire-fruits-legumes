<?php if (!isset($gallery) || empty($gallery)) { ?>
    <div class="block">Aucune image ajoutée à la galerie.</div>
    <input type="hidden" id="countGallery" value="<?php echo count($gallery); ?>">
<?php } else { ?>
    <input type="hidden" id="countGallery" value="<?php echo count($gallery); ?>">
    <table width="100%" class="liste">
        <thead>
            <tr>
                <th width="40">Photo</th>
                <th></th>
                <th width="36" style="padding: 0;">&nbsp;</th>
                <th width="50">&nbsp;</th>
                <th width="65">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($gallery as $k => $g): ?>
                <tr>
                    <td>
                        <?php
                        echo $this->Html->image("../files/recettes/gallerie/" . $g["GallerieRecette"]["filename"], array(
//                            "alt" => $g["GallerieProduit"]["name"],
                            "width" => "100%"
                        ));
                        ?>
                    </td>
                    <td><?php // echo (!empty($g["GallerieProduit"]["name"])) ? $g["GallerieProduit"]["name"] : "<i>(Photo sans légende)</i>"; ?></td>
                    <td class="sortable">
                        <?php
//                        echo $this->Form->button("Haut", array(
//                            "type" => "button",
//                            "class" => ($k === 0) ? "sortable__up sortable__up--disabled" : "sortable__up",
//                            "title" => "Monter la photo d'un cran",
//                            "data-type" => "gallerie_recettes",
//                            "data-id" => $g["GallerieRecette"]["id"],
//                            "data-move" => "up"
//                        ));
//                        echo $this->Form->button("Bas", array(
//                            "type" => "button",
//                            "class" => ($k === (count($gallery)-1)) ? "sortable__down sortable__down--disabled" : "sortable__down",
//                            "title" => "Descendre la photo d'un cran",
//                            "data-type" => "gallerie_recettes",
//                            "data-id" => $g["GallerieRecette"]["id"],
//                            "data-move" => "down"
//                        ));
                        ?>
                    </td>
                    <!--<td class="btn-action">-->
                        <?php
//                        echo $this->Form->button("Modifier", array(
//                            "type" => "button",
//                            "class" => "edit-item",
//                            "data-type" => "gallerie_recettes",
//                            "data-id" => $g["GallerieRecette"]["id"],
////                            "data-name" => $g["GallerieProduit"]["name"],
//                            "data-filename" => $g["GallerieRecette"]["filename"]
//                        ));
                        ?>
                    <!--</td>-->
                    <td class="btn-action">
                        <?php
                        echo $this->Form->button("Supprimer", array(
                            "type" => "button",
                            "class" => "delete-item",
                            "data-type" => "gallerie_recettes",
                            "data-id" => $g["GallerieRecette"]["id"]
                        ));
                        ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php } ?>