<?php if (!isset($gallery) || empty($gallery)) { ?>
    <div class="block">Aucune image ajoutée à la galerie.</div>
<?php } else { ?>
    <table width="100%" class="liste">
        <thead>
            <tr>
                <th width="40">Photo</th>
                <th>Légende</th>
                <th width="36" style="padding: 0;">&nbsp;</th>
                <th width="50">&nbsp;</th>
                <th width="65">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($gallery as $k => $g): ?>
                <tr>
                    <td>
                        <?php
                        echo $this->Html->image("../files/galleries/" . $g["Gallery"]["filename"], array(
                            "alt" => $g["Gallery"]["caption"],
                            "width" => "100%"
                        ));
                        ?>
                    </td>
                    <td><?php echo (!empty($g["Gallery"]["caption"])) ? $g["Gallery"]["caption"] : "<i>(Photo sans légende)</i>"; ?></td>
                    <td class="sortable">
                        <?php
                        echo $this->Form->button("Haut", array(
                            "type" => "button",
                            "class" => ($k === 0) ? "sortable__up sortable__up--disabled" : "sortable__up",
                            "title" => "Monter la photo d'un cran",
                            "data-id" => $g["Gallery"]["id"],
                            "data-move" => "up"
                        ));
                        echo $this->Form->button("Bas", array(
                            "type" => "button",
                            "class" => ($k === (count($gallery)-1)) ? "sortable__down sortable__down--disabled" : "sortable__down",
                            "title" => "Descendre la photo d'un cran",
                            "data-id" => $g["Gallery"]["id"],
                            "data-move" => "down"
                        ));
                        ?>
                    </td>
                    <td class="btn-action">
                        <?php
                        echo $this->Form->button("Modifier", array(
                            "type" => "button",
                            "class" => "edit-item",
                            "data-id" => $g["Gallery"]["id"],
                            "data-caption" => $g["Gallery"]["caption"],
                            "data-filename" => $g["Gallery"]["filename"]
                        ));
                        ?>
                    </td>
                    <td class="btn-action">
                        <?php
                        echo $this->Form->button("Supprimer", array(
                            "type" => "button",
                            "class" => "delete-item",
                            "data-id" => $g["Gallery"]["id"]
                        ));
                        ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php } ?>