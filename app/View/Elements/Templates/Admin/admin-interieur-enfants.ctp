<?php
/**
 * TEMPLATE DE PAGE AVEC LISTE DES PAGES DU MÊME NIVEAU
 * 1 zone de modules
 * 
 * L'affichage des pages du même niveau est géré directement dans AppController::template_interieur_enfants
 * 
 * @param array $listModules : liste des modules dispos + liste des collections + ID de la page courante
 * @param array $modules : liste des zones de la page + modules de chaque zone
 */
?>
<div class="row-fluid">
    <div class="span12">
        <h2 class="list">
            <span>Contenu principal</span>
            <?php
            // Liste des modules disponibles pour la zone
            echo $this->element('Components/Admin/admin_add_list', array(
                "zone" => 1
            ));
            ?>
        </h2>
        <div>
            <?php
            // Liste des modules déjà créés dans la zone
            echo $this->element('Components/Admin/admin_modules', array("modules" => (isset($modules[1])) ? $modules[1] : array()));
            ?>
        </div>
    </div>
</div>