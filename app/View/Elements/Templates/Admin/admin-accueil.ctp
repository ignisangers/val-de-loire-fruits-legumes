<?php
/**
 * TEMPLATE ADMIN DE LA PAGE D'ACCUEIL
 * 5 zones de modules
 * -> 1 zone pour un slider ou une image seule (1 module max)
 * -> 4 zones pouvant accueillir un lien vers une page interne (1 module max)
 * 
 * @param array $listModules : liste des modules dispos + liste des collections + ID de la page courante
 * @param array $modules : liste des zones de la page + modules de chaque zone
 */
?>
<div class="row-fluid">
    <div class="span12">
        <h2 class="list">
            <span>Diaporama/image fixe</span>
            <?php            
            // Liste des modules disponibles pour la zone
            if (!isset($modules[1]) || count($modules[1]) < 1) {
                echo $this->element('Components/Admin/admin_add_list', array(
                    "collection" => "gallery",
                    "zone" => 1,
                    "max" => 1
                ));
            }
            ?>
        </h2>
        <div>
            <?php
            // Liste des modules déjà créés dans la zone
            echo $this->element('Components/Admin/admin_modules', array("modules" => (isset($modules[1])) ? $modules[1] : array()));
            ?>
        </div>
    </div>
</div>
<div class="row-fluid">
    <?php for($i=1; $i<=4; $i++): ?>
        <div class="span3">
            <h2 class="list">
                <span>Lien <?php echo $i; ?></span>
                <?php
                // Liste des modules disponibles pour la zone
                if (!isset($modules[($i+1)]) || count($modules[($i+1)]) < 1) {
                    echo $this->element('Components/Admin/admin_add_list', array(
                        "collection" => "link", 
                        "zone" => ($i+1),
                        "max" => 1
                    ));
                }
                ?>
            </h2>
            <div>
                <?php
                // Liste des modules déjà créés dans la zone
                echo $this->element('Components/Admin/admin_modules', array("modules" => (isset($modules[($i+1)])) ? $modules[($i+1)] : array()));
                ?>
            </div>
        </div>
    <?php endfor; ?>
</div>