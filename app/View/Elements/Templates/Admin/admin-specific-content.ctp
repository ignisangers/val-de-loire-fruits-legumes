<?php
/**
 * Template admin affiché lorsqu'une page n'est pas paramétrée pour de la gestion de modules
 * Cela signifie que les contenus de la page sont gérés à part, dans l'onglet "Contenus"
 * On indique à l'utilisateur le type de contenu de la page, s'il peut ou non l'administrer et où l'administrer le cas échéant
 */
?>
<div class="row-fluid">
    <div class="span12">
        <h2 class="list">
            <span><?php echo $pageType['name']; ?></span>
        </h2>
        <p class="module" style="padding: 12px; margin: 0;">
            <?php echo $pageType['description']; ?>
            <?php if (!empty($pageType['link'])) { ?>
                <br>
                <?php
                echo $this->Html->link("> " . $pageType['link'], array(
                    'controller' => $pageType['controller'],
                    'action' => $pageType['action'],
                    'admin' => true
                ));
            }
            ?>
        </p>
    </div>
</div>