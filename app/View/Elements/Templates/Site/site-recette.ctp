<div class="l-default">

    <div class="l-default__right">

        <div class="recette-gallerie" >
            <div class="recette-gallerie__degrade"></div>
            <?php if (!empty($recette['VignetteRecette'][0]['filename'])): ?>
                <?php echo $this->Html->image('/files/recettes/gallerie/' . $recette['GallerieRecette'][0]['filename']); ?>
            <?php endif; ?>
            <div class="recette-gallerie__content">
                <?php if ($recetteSaison == true): ?>
                    <h1><?php echo $recette['Recette']['name'] . " " . $this->Html->image('smiles-large.png', array('style' => '')); ?></h1>
                <?php else: ?>
                    <h1><?php echo ucfirst($recette['Recette']['name']) ?></h1>
                <?php endif; ?>
            </div>

            <div class="recette-gallerie__logo">
                <?php echo $this->Html->image('logo.png', array('alt' => '')); ?>
            </div>

            <div class="recette-gallerie__saison">
                <span><?php echo "De " . $recette['Recette']['debut'] . " à " . $recette['Recette']['fin']; ?></span>
            </div>

            <div class="recette-gallerie__type">
                <span><?php echo $recette['Recette']['type']; ?></span>
            </div>

            <div class="recette-gallerie__items">
                <?php if (!empty($recette['Recette']['difficulte'])): ?>
                    <div class="recette-gallerie__item">

                        <?php echo $this->Html->image('difficulte.png', array('alt' => '')); ?>                        
                        <?php echo $this->element('Components/Site/recette_difficulte') ?>
                    </div>
                <?php endif; ?>
                <?php if (!empty($recette['Recette']['personne'])): ?>
                    <div class="recette-gallerie__item">
                        <?php echo $this->Html->image('personnes.png', array('alt' => '')); ?>
                        <span><?php echo $recette['Recette']['personne'] ?> pers.</span>
                    </div>
                <?php endif; ?>
                <?php if (!empty($recette['Recette']['preparation'])): ?>
                    <div class="recette-gallerie__item">
                        <?php echo $this->Html->image('preparation.png', array('alt' => '')); ?>
                        <span><?php echo $recette['Recette']['preparation'] ?> min.</span>
                    </div>
                <?php endif; ?>
                <?php if (!empty($recette['Recette']['cuisson'])): ?>
                    <div class="recette-gallerie__item">
                        <?php echo $this->Html->image('cuisson.png', array('alt' => '')); ?>
                        <span><?php echo $recette['Recette']['cuisson'] ?> min.</span>
                    </div>
                <?php endif; ?>
            </div>

        </div>

        <div class="recette-description">

            <div class="recette-description__content">
                <?php echo $recette['Recette']['content']; ?>

                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_inline_share_toolbox" style="margin-top: 25px;"></div>
            </div>            

            <div class="recette-description__img">
                <?php if (!empty($recette['VignetteRecette'][0]['filename'])): ?>
                    <div class="description__img__container">                    
                        <?php echo $this->Html->image('/files/recettes/vignette/' . $recette['VignetteRecette'][0]['filename']) ?>
                        <?php echo $this->Html->image('zoom.png', array('data-toggle' => 'modal', 'data-target' => '#myModal', 'class' => 'zoom', 'style' => 'position: absolute;bottom: 5px;right: 5px;cursor:pointer')) ?>                    
                    </div>  
                <?php endif; ?>
                <a href="/recettes/recettePdf/<?php echo $recette['Recette']['id'] . ".pdf" ?>" target="_blank">
                    <?php echo $this->Html->image('pdf.png', array('style' => 'margin-top:3%;')) ?>
                </a>            
            </div>

        </div>

    </div>

    <div class="l-default__left">

        <div class="recette-saison-links">
            <h1>Les recettes de saison</h1>
            <?php if (!empty($recettesSaison)): ?>
                <ul class="liste-recettes">
                    <?php foreach ($recettesSaison as $recetteSaison): ?>
                        <li>
                            <?php
                            echo $this->Html->link($recetteSaison['Recette']['name'], array(
                                "controller" => "recettes",
                                "action" => "display",
                                $recetteSaison['Recette']['slug']
                            ));
                            echo $this->Html->image('smiles.png', array('style' => ''));
                            ?> 
                        </li>
                    <?php endforeach; ?>
                    <?php
                    echo $this->Html->link(">> voir toutes les recettes", array(
                        "controller" => "pages",
                        "action" => "display", 'les-recettes'
                    ));
                    ?>
                </ul>
            <?php endif; ?>          
        </div>

        <div class="produit-link" style="background-image: url(/files/produits/vignette/<?php
        if ($produitSaison['VignetteProduit']) {
            echo $produitSaison['VignetteProduit'][0]['filename'];
        }
        ?>) ;no-repeat;background-position: center;background-size: cover;background-repeat: no-repeat;">
            <div class="produit-link__content">
                <h1>Un produit de saison</h1>
                <?php if (!empty($produitSaison)): ?>
                    <?php
                    echo $this->Html->link($produitSaison['Produit']['name'], array(
                        "controller" => "produits",
                        "action" => "display",
                        $produitSaison['Produit']['slug']
                    ));
                    echo $this->Html->image('fleche.png', array('style' => 'padding: 5px 5px 30px 5px !important;'));
                    ?>
                <?php endif; ?>
            </div>

        </div>

    </div>


    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                <div class="modal-body">
                    <?php echo $this->Html->image('/files/recettes/vignette/' . $recette['VignetteRecette'][0]['filename']) ?>
                </div>
            </div>

        </div>
    </div>


</div>

<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57d017caa3b9cce2"></script>




