<?php
/**
 * TEMPLATE DE PAGE AVEC LISTE DES PAGES ENFANTS DIRECTES
 * 1 zone de modules
 * 
 * Gestion de la mise en page générale de la <b>page</b>.
 * C'est ici qu'on va inclure tous les éléments spécifiques à la page : modules par zone, fil d'ariane, etc.
 * On passera en majorité par des "élements", pour garder au maximum la mise en page HTML.
 * 
 * Fonctionnement de l'élément "/app/View/Elements/Components/Site/site_modules.ctp"
 * On se sert de cet élément pour afficher les modules d'une zone spécifique à travers la variable $modules
 * On lui fournit simplement la liste des modules de sa zone : $modules["ID_de_la_zone"]
 * (ex. : $modules[1], pour la liste des modules de la Zone n°1)
 * Ainsi que le nom du template courant : $template, pour voir si une vue spécifique à ce template existe pour chaque module
 * 
 * Fonctionnement de l'élément "/app/View/Elements/Components/Site/site_breadcrumb.ctp"
 * C'est le fil d'ariane du site, autrement dit le parcours depuis la racine jusqu'à la page courante
 * Attention : actuellement, ce fil d'ariane ne fonctionne qu'avec le système de pages.
 * Donc, si des pages "fictives" existent (ex. : page d'une actualité, créée à partir d'une actualité et non d'une page), 
 * elles ne seront pas prises en compte voire feront planter la page.
 * 
  * Fonctionnement de l'élément "/app/View/Elements/Components/Site/site_children_list"
 * On affiche la liste des pages du même niveau ou les enfants directs.
 * Ici ce sont les <b>pages enfants directes</b>.
 * 
 * @param array $modules : liste des zones et de leurs modules
 * @param string $template : nom du template actuel
 */
?>
<div class="container">
    <!-- FIL D'ARIANE -->
    <?php echo $this->element('Components/Site/site_breadcrumb', array('breadcrumb' => $breadcrumb)) ?>
    <div class="row-fluid">
        <!-- CONTENU PRINCIPAL -->
        <section class="span8">
            <?php
            echo $this->element('Components/Site/site_modules', array(
                'modules' => (isset($modules[1])) ? $modules[1] : array(),
                'template' => $template
            ));
            ?>
        </section>
        <!-- ARBORESCENCE MÊME NIVEAU -->
        <aside class="span4">
            <?php echo $this->element('Components/Site/site_children_list', array('children' => $children, 'id' => $page['Page']['id'])); ?>
        </aside>
    </div>
</div>