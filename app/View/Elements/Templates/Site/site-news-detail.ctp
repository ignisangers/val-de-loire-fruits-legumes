<?php
/**
 * TEMPLATE D'UNE PAGE ACTUALITÉ
 * 
 * @param array $news : données de l'actualité
 * @param string $template : nom du template actuel
 */
?>
<div class="container">
    <?php echo $this->element('Components/Site/site_breadcrumb', array('breadcrumb' => $breadcrumb, 'extension' => $extBreadcrumb)) ?>
    <article>
        <header>
            <h1><?php echo $news['Actualite']['name']; ?></h1>
            <time datetime="<?php echo date("Y-m-d", strtotime($news['Actualite']['created'])); ?>"><?php echo strftime("%d %B %Y", strtotime($news['Actualite']['created'])); ?></time>
        </header>
        <div>
            <?php echo $news['Actualite']['content']; ?>
        </div>
    </article>
</div>