<div class="l-default">

    <div class="l-default__right">

        <div class="produit-gallerie" >
            <?php echo $this->Html->image('/files/produits/gallerie/' . $produit['GallerieProduit'][0]['filename']); ?>

            <div class="produit-gallerie__content">
                <?php if ($produitSaison == true): ?>
                    <h1><?php echo $produit['Produit']['name'] . " " . $this->Html->image('smiles-large.png', array('style' => '')); ?></h1>
                <?php else: ?>
                    <h1><?php echo $produit['Produit']['name'] ?></h1>
                <?php endif; ?>
            </div>

            <div class="produit-gallerie__logo">
                <?php echo $this->Html->image('logo.png', array('alt' => '')); ?>
            </div>

            <div class="recette-gallerie__type">
                <span><?php echo $produit['Produit']['type']; ?></span>
            </div>

            <div class="produit-gallerie__saison">
                <span><?php echo "De " . $produit['Produit']['debut'] . " à " . $produit['Produit']['fin']; ?></span>
            </div>
        </div>

        <div class="produit-description">

            <div class="produit-description__content">
                <?php echo $produit['Produit']['content']; ?>
                <div class="addthis_inline_share_toolbox" style="margin-top: 25px;"></div>
            </div>

            <div class="produit-description__liste">
                <?php if (!empty($recettesListe)): ?>
                    <h1><?php echo $produit['Produit']['name'] ?> : ses recettes </h1>
                <?php endif; ?>
                <?php foreach ($recettesListe as $recetteListe): ?>
                    <?php if (!empty($recetteListe['GallerieRecette'][0]['filename'])): ?>
                        <?php
//                        echo $this->Html->image('/files/recettes/gallerie/' . $recetteListe['GallerieRecette'][0]['filename']);
                        echo $this->Html->link($this->Html->image('/files/recettes/gallerie/' . $recetteListe['GallerieRecette'][0]['filename']), array(
                            "controller" => "recettes",
                            "action" => "display",
                            $recetteListe['Recette']['slug']), array('escape' => false)
                        );
                        ?>
                        <span><?php echo $recetteListe['Recette']['name'] ?></span>
                    <?php endif; ?>
                <?php endforeach; ?> 
            </div>

        </div>
    </div>

    <div class="l-default__left">

        <div class="produit-saison-links">
            <h1>Les produits de saison</h1>
            <?php if (!empty($fruits)): ?>
                <ul class="liste-produits">
                    <?php foreach ($fruits as $fruit): ?>
                        <li>
                            <?php
                            echo $this->Html->link($fruit['Produit']['name'], array(
                                "controller" => "produits",
                                "action" => "display",
                                $fruit['Produit']['slug']
                            ));
                            echo $this->Html->image('smiles.png', array('style' => ''));
                            ?> 
                        </li>
                    <?php endforeach; ?>
                    <?php
                    echo $this->Html->link(">> voir tous les fruits", array(
                        "controller" => "pages",
                        "action" => "display", 'les-produits'
                    ));
                    ?>
                </ul>
            <?php endif; ?>
            <?php if (!empty($legumes)): ?>
                <ul class="liste-produits">
                    <?php foreach ($legumes as $legume): ?>
                        <li>
                            <?php
                            echo $this->Html->link($legume['Produit']['name'], array(
                                "controller" => "produits",
                                "action" => "display",
                                $legume['Produit']['slug']
                            ));
                            echo $this->Html->image('smiles.png', array('style' => ''));
                            ?> 
                        </li>
                    <?php endforeach; ?>
                    <?php
                    echo $this->Html->link(">> voir tous les légumes", array(
                        "controller" => "pages",
                        "action" => "display", 'les-produits'
                    ));
                    ?>
                </ul>
            <?php endif; ?>            
        </div>

        <div class="recette-link">

            <ul class="recette-slider">
                <?php // debug($recettes)  ?>
                <?php if (!empty($recettes)): ?>
                    <?php foreach ($recettes as $recette): ?>

                        <li>

                            <div class="recette-slider-image"> 
                                <div class="recette-slider-image__content recette-slider-image__content--blue">                                    
                                    <div class="recette-slider-image__content__link">
                                        <h1>Une recette de saison</h1>
                                        <?php
                                        echo $this->Html->link($recette['Recette']['name'], array(
                                            "controller" => "recettes",
                                            "action" => "display",
                                            $recette['Recette']['slug']
                                        ));
                                        echo $this->Html->image('fleche.png', array('style' => 'padding: 5px 5px 30px 5px !important;'));
                                        ?>
                                    </div>
                                </div>
                                <?php echo $this->Html->image('/files/recettes/vignette/' . $recette['VignetteRecette'][0]['filename']); ?>      
                            </div>                            
                        </li>

                    <?php endforeach; ?>
                <?php endif; ?>
            </ul>

        </div>

    </div>
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57d017caa3b9cce2"></script>
    <?php echo $this->Html->script("responsiveslides.min", array("inline" => false)); ?>
    <?php echo $this->Html->scriptStart(array("inline" => false)); ?>

    $(function () {
    //slider
    $(".m-gallery").responsiveSlides();

    $(".recette-slider").responsiveSlides({
    auto: true,
    speed: 2000,
    timeout: 4000
    });

    });
    <?php echo $this->Html->scriptEnd(); ?>