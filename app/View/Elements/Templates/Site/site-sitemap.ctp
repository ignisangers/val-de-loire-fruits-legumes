<?php
/**
 * TEMPLATE PAGE PLAN DU SITE (hors modules)
 * 
 * On va afficher l'arborescence des pages du site
 * 
 * @param array $children : arborescence des pages du site
 */
?>
<div class="container">
    <?php
    // Fil d'ariane
    echo $this->element('Components/Site/site_breadcrumb', array('breadcrumb' => $breadcrumb));
    ?>
    </nav>
    
    <h1><?php echo __("Plan du site"); ?></h1>
    <div id="sitemap">
        <?php
        // Arborescence récursive des pages
        echo $this->element("Components/Site/site_sitemap");
        ?>
    </div>
</div>