<?php
/**
 * TEMPLATE DE LA PAGE D'ACCUEIL
 * 5 zones de modules :
 * -> 1 zone pour un slider ou une image seule
 * -> 4 zones pouvant accueillir un lien vers une page interne
 * 
 * Gestion de la mise en page générale de la <b>page</b>.
 * C'est ici qu'on va inclure tous les éléments spécifiques à la page : modules par zone, fil d'ariane, etc.
 * On passera en majorité par des "élements", pour garder au maximum la mise en page HTML.
 * 
 * Fonctionnement de l'élément "/app/View/Elements/Components/Site/site_modules.ctp"
 * On se sert de cet élément pour afficher les modules d'une zone spécifique à travers la variable $modules
 * On lui fournit simplement la liste des modules de sa zone : $modules["ID_de_la_zone"]
 * (ex. : $modules[1], pour la liste des modules de la Zone n°1)
 * Ainsi que le nom du template courant : $template, pour voir si une vue spécifique à ce template existe pour chaque module
 * 
 * Fonctionnement de l'élément "/app/View/Elements/Components/Site/site_breadcrumb.ctp"
 * C'est le fil d'ariane du site, autrement dit le parcours depuis la racine jusqu'à la page courante
 * Attention : actuellement, ce fil d'ariane ne fonctionne qu'avec le système de pages.
 * Donc, si des pages "fictives" existent (ex. : page d'une actualité, créée à partir d'une actualité et non d'une page), 
 * elles ne seront pas prises en compte voire feront planter la page.
 * 
 * @param array $modules : liste des zones et de leurs modules
 * @param string $template : nom du template actuel
 */
?>
<!-- SLIDER -->
<!--<div id="slider" class="container">
<?php
//    echo $this->element('Components/Site/site_modules', array(
//        'modules' => (isset($modules[1])) ? $modules[1] : array(),
//        'template' => $template
//    ));
?>
</div>-->
<!--<div class="container">
     FIL D'ARIANE 
<?php // echo $this->element('Components/Site/site_breadcrumb', array('breadcrumb' => $breadcrumb)) ?>
     LIENS 
    <div class="row-fluid liens_accueil">
<?php // for($i=2; $i<=5; $i++): ?>
            <div class="span3">
<?php
//                echo $this->element('Components/Site/site_modules', array(
//                    'modules' => (isset($modules[$i])) ? $modules[$i] : array(),
//                    'template' => $template
//                ));
?>
            </div>
<?php // endfor; ?>
    </div>
</div>-->


<div class="l-default">

    <div class="l-default__right--accueil">

        <div class="accueil-logo">
            <?php echo $this->Html->image('logo.png', array('alt' => 'val-de-loire-fruits-et-legumes')); ?>
        </div>

        <div class="accueil-title">
            <h1 class="accueil__title accueil__title--top">Mettez du goût</h1>
            <h1 class="accueil__title accueil__title--bottom">dans votre assiette !</h2>
        </div>

        <div class="accueil-img-france">
            <?php echo $this->Html->image('france.png', array('alt' => 'pays-de-la-loire')); ?>
        </div>

        <div class="accueil-img">
            <a href="/fra/pages/les-produits">
                <?php echo $this->Html->image('carte_region.png', array('alt' => 'pays-de-la-loire')); ?>
            </a>
        </div>         

    </div>

    <div class="l-default__left">

        <div class="produit-link" style="">
            <ul class="recette-slider">
                <?php // debug($produits) ?>
                <?php if (!empty($produits)): ?>
                    <?php foreach ($produits as $produit): ?>
                        <?php if (!empty($produit['VignetteProduit'][0]['filename'])): ?>
                            <li>

                                <div class="recette-slider-image ">
                                    <div class="recette-slider-image__content recette-slider-image__content--orange">                                    

                                        <div class="recette-slider-image__content__link">
                                            <h1>Un produit de saison</h1>
                                            <?php
                                            echo $this->Html->link($produit['Produit']['name'], array(
                                                "controller" => "produits",
                                                "action" => "display",
                                                $produit['Produit']['slug']
                                            ));
                                            echo $this->Html->image('fleche.png', array('style' => 'padding: 5px 5px 30px 5px !important;'));
                                            ?>
                                        </div>                                    
                                    </div>
                                    <?php echo $this->Html->image('/files/produits/vignette/' . $produit['VignetteProduit'][0]['filename']); ?>      
                                </div> 

                            </li>
                        <?php endif; ?>

                    <?php endforeach; ?>
                <?php endif; ?>
            </ul>

        </div>

        <div class="recette-link">

            <ul class="recette-slider">
                <?php // debug($recettes) ?>
                <?php if (!empty($recettes)): ?>
                    <?php foreach ($recettes as $recette): ?>
                        <?php if (!empty($recette['VignetteRecette'][0]['filename'])): ?>
                            <li>

                                <div class="recette-slider-image">
                                    <div class="recette-slider-image__content recette-slider-image__content--blue">                                  

                                        <div class="recette-slider-image__content__link">
                                            <h1>Une recette de saison</h1>
                                            <?php
                                            echo $this->Html->link($recette['Recette']['name'], array(
                                                "controller" => "recettes",
                                                "action" => "display",
                                                $recette['Recette']['slug']
                                            ));
                                            echo $this->Html->image('fleche.png', array('style' => 'padding: 5px 5px 30px 5px !important;'));
                                            ?>
                                        </div>                                    
                                    </div>
                                    <?php echo $this->Html->image('/files/recettes/vignette/' . $recette['VignetteRecette'][0]['filename']); ?>      
                                </div> 

                            </li>
                        <?php endif; ?>

                    <?php endforeach; ?>
                <?php endif; ?>
            </ul>

        </div>


    </div>                
</div>        





<?php echo $this->Html->script("responsiveslides.min", array("inline" => false)); ?>
<?php echo $this->Html->scriptStart(array("inline" => false)); ?>

$(function () {
//slider
$(".m-gallery").responsiveSlides();

$(".recette-slider").responsiveSlides({
auto: true,
speed: 3000,
timeout: 4000
});

});
<?php echo $this->Html->scriptEnd(); ?>