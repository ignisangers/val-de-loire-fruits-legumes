<?php
/**
 * TEMPLATE PAGE CONTACT (hors modules)
 * 
 * On va afficher le formulaire de contact du site
 */
?>
<div class="container">
    <div class="l-default">

        <div class="l-default__right"> 
            <div class="items_list">
                <div class="items_list__header">
                    <div class="items_list__logo">
                        <?php echo $this->Html->image('logo.png', array('alt' => '')); ?>
                    </div>
                    <div class="items_list__image-france">
                        <?php echo $this->Html->image('france.png', array('alt' => 'pays-de-la-loire')); ?>
                    </div>

                    <h1 class="items_list__title">Contactez-nous</h1>
                </div>


                <?php echo $this->Session->flash(); ?>
                <?php
                echo $this->Form->create("Contact");
                echo '<div class="row-fluid" style="">';
                echo '<div class="span4">';
                echo $this->Form->input("Contact.email", array(
                    'type' => 'text',
                    'label' => __('Votre adresse e-mail *'),
                    'class' => 'span12'
                ));
                echo '</div>';
                echo '</div>';
                echo '<div class="row-fluid">';
                echo '<div class="span4">';
                echo $this->Form->input("Contact.nom", array(
                    'type' => 'text',
                    'label' => __('Votre nom *'),
                    'class' => 'span12'
                ));
                echo '</div>';
                echo '<div class="span4">';
                echo $this->Form->input("Contact.prenom", array(
                    'type' => 'text',
                    'label' => __('Votre prénom *'),
                    'class' => 'span12'
                ));
                echo '</div>';
                echo '</div>';
                echo '<div class="row-fluid">';
                echo '<div class="span4">';
                echo $this->Form->input("Contact.telephone", array(
                    'type' => 'text',
                    'label' => __('Votre numéro de téléphone'),
                    'class' => 'span12'
                ));
                echo '</div>';
                echo '<div class="span4">';
                echo $this->Form->input("Contact.societe", array(
                    'type' => 'text',
                    'label' => __('Votre société'),
                    'class' => 'span12'
                ));
                echo '</div>';
                echo '</div>';
                echo '<div class="row-fluid">';
                echo '<div class="span8">';
                echo $this->Form->input("Contact.comment", array(
                    'type' => 'textarea',
                    'label' => __('Votre message *'),
                    'class' => 'span12',
                    'rows' => 10
                ));
                echo '</div>';
                echo '</div>';
                echo '<p style="margin-top: 0;"><small>' . __("* Les champs avec astérisque sont obligatoires") . '</small></p>';
                echo $this->Form->hidden("Contact.captcha");
                echo $this->Form->submit(__('Envoyer'));
                echo $this->Form->end();
                ?>                
            </div> 
            <?php
            echo $this->Html->link('Mentions légales', array(
                'controller' => 'pages',
                'action' => 'display', 'mentions-legales'), array('class' => 'mentions-link')
            );
            ?>
        </div>

        <div class="l-default__left">

            <div class="produit-link" style="background-image: url(/files/produits/vignette/<?php
            if ($produitSaison['VignetteProduit']) {
                echo $produitSaison['VignetteProduit'][0]['filename'];
            }
            ?>) ;no-repeat;background-position: center;background-size: cover;background-repeat: no-repeat;">
                <div class="produit-link__content">
                    <h1>Un produit de saison</h1>
                    <?php if (!empty($produitSaison)): ?>
                        <?php
                        echo $this->Html->link($produitSaison['Produit']['name'], array(
                            "controller" => "produits",
                            "action" => "display",
                            $produitSaison['Produit']['slug']
                        ));
                        echo $this->Html->image('fleche.png', array('style' => 'padding: 5px 5px 30px 5px !important;'));
                        ?>
                    <?php endif; ?>
                </div>
            </div>

            <div class="recette-link">

                <ul class="recette-slider">
                    <?php // debug($recettes)  ?>
                    <?php if (!empty($recettes)): ?>
                        <?php foreach ($recettes as $recette): ?>

                            <li>

                                <div class="recette-slider-image">
                                    <div class="recette-slider-image__content recette-slider-image__content--blue">                                    

                                        <div class="recette-slider-image__content__link">
                                            <h1>Une recette de saison</h1>
                                            <?php
                                            echo $this->Html->link($recette['Recette']['name'], array(
                                                "controller" => "recettes",
                                                "action" => "display",
                                                $recette['Recette']['slug']
                                            ));
                                            echo $this->Html->image('fleche.png', array('style' => 'padding: 5px 5px 30px 5px !important;'));
                                            ?>
                                        </div>                                    
                                    </div>
                                    <?php echo $this->Html->image('/files/recettes/vignette/' . $recette['VignetteRecette'][0]['filename']); ?>      
                                </div> 

                            </li>

                        <?php endforeach; ?>
                    <?php endif; ?>
                </ul>

            </div>

        </div>


    </div> 

</div>