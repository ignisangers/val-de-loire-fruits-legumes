<?php
/**
 * TEMPLATE DE LA PAGE ACTUALITÉS
 * 
 * @param array $news : liste des actualités
 * @param string $template : nom du template actuel
 */
?>
<div class="container">
    <?php echo $this->element('Components/Site/site_breadcrumb', array('breadcrumb' => $breadcrumb)) ?>
    <section>
        <?php if (!empty($news)) { ?>
            <?php foreach ($news as $n): ?>
                <article>
                    <h1><?php echo $n['Actualite']['name']; ?></h1>
                    <time datetime="<?php echo date("Y-m-d", strtotime($n['Actualite']['created'])); ?>"><?php echo strftime("%d %B %Y", strtotime($n['Actualite']['created'])); ?></time>
                    <div>
                        <?php echo $n['Actualite']['content']; ?>
                    </div>
                    <?php
                    echo $this->Html->link('Lire la suite', array(
                        'controller' => 'actualites',
                        'action' => 'display',
                        $n['Actualite']['slug']
                    ));
                    ?>
                </article>
            <?php endforeach; ?>
        <?php } ?>
        <footer>
            <?php
            // Pagination
            echo $this->Paginator->numbers(array(
                'before' => '<ol class="pagination">',
                'after' => '</ol>',
                'tag' => 'li',
                'separator' => false
            ));
            ?>
        </footer>
    </section>
</div>
