<?php
/**
 * TEMPLATE D'UNE PAGE AGENDA
 * 
 * @param array $event : données de l'événement de l'agenda
 * @param string $template : nom du template actuel
 */
?>
<div class="container">
    <?php echo $this->element('Components/Site/site_breadcrumb', array('breadcrumb' => $breadcrumb, 'extension' => $extBreadcrumb)) ?>
    <article>
        <header>
            <time datetime="<?php echo date("Y-m-d", strtotime($event['Evenement']['created'])); ?>"><?php echo strftime("%d %B %Y", strtotime($event['Evenement']['created'])); ?></time>
        </header>
        <div>
            <?php echo $event['Evenement']['content']; ?>
        </div>
    </article>
</div>