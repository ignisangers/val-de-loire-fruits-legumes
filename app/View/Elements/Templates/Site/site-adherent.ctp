<div class="container">
    <div class="l-default">

        <div class="l-default__right">                

            <div class="items_list">
                <div class="items_list__header">
                    <div class="items_list__logo">
                        <?php echo $this->Html->image('logo.png', array('alt' => '')); ?>
                    </div>
                    <div class="items_list__image-france">
                        <?php echo $this->Html->image('france.png', array('alt' => 'pays-de-la-loire')); ?>
                    </div>

                    <h1 class="items_list__title">Nos partenaires</h1>
                </div>
                <div class="items_list__resume">
<!--                    La marque Val de Loire représente la filière des fruits et légumes auprès des partenaires, des institutions et des entreprises adhérentes. Les membres de l’association IDfel Val de Loire, font l'activité économique du Val de Loire dans le domaine des Fruits et Légumes !-->
                </div>

                <div class='files-list '>

                    <?php if (isset($adherents)) { ?>
                        <?php foreach ($adherents as $adherent): ?>


                            <div class="container-file">
                                <h1><?php echo $adherent['Partner']['name'] ?></h1>
                                <article class="file--partner grow">

                                    <div class='file__img--partner' onclick="">
                                        <a href="<?php echo $adherent['Partner']['url'] ?>"><img id="mainImg" src="<?php echo "/files/partners/" . $adherent['Partner']['logo'] ?>"></a>
<!--                                        <a href="<?php // echo $adherent['Partner']['url'] ?>"><img id="mainImg" src="<?php // echo "/www/val-de-loire-fruits-legumes/files/partners/" . $adherent['Partner']['logo'] ?>"></a>
                                    </div>-->

                                </article>
                                <?php if (!empty($adherent['Partner']['fichier'])): ?>
            <!--                                    <span><a href="/www/val-de-loire-fruits-legumes/files/partners/<?php echo $adherent['Partner']['fichier'] ?>" download="filename">Télécharger le fichier</a></span>-->
                                    <span><a href="/files/partners/<?php echo $adherent['Partner']['fichier'] ?>" download="filename">Télécharger le fichier</a></span>
                                <?php endif; ?>
                            </div>    

                        <?php endforeach; ?>
                    <?php } ?>

                    <div class="addthis_inline_share_toolbox" style="padding-top: 20px;"></div>
                </div>                

            </div>

        </div>

        <div class="l-default__left">

            <div class="produit-link" style="background-image: url(/files/produits/vignette/<?php
            if ($produitSaison['VignetteProduit']) {
                echo $produitSaison['VignetteProduit'][0]['filename'];
            }
            ?>) ;no-repeat;background-position: center;background-size: cover;background-repeat: no-repeat;">
                <div class="produit-link__content">
                    <h1>Un produit de saison</h1>
                    <?php if (!empty($produitSaison)): ?>
                        <?php
                        echo $this->Html->link($produitSaison['Produit']['name'], array(
                            "controller" => "produits",
                            "action" => "display",
                            $produitSaison['Produit']['slug']
                        ));
                        echo $this->Html->image('fleche.png', array('style' => 'padding: 5px 5px 30px 5px !important;'));
                        ?>
                    <?php endif; ?>
                </div>
            </div>

            <div class="recette-link">

                <ul class="recette-slider">
                    <?php // debug($recettes) ?>
                    <?php if (!empty($recettes)): ?>
                        <?php foreach ($recettes as $recette): ?>

                            <li>

                                <div class="recette-slider-image">
                                    <div class="recette-slider-image__content recette-slider-image__content--blue">                                    

                                        <div class="recette-slider-image__content__link">
                                            <h1>Une recette de saison</h1>
                                            <?php
                                            echo $this->Html->link($recette['Recette']['name'], array(
                                                "controller" => "recettes",
                                                "action" => "display",
                                                $recette['Recette']['slug']
                                            ));
                                            echo $this->Html->image('fleche.png', array('style' => 'padding: 5px 5px 30px 5px !important;'));
                                            ?>
                                        </div>                                    
                                    </div>
                                    <?php echo $this->Html->image('/files/recettes/vignette/' . $recette['VignetteRecette'][0]['filename']); ?>      
                                </div> 

                            </li>

                        <?php endforeach; ?>
                    <?php endif; ?>
                </ul>

            </div>

        </div>

    </div>
</div>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57d017caa3b9cce2"></script>
<?php echo $this->Html->script("responsiveslides.min", array("inline" => false)); ?>
<?php echo $this->Html->scriptStart(array("inline" => false)); ?>

$(function () {
//slider
$(".m-gallery").responsiveSlides();

$(".recette-slider").responsiveSlides({
auto: true,
speed: 2000,
timeout: 4000
});

});
<?php echo $this->Html->scriptEnd(); ?>