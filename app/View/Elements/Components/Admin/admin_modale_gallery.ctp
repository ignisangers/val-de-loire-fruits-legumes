<div class="modal-group--add" style="display: none;">
    <div class="modal-backdrop"></div>
    <div class="modal">
        <div class="modal-header">
            <h3>Rognez votre image</h3>
        </div>
        <div class="modal-body">
            <?php
            echo $this->Form->input("Gallery.caption", array(
                "label" => "Légende de votre photo (facultatif)",
                "class" => "modal-body__input"
            ));
            ?>
            <p><b>Déplacez</b> et <b>redimensionnez</b> le carré de sélection sur votre image, puis <b>validez</b>.</p>
            <div id="image_preview" class="image_preview"></div>
        </div>
        <div class="modal-footer">
            <button class="button modal-submit--add">Valider l'opération</button>
            <button class="button button--secondary" onclick="$('.modal-group--add').hide(); return false;">Annuler</button>
        </div>
    </div>
</div>
<div class="modal-group--edit" style="display: none;">
    <div class="modal-backdrop"></div>
    <div class="modal">
        <div class="modal-header">
            <h3>Modifier la légende de votre image</h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <div class="span2">
                    <?php
                    echo $this->Html->image("https://placehold.it/200x200", array(
                        "alt" => "",
                        "id" => "GalleryEditThumb",
                        "style" => "width: 100%;"
                    ));
                    ?>
                </div>
                <div class="span10">
                    <?php
                    echo $this->Form->hidden("GalleryEdit.id");
                    echo $this->Form->hidden("GalleryEdit.module_gallery_id");
                    echo $this->Form->input("GalleryEdit.caption", array(
                        "label" => "Légende de votre photo",
                        "class" => "modal-body__input"
                    ));
                    echo $this->Form->input("GalleryEdit.filename", array(
                        "label" => "Changer de photo",
                        "type" => "file"
                    ));
                    ?>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button class="button modal-submit--edit">Valider</button>
            <button class="button button--secondary" onclick="$('.modal-group--edit').hide(); return false;">Annuler</button>
        </div>
    </div>
</div>