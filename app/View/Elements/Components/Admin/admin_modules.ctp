<?php
/**
 * Affichage des modules d'une zone dans l'administration
 * 
 * On boucle les modules de la zone et on appelle l'affichage spécifique à chaque module
 * On va également inclure les boutons d'actions permettant de :
 * - modifier l'ordre des modules (vers le haut/vers le bas)
 * - modifier le module
 * - supprimer le module
 * 
 * @param array $modules : liste des modules de la zone
 * 
 * $module['model'] -> Model du module passé par Inflector::tableize (ex. : module_liens, module_articles...)
 * $module['id'] -> ID de la table Content
 * $module['page_id'] -> ID de la page courante
 * $module['zone'] -> Numéro de la zone pour laquelle on affiche les modules
 * $module['content'] -> Contenu du module + contenus models associés le cas échéant (ex. : $module['content']['id'])
 */
if (empty($modules)) {
    ?>
    <div class="module">
        <p style="margin: 0; padding: 12px;">Aucun module trouvé.</p>
    </div>
    <?php
} else {
    $i = 1;
    foreach ($modules as $rang => $module):
        $model = Inflector::classify($module['model']);
        echo '<div class="module ' . $module['model'] . ' clearfix">';
            // Liens d'actions
            echo '<aside>';
                echo '<ul>';
                    echo '<li class="edit">';
                        echo $this->Html->link("Modifier", array(
                            'controller' => $module['model'],
                            'action' => 'edit',
                            $module['content'][$model]['id'],
                            $module['page_id']
                        ), array('title' => 'Modifier'));
                    echo '</li>';
                    echo '<li class="remove">';
                        echo $this->Html->link("Supprimer", array(
                            'controller' => 'contents',
                            'action' => 'delete',
                            $module['page_id'],
                            $module['zone'],
                            $rang,
                            $module['id']
                        ), array('title' => 'Supprimer'), "Supprimer ce module ?");
                    echo '</li>';
                    echo '<li class="up">';
                        if ($i == 1) {
                            echo $this->Html->link("Haut", '#', array('title' => 'Haut', 'class' => 'disabled'));
                        } else {
                            echo $this->Html->link("Haut", array(
                                'controller' => 'contents',
                                'action' => 'move',
                                $module['id'],
                                $module['page_id'],
                                $module['zone'],
                                $rang,
                                'up'
                            ), array('title' => 'Haut'));
                        }
                    echo '</li>';
                    echo '<li class="down">';
                        if ($i == count($modules)) {
                            echo $this->Html->link("Bas", '#', array('title' => 'Bas', 'class' => 'disabled'));
                        } else {
                            echo $this->Html->link("Bas", array(
                                'controller' => 'contents',
                                'action' => 'move',
                                $module['id'],
                                $module['page_id'],
                                $module['zone'],
                                $rang,
                                'down'
                            ), array('title' => 'Bas'));
                        }
                    echo '</li>';
                echo '</ul>';
            echo '</aside>';
            // Affichage spécifique du module
            echo '<section>' . $this->element("Modules/Admin/admin_" . $module['model'], array('module' => $module['content'])) . '</section>';
        echo '</div>';
        $i++;
    endforeach;
}