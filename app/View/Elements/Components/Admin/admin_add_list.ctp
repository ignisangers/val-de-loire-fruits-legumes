<?php
/**
 * Liste des modules pouvant être ajoutés à une zone de modules
 * (lien vers les pages d'ajout d'un module)
 * 
 * Utile pour contraindre les choix des utilisateurs et empêcher des cas non prévus par les maquettes du webdesigner
 * 
 * $listModules['modules'] -> liste complète des modules de l'application ([Model] => [Nom du module], ex. : "ModuleLien" => "Module lien")
 * $listModules['collections'] -> liste des collections paramétrée dans /app/Model/Module.php
 * $listModules['page_id'] -> ID de la page courante
 * $collection -> collection de modules souhaitée pour cette zone (facultatif, si non renseigné on affichera la liste complète des modules) 
 * $zone -> zone courante, pour laquelle on va proposer une liste de modules à ajouter
 */
?>
<div class="btn-group dropdown" style="float: right;">
    <a class="dropdown-toggle add-module" data-toggle="dropdown" href="#"><?php echo $this->Html->image('admin/add-module.png', array('alt' => 'Ajouter')); ?></a>
    <ul class="dropdown-menu pull-right">
        <li class="nav-header">Ajouter un module</li>
        <?php
        // On définit la liste des modules autorisés pour cette zone
        $authModules = array();
        if (isset($collection) && array_key_exists($collection, $listModules["collections"])) { // On veut contraindre la liste des modules disponibles
            foreach ($listModules["collections"][$collection] as $authModule):
                $authModules[$authModule] = $listModules["modules"][$authModule];
            endforeach;
        } else { // On veut la liste complète des modules
            $authModules = $listModules["modules"];
        }

        // On affiche la liste des liens des modules souhaités
        foreach($authModules as $mModel => $mName):
            echo '<li>';
                echo $this->Html->link($mName, array(
                    'controller' => Inflector::tableize($mModel),
                    'action' => 'add',
                    $listModules['page_id'],
                    $zone
                ), array('tabindex' => -1));
            echo '</li>';
        endforeach;
        ?>
    </ul>
</div>