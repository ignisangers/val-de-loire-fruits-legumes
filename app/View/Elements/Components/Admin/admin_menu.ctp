<ul id="nav-home">
    <?php $current = ($this->params['controller'] == 'dashboards') ? 'current' : ''; ?>
    <li class="home <?php echo $current; ?>">
        <?php
        echo $this->Html->link($this->Session->read('Auth.User.prenom') . ' ' . mb_strtoupper($this->Session->read('Auth.User.nom'), 'UTF-8'), array(
            'controller' => 'dashboards',
            'action' => 'index'
        ));
        ?>
    </li>
<!--    <li class="languages">
        <?php
//        echo $this->Html->link($this->Html->image('admin/flags/' . Configure::read('Config.language') . '.png'), '#', array(
//            'escape' => false,
//            'id' => 'languages'
//        ));
        ?>
    </li>-->
</ul>
<ul id="nav-main">
    <?php // $current = ($this->params['controller'] == 'pages' || preg_match('#module_#', $this->params['controller'])) ? 'current' : ''; ?>
    <!--<li class="pages <?php // echo $current; ?>">-->
        <?php
//        echo $this->Html->link('Pages', array(
//            'controller' => 'pages',
//            'action' => 'index'
//        ));
        ?>
    <!--</li>-->
    <?php $current = ($this->params['controller'] == 'produits') ? 'current' : ''; ?>
    <li class="actualites <?php echo $current; ?>">
        <?php
        echo $this->Html->link('Produits', array(
            'controller' => 'produits',
            'action' => 'index'
        ));
        ?>
    </li>
    <?php $current = ($this->params['controller'] == 'recettes') ? 'current' : ''; ?>
    <li class="actualites <?php echo $current; ?>">
        <?php
        echo $this->Html->link('Recettes', array(
            'controller' => 'recettes',
            'action' => 'index'
        ));
        ?>
    </li>
    <?php // $current = ($this->params['controller'] == 'actualites') ? 'current' : ''; ?>
    <!--<li class="actualites <?php // echo $current; ?>">-->
        <?php
//        echo $this->Html->link('Actualités', array(
//            'controller' => 'actualites',
//            'action' => 'index'
//        ));
        ?>
    <!--</li>-->
    <?php $current = ($this->params['controller'] == 'partners') ? 'current' : ''; ?>
    <li class="actualites <?php echo $current; ?>">
        <?php
        echo $this->Html->link('Adherents', array(
            'controller' => 'partners',
            'action' => 'index'
        ));
        ?>
    </li>
    <?php // $current = ($this->params['controller'] == 'evenements') ? 'current' : ''; ?>
<!--    <li class="evenements <?php // echo $current; ?>">
        <?php
//        echo $this->Html->link('Agenda', array(
//            'controller' => 'evenements',
//            'action' => 'index'
//        ));
        ?>
    </li>-->
    <?php $current = ($this->params['controller'] == 'files') ? 'current' : ''; ?>
    <li class="files <?php echo $current; ?>">
        <?php
        echo $this->Html->link('Le coin des enfants', array(
            'controller' => 'files',
            'action' => 'index'
        ));
        ?>
    </li>
    <?php // $current = ($this->params['controller'] == 'trombinoscopes') ? 'current' : ''; ?>
    <!--<li class="trombinoscopes <?php // echo $current; ?>">-->
        <?php
//        echo $this->Html->link('Trombinoscope', array(
//            'controller' => 'trombinoscopes',
//            'action' => 'index'
//        ));
//        ?>
    <!--</li>-->
    <?php $current = ($this->params['controller'] == 'groupes') ? 'current' : ''; ?>
<!--    <li class="groupes <?php echo $current; ?>">
        <?php
//        echo $this->Html->link('Groupes', array(
//            'controller' => 'groupes',
//            'action' => 'index'
//        ));
        ?>
    </li>-->
    <?php $current = ($this->params['controller'] == 'users') ? 'current' : ''; ?>
    <li class="users <?php echo $current; ?>">
        <?php
        echo $this->Html->link('Utilisateurs', array(
            'controller' => 'users',
            'action' => 'index'
        ));
        ?>
    </li>
    <?php // $current = ($this->params['controller'] == 'media') ? 'current' : ''; ?>
<!--    <li class="medias <?php // echo $current; ?>">
    <?php
//        echo $this->Html->link('Médias', array(
//            'controller' => 'media',
//            'action' => 'index'
//        ));
    ?>
    </li>-->
    <?php // $current = ($this->params['controller'] == 'helps') ? 'current' : ''; ?>
<!--    <li class="helps <?php echo $current; ?>">
        <?php
//        echo $this->Html->link('Aide', array(
//            'controller' => 'helps',
//            'action' => 'index'
//        ));
        ?>
    </li>-->
    <li class="logout">
        <?php
        echo $this->Html->link('Déconnexion', array(
            'controller' => 'users',
            'action' => 'logout'
        ));
        ?>
    </li>
</ul>