<?php
/**
 * Menu principal du site
 * 
 * @param array $pages : liste des pages à afficher dans le menu
 * @param int $pageActive : page courante à activer dans le menu
 * @param boolean $main : est-ce le premier niveau de menu ? (facultatif)
 */
?>
<ul class='nav nav-pills pull-right<?php
if (isset($main)) {
    echo " main";
}
?>'>
    <li class="menu-button-container-small">
        <a  href="#">
            <?php
            echo $this->Html->image('menu-button.png', array(
                'style' => 'margin-right:10px',
                'class' => 'menu-button-small'
            ))
            ?>
        </a>
    </li>
    <?php
    foreach ($pages as $page):
        // Est-ce la page à activer ?
        $class = ($pageActive === $page['Page']['id']) ? 'active' : '';

        // Gestion URL du lien
        if ($page['Page']['home']) { // Page d'accueil
            $href = array(
                "controller" => "pages",
                "action" => "index"
            );
        } else if ($page['PageType']['slug'] !== "modules") { // Page spécifique sans gestion de modules
            $href = array(
                "controller" => $page['PageType']['controller'],
                "action" => $page['PageType']['action']
            );
        } else {
            $href = array(
                'controller' => 'pages',
                'action' => 'display',
                $page['Page']['alias']
            );
        }
        ?>
        <li class="menu-item">
            <?php
            // Le lien
            echo $this->Html->link($page['Page']['name'], $href, array('class' => $class));

            // Y a-t-il des enfants à afficher ?
            if (!empty($page['children'])) {
                echo $this->element("Components/Site/site_menu", array("pages" => $page["children"], "pageActive" => $pageActive));
            }
            ?>
        </li>        
        <?php
    endforeach;
    ?>
    <!--<li class="menu-item">-->
        <?php
//        echo $this->Html->link("Le coin des enfants", array(
//            'controller' => 'pages',
//            'action' => 'display',
//            $page['Page']['alias']
//        ));
//        ?>
    <!--</li>-->        
    <li class="menu-button-container">
        <a href="#">
            <?php
            echo $this->Html->image('menu-button.png', array(
                'style' => 'margin-right:10px',
                'class' => 'menu-button'
            ))
            ?>
        </a>
    </li>
</ul>
<?php echo $this->Html->scriptStart(array("inline" => false)); ?>  

$(".menu-button").click(function (){

$('.menu-item').toggle("slow");
});
$(".menu-button-small").click(function (){
$('.menu-item').toggle("slow");
});
<?php echo $this->Html->scriptEnd(); ?>