<?php
/**
 * Affichage récursif du plan du site
 * Élément appelé par /app/View/Elements/Templates/Site/site-sitemap.ctp
 * 
 * @param array $children : liste de pages avec enfants
 */
?>
<ul>
    <?php
    foreach ($children as $child):
        // Gestion URL du lien
        if ($child['Page']['home']) { // Page d'accueil
            $href = array(
                'controller' => 'pages',
                'action' => 'index',
            );
        } else if ($child['PageType']['slug'] !== "modules") { // Page spécifique sans gestion de modules
            $href = array(
                'controller' => $child['PageType']['controller'],
                'action' => $child['PageType']['action'],
            );
        } else { // Page normale
            $href = array(
                'controller' => 'pages',
                'action' => 'display',
                $child['Page']['alias']
            );
        }
        ?>
        <li>
            <?php
            // Affichage du lien
            echo $this->Html->link($child['Page']['name'], $href);
            
            // Y a-t-il des enfants à afficher ?
            if(!empty($child['children'])) {
                echo $this->element("Components/Site/site_sitemap", array("children" => $child["children"]));
            }
            ?>
        </li>
    <?php endforeach; ?>
</ul>