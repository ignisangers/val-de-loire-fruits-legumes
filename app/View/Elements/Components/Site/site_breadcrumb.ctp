<?php
/**
 * Fil d'ariane
 * 
 * Les données du fil d'ariane sont traitées dans AppController::constructPage()
 * Le fil récupère le parcours à travers les pages (et uniquement les pages) depuis l'accueil jusqu'à la page courante
 * 
 * @param array $breadcrumb : parcours des pages du plus haut niveau d'arborescence jusqu'au plus profond
 * @param array $extension : liens supplémentaires pour contenus hors pages (page détail d'une actualité par exemple), facultatif
 */
?>
<nav id="breadcrumb">
    <ul class="clearfix">
        <?php
        foreach($breadcrumb as $key => $fil):
            echo ($key == (count($breadcrumb)-1) && !isset($extension)) ? '<li class="active">': '<li>';
                if($fil['Page']['home']) { // Page d'accueil
                    echo $this->Html->link($fil['Page']['name'], array(
                        'controller' => 'pages',
                        'action' => 'index'
                    ));
                } else if($fil['PageType']['slug'] !== "modules") { // Page spécifique sans gestion de modules
                    echo $this->Html->link($fil['Page']['name'], array(
                        'controller' => $fil['PageType']['controller'],
                        'action' => $fil['PageType']['action']
                    ));
                } else {
                    echo $this->Html->link($fil['Page']['name'], array(
                        'controller' => 'pages',
                        'action' => 'display',
                        $fil['Page']['alias']
                    ));
                }
            echo '</li>';
        endforeach;
        
        if (isset($extension)) {
            foreach ($extension as $key => $ext):
                echo ($key == count($extension)-1) ? '<li class="active">' : '<li>';
                    echo $this->Html->link($ext['name'], $ext['link']);
                echo '</li>';
            endforeach;
        }
        ?>
    </ul>
</nav>
