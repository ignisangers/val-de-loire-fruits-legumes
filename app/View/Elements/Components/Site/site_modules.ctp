<?php
/**
 * Affichage des modules d'une zone
 * 
 * Ici on se contente de boucler les modules de la zone et d'appeler l'affichage spécifique à chaque module
 * Pas de style ni de mise en page ici !
 * 
 * @param array $modules : liste des modules de la zone
 * 
 * $modules[x]['model'] -> Model du module passé par Inflector::tableize (ex. : module_liens, module_articles...)
 * $modules[x]['content'] -> Contenu du module + contenus models associés le cas échéant (ex. : $module['content']['id'])
 * $template -> Nom du template courant
 */
$template = (isset($template)) ? $template : "";

if (!empty($modules)) {
    foreach ($modules as $module):
        // Existe-t-il une vue du module spécifique à ce template ?
        $moduleElementPath = "Modules/Site/site_";
        $moduleSpecificFile = ROOT . DS . APP_DIR . DS . "View" . DS . "Elements" . DS . $moduleElementPath . $module['model'] . "_" . $template . ".ctp";
        $moduleView = (file_exists($moduleSpecificFile)) ? $module['model'] . "_" . $template : $module['model'];
        echo $this->element($moduleElementPath . $moduleView, array('module' => $module['content']));
    endforeach;
}