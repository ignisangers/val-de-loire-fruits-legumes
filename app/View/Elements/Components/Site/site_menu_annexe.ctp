<?php
/**
 * Menu annexe du site
 * 
 * Liste de liens hors menu principal, permettant généralement d'afficher des liens de contact, mentions légales, retour en haut...
 * Affiché depuis un template : /app/View/Templates/
 * 
 * @param array $annexes : liste des pages du menu annexe
 */
foreach ($annexes as $annexe):
    ?>
    <li>
        <?php
        // Composition de l'URL
        if ($annexe['Page']['home']) { // Page d'accueil
            $href = array(
                'controller' => 'pages',
                'action' => 'index'
            );
        } else if ($annexe['PageType']['slug'] !== "modules") { // Page spécifique sans gestion de modules
            $href = array(
                'controller' => $annexe['PageType']['controller'],
                'action' => $annexe['PageType']['action']
            );
        } else { // Page normale
            $href = array(
                'controller' => 'pages',
                'action' => 'display',
                $annexe['Page']['alias']
            );
        }
        // Affichage du lien
        echo $this->Html->link($annexe['Page']['name'], $href);
        ?>
    </li>
    <?php
endforeach;
