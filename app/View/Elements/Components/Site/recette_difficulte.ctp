<?php if ((int)$recette['Recette']['difficulte'] == 1): ?>
<ul class="recette-difficulte">
        <li>
            <?php echo $this->Html->image('star.png', array('style' => 'width:25px')); ?>
        </li>
        <li>
            <?php echo $this->Html->image('star-copie.png', array('style' => 'width:25px')); ?>
        </li>
        <li>
            <?php echo $this->Html->image('star-copie.png', array('style' => 'width:25px')); ?>
        </li>
        <li>
            <?php echo $this->Html->image('star-copie.png', array('style' => 'width:25px')); ?>
        </li>
    </ul>
<?php endif; ?>


<?php if ((int)$recette['Recette']['difficulte'] == 2): ?> 
    <ul class="recette-difficulte">
        <li>
            <?php echo $this->Html->image('star.png', array('style' => 'width:25px')); ?>
        </li>
        <li>
            <?php echo $this->Html->image('star.png', array('style' => 'width:25px')); ?>
        </li>
        <li>
            <?php echo $this->Html->image('star-copie.png', array('style' => 'width:25px')); ?>
        </li>
        <li>
            <?php echo $this->Html->image('star-copie.png', array('style' => 'width:25px')); ?>
        </li>
    </ul>
<?php endif; ?>

<?php if ((int)$recette['Recette']['difficulte'] == 3): ?> 
    <ul class="recette-difficulte">
        <li>
            <?php echo $this->Html->image('star.png', array('style' => 'width:25px')); ?>
        </li>
        <li>
            <?php echo $this->Html->image('star.png', array('style' => 'width:25px')); ?>
        </li>
        <li>
            <?php echo $this->Html->image('star.png', array('style' => 'width:25px')); ?>
        </li>
        <li>
            <?php echo $this->Html->image('star-copie.png', array('style' => 'width:25px')); ?>
        </li>
    </ul>
<?php endif; ?>

<?php if ((int)$recette['Recette']['difficulte'] == 4): ?> 
    <ul class="recette-difficulte">
        <li>
            <?php echo $this->Html->image('star.png', array('style' => 'width:25px')); ?>
        </li>
        <li>
            <?php echo $this->Html->image('star.png', array('style' => 'width:25px')); ?>
        </li>
        <li>
            <?php echo $this->Html->image('star.png', array('style' => 'width:25px')); ?>
        </li>
        <li>
            <?php echo $this->Html->image('star.png', array('style' => 'width:25px')); ?>
        </li>
    </ul>
<?php endif; ?>