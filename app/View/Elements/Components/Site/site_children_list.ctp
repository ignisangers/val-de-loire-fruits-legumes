<?php
/**
 * Élément utilisé par les templates "interieur-enfants.ctp" et "interieur-parent.ctp"
 * On affiche la liste des pages du même niveau ("interieur-enfants") ou les pages enfants directes ("interieur-parent")
 * Cet affichage est utilisé pour afficher les données traitées dans AppController::template_interieur_enfants() et AppController::template_interieur_parent()
 */
if(isset($children) && !empty($children)) {
    ?>
    <nav>
        <h1>Consultez également...</h1>
        <ul>
            <?php foreach($children as $child): ?>
                <li>
                    <?php
                    // Est-ce une page la activer ?
                    $class = ($child['Page']['id'] == $id) ? 'current': '';
                    
                    // Gestion du lien de la page
                    if($child['Page']['home']) { // Page d'accueil
                        $href = array(
                            'controller' => 'pages',
                            'action' => 'index'
                        );
                    } else if($child['PageType']['slug'] !== "modules") { // Page spécifique sans gestion de modules
                        $href = array(
                            'controller' => $child['PageType']['controller'],
                            'action' => $child['PageType']['action']
                        );
                    } else { // Page normale
                        $href = array(
                            'controller' => 'pages',
                            'action' => 'display',
                            $child['Page']['alias']
                        );
                    }
                    // Affichage du lien
                    echo $this->Html->link($child['Page']['name'], $href, array('class' => $class));
                    ?>
                </li>
            <?php endforeach; ?>
        </ul>
    </nav>
<?php
}