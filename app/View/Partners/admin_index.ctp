<?php
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

// Titrage + boutons d'actions + sous-nav
echo $this->start('header');
    echo '<h1>'. $title_for_layout .'</h1>';
    echo '<nav class="clearfix">';
        echo '<ul id="sous-nav" class="clearfix">';
            echo '<li>';
                echo '<p>Gestion de la liste des partenaires en bas de page.</p>';
            echo '</li>';
        echo '</ul>';
        echo '<ul id="nav-actions" class="clearfix">';
            echo '<li>';
                echo $this->Html->link('Ajouter un partenaire', array(
                    'action' => 'add'
                ));
            echo '</li>';
        echo '</ul>';
    echo '</nav>';
echo $this->end();
?>

<h2 class="add">Liste des partenaires</h2>
<?php echo $this->Session->flash(); ?>
<section>
    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="liste">
        <thead>
            <tr>
                <th>Logo</th>
                <th>Intitulé</th>
                <th>Site web</th>
                <th>&nbsp;</th>
                <th colspan="2">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if(empty($partners)) {
                echo '<tr><td colspan="5">Aucun enregistrement. '. $this->Html->link("Ajouter un partenaire", array("action" => "add")) .'</td></tr>';
            } else {
                foreach($partners as $k => $partner):
                    echo '<tr>';
                        echo '<td width="100">';
                            $logo = $partner['Partner']['logo'];
                            $dirLogo = 'files/partners/' . $logo;
                            if (!empty($logo)) {
                                $img = new File($dirLogo);
                                $imgSize = getimagesize($img->path);
                                $height = 44;
                                $ratio = $imgSize[0] / $imgSize[1];

                                echo $this->Html->image('/' . $dirLogo, array(
                                    'alt' => '',
                                    'height' => $height,
                                    'width' => $height * $ratio
                                ));
                            } else {
                                echo '&nbsp;';
                            }
                        echo '</td>';
                        echo '<td>'. $partner['Partner']['name'] .'</td>';
                        echo '<td><small>';
                            if (!empty($partner['Partner']['url'])) {
                                echo $this->Html->link($partner['Partner']['url'], $partner['Partner']['url'], array(
                                    'target' => '_blank'
                                ));
                            } else {
                                echo '&nbsp;';
                            }
                        echo '</small></td>';
                        echo '<td width="36" class="partners sortable">';
                            $classUp = ($k === 0) ? 'sortable__up sortable__up--disabled' : 'sortable__up';
                            $classDown = ($k === (count($partners)-1)) ? 'sortable__down sortable__down--disabled' : 'sortable__down';
                            echo $this->Html->link('Haut', array('action' => 'move', $partner['Partner']['id'], 'up'), array('class' => $classUp, 'title' => "Monter la page d'un cran"));
                            echo $this->Html->link('Bas', array('action' => 'move', $partner['Partner']['id'], 'down'), array('class' => $classDown, 'title' => "Descendre la page d'un cran"));
                        echo '</td>';
                        echo '<td width="100" class="btn-action">'. $this->Html->link('Modifier', array(
                            "action" => "edit",
                            $partner['Partner']['id']
                        )) .'</td>';
                        echo '<td width="100" class="btn-action">'. $this->Html->link('Supprimer', array(
                            "action" => "delete",
                            $partner['Partner']['id']
                        ), array(), "Supprimer le partenaire : ". $partner['Partner']['name'] ." ?") .'</td>';
                    echo '</tr>';
                endforeach;
            }
            ?>
        </tbody>
    </table>
</section>

<?php echo $this->Html->scriptStart(array('inline' => false)); ?>
    (function ($) {
        $('.sortable__up--disabled, .sortable__down--disabled').click(function () {
            return false;
        });
    }(jQuery));
<?php echo $this->Html->scriptEnd();
