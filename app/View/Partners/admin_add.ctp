<?php echo $this->Form->create('Partner', array('type' => 'file')); ?>
<?php
// Titrage + boutons d'actions + sous-nav
echo '<header>';
echo '<h1>' . $title_for_layout . '</h1>';
echo '<nav class="clearfix">';
echo '<ul id="sous-nav" class="clearfix">';
echo '<li>';
echo $this->Html->link('Nouveau partenaire', array(
    'action' => 'add'
        ), array('class' => 'current'));
echo '</li>';
echo '</ul>';
echo '<ul id="nav-actions" class="clearfix">';
echo '<li class="other">';
echo $this->Html->link('Retour', array(
    'controller' => 'partners',
    'action' => 'index'
));
echo '</li>';
echo '<li>';
echo $this->Form->submit('Enregistrer');
echo '</li>';
echo '</ul>';
echo '</nav>';
echo '</header>';
?>

<?php
echo $this->Form->input('Partner.id');
echo '<div><div>';
?>
<?php echo $this->Session->flash(); ?>
<div class="row-fluid">
    <div class="span6">
        <h2>Informations générales</h2>
        <fieldset class="block">
            <?php
            echo $this->Form->input('Partner.name', array('label' => 'Nom du partenaire *', 'class' => 'span12'));
            echo $this->Form->input('Partner.url', array('type' => 'url', 'label' => 'Site web', 'class' => 'span12', 'value' => 'http://'));
            ?>
        </fieldset>
    </div>
    <div class="span6">
        <h2>Logo</h2>
        <fieldset class="block">
            <div class="row-fluid">
                <div style="background: url(http://placehold.it/300x200) no-repeat left top; float: left;">
                    <?php
                    echo $this->Html->image("http://placehold.it/300x200", array(
                        'alt' => ''
                    ));
                    ?>
                </div>
                <div style="margin-left: 120px;">
                    <?php
                    echo $this->Form->input('Partner.logo', array(
                        'label' => 'Changer le logo',
                        'type' => 'file'
                    ));
                    ?>
                </div>
            </div><br>
            <small>L'image sera redimensionnée à 100px de large. <br>Formats autorisés : <b>.jpg</b>, <b>.jpeg</b>, <b>.gif</b>, <b>.png</b>.</small>
        </fieldset>
    </div>
</div>
<div class="row-fluid">
    <div class="span6">
    </div>
    <div class="span6">
        <h2>Fichier</h2>
        <fieldset class="block">
            <div class="row-fluid">
                <?php echo $this->Form->input('Partner.fichier', array('label' => 'Fichier', 'class' => 'span6', 'type' => 'file')); ?>
            </div>
        </fieldset>
    </div>
</div>
<?php echo '</div></div>'; ?>
<?php echo $this->Form->end(); ?>
