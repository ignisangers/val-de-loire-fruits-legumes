<?php echo $this->Form->create('ModulePicture', array('type' => 'file')); ?>
    <?php
    // Titrage + boutons d'actions + sous-nav
    echo '<header>';
        echo '<h1>Gestion des pages : '. $this->Html->link($page['Page']['name'], array(
            'controller' => 'pages',
            'action' => 'edit_modules',
            $page_id
        )) .'</h1>';
        echo '<nav class="clearfix">';
            echo '<ul id="sous-nav" class="clearfix">';
                echo '<li>';
                    echo $this->Html->link('Votre image', $this->request->here, array('class' => 'current'));
                echo '</li>';
            echo '</ul>';
            echo '<ul id="nav-actions" class="clearfix">';
                echo '<li class="other">';
                    echo $this->Html->link('Retour', array(
                        'controller' => 'pages',
                        'action' => 'edit_modules',
                        $page_id
                    ));
                echo '</li>';
                echo '<li>';
                    echo $this->Form->submit('Enregistrer');
                echo '</li>';
            echo '</ul>';
        echo '</nav>';
    echo '</header>';
    ?>

    <?php
    echo $this->Form->input('ModulePicture.id');
    echo '<div><div>';
    ?>
        <?php echo $this->Session->flash(); ?>
        <div class="row-fluid">
            <div class="span12">
                <h2>Chargez une image (la plus large possible)</h2>
                <fieldset class="block">
                    <div class="row-fluid">
                        <div class="span8">
                            <?php echo $this->Form->input('ModulePicture.image', array('label' => false, 'type' => 'file')); ?>
                            <div style="margin: 15px 0 0 15px; width: auto;">
                                <?php
                                echo $this->Form->input('ModulePicture.full', array(
                                    'type' => 'checkbox',
                                    'label' => "L'image occupe toute la largeur disponible"
                                ));
                                ?>
                            </div>
                        </div>
                        <div class="span4">
                            <?php echo $this->Html->image("../medias/" . $this->request->data["ModulePicture"]["image"], array('alt' => "Image", "style" => "width: 100%;")) ?>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <h2>Renseignez-la (facultatif)</h2>
                <fieldset class="block">                    
                    <?php
                    echo $this->Form->input('ModulePicture.name', array('label' => 'Titre de l\'image', 'class' => 'span8'));
                    echo $this->Form->input('ModulePicture.caption', array('label' => 'Sous-titre de l\'image', 'class' => 'span8'));
                    echo $this->Form->input('ModulePicture.content', array('label' => 'Description', 'class' => 'redactor span12'));
                    ?>
                </fieldset>
            </div>
        </div>
    <?php echo '</div></div>'; ?>
<?php echo $this->Form->end(); ?>
<?php echo $this->Html->script('ckeditor/ckeditor.js', array('inline' => false)); ?>

<?php echo $this->Html->scriptStart(array('inline' => false)); ?>
    $(function () {
        $('.redactor').redactor({
            minHeight: 200,
            lang: 'fr',
            buttons: ['formatting', '|', 'bold', 'italic', 'underline', 'deleted', '|', 'unorderedlist', 'orderedlist', '|', 'outdent', 'indent', 'link', 'alignment', '|', 'image', 'video'],
            imageUpload: '<?php echo Router::url(array("controller" => "images", "action" => "upload_image_redactor", "admin" => true), true); ?>',            imageUploadErrorCallback: function(json) {
                alert(json.message);
            }, 
            linebreaks: true,
            dragUpload: true
        });
    });
<?php
echo $this->Html->scriptEnd(); ?>
