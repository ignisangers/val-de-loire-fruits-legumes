<!-- Ajout du datepicker -->
<?php echo $this->Html->css('pickadate/classic'); ?>
<?php echo $this->Html->css('pickadate/classic.date'); ?>
<?php echo $this->Html->css('pickadate/classic.time'); ?>
<?php echo $this->Html->script('pickadate/picker'); ?>
<?php echo $this->Html->script('pickadate/picker.date'); ?>
<?php echo $this->Html->script('pickadate/picker.time'); ?>
<?php echo $this->Html->script('pickadate/fr_FR'); ?>
<style>
    input[readonly].datepicker, input[readonly].timepicker { cursor: pointer; }
</style>

<?php echo $this->Form->create('Evenement', array('type' => 'file')); ?>
    <!-- // Titrage + boutons d'actions + sous-nav -->
    <header>
        <h1>Ajouter un évènement</h1>
        <nav class="clearfix">
            <ul id="nav-actions" class="clearfix">
                <li class="other">
                    <?php echo $this->Html->link('Retour', array(
                        'controller' => 'evenements',
                        'action' => 'index',
                        'admin' => true,
                    )); ?>
                </li>
                <li>
                    <?php echo $this->Form->submit('Enregistrer'); ?>
                </li>
            </ul>
        </nav>
    </header>
    
    <div>
        <?php echo $this->Session->flash(); ?>
        <div class="row-fluid">
            <div class="span12">
                <h2>Rédigez votre évènement</h2>
                <fieldset class="block">
                    <?php echo $this->Form->input('Evenement.debut_date', array('label' => false, 'class' => 'span2 datepicker', 'div' => false, 'placeholder' => "Date de début")); ?>
                    <?php echo $this->Form->input('Evenement.debut_heure', array('label' => false, 'class' => 'span2 timepicker', 'div' => false, 'placeholder' => "Heure de début")); ?>
                    (Heure facultative)
                    <br>
                    <?php echo $this->Form->input('Evenement.fin_date', array('label' => false, 'class' => 'span2 datepicker', 'div' => false, 'placeholder' => "Date de fin")); ?>
                    <?php echo $this->Form->input('Evenement.fin_heure', array('label' => false, 'class' => 'span2 timepicker', 'div' => false, 'placeholder' => "Heure de fin")); ?>
                    (Date et heure de fin facultatives)
                    <br>
                    <?php echo $this->Form->input('Evenement.content', array('label' => "Contenu", 'class' => 'redactor span12')); ?>
                </fieldset>
            </div>
        </div>
    </div>
<?php echo $this->Form->end(); ?>

<?php echo $this->Html->scriptStart(array('inline' => false)); ?>
    $(function () {
        $('.redactor').redactor({
            minHeight: 200,
            lang: 'fr',
            buttons: ['formatting', '|', 'bold', 'italic', 'underline', 'deleted', '|', 'unorderedlist', 'orderedlist', '|', 'outdent', 'indent', 'link', 'alignment', '|', 'image', 'video'],
            imageUpload: '<?php echo Router::url(array("controller" => "images", "action" => "upload_image_redactor", "admin" => true), true); ?>',            imageUploadErrorCallback: function(json) {
                alert(json.message);
            },
            linebreaks: true,
            dragUpload: true
        });
        
        $('.datepicker').pickadate();
        $('.timepicker').pickatime({
            format: 'H:i',
            formatSubmit: 'HH:i:00',
            interval: 15
        });
    });
<?php echo $this->Html->scriptEnd(); ?>