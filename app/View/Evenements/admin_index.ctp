<?php
// Titrage + boutons d'actions + sous-nav
echo $this->start('header'); ?>
    <h1>Gestion des évènements</h1>
    <nav class="clearfix">
        <ul id="sous-nav" class="clearfix">
            <li>
                <p>Rédigez les évènements de votre site</p>
            </li>
        </ul>
        <ul id="nav-actions" class="clearfix">
            <li><?php echo $this->Html->link('Ajouter un évènement', array('action' => 'add')); ?>
            </li>
        </ul>
    </nav>
<?php echo $this->end(); ?>

<h2 class="add">Liste des évènements</h2>

<?php echo $this->Session->flash(); ?>

<ul class="pages-sortable">
<?php foreach($evenements as $k => $evenement): ?>

    <?php echo (($k + 1) == sizeof($evenements)) ? '<li class="last">' : '<li>'; ?>
        <p>
            <span>
                <?php
                if (!empty($evenement['Evenement']['date_fin'])) {
                    echo "Du " . date('d/m/Y', strtotime($evenement['Evenement']['date_debut']));
                    if (date('H:i', strtotime($evenement['Evenement']['date_debut'])) !== '00:00') {
                        echo " à " . date('H:i', strtotime($evenement['Evenement']['date_debut']));
                    }
                    echo " au " . date('d/m/Y', strtotime($evenement['Evenement']['date_fin']));
                    if (date('H:i', strtotime($evenement['Evenement']['date_fin'])) !== '00:00') {
                        echo " à " . date('H:i', strtotime($evenement['Evenement']['date_fin']));
                    }
                } else {
                    echo "Le " . date('d/m/Y', strtotime($evenement['Evenement']['date_debut']));
                    if (date('H:i', strtotime($evenement['Evenement']['date_debut'])) !== '00:00') {
                        echo " à " . date('H:i', strtotime($evenement['Evenement']['date_debut']));
                    }
                }
                echo " : ";
                echo $this->Text->truncate(strip_tags($evenement['Evenement']['content'], 50)); ?>
            </span>
        </p>
        <ul>
            <?php if(AuthComponent::user('role') == "admin"): ?>
                <li class="hide"><?php echo $this->Html->link('Modifier', array('action' => 'edit', $evenement['Evenement']['id'])); ?></li>
                <li class="hide"><?php echo $this->Html->link('Supprimer', array('action' => 'delete', $evenement['Evenement']['id']), array(), "Supprimer cet évènement ?"); ?></li>
            <?php endif; ?>
            <li class="options"><?php echo $this->Html->link('Options', "#"); ?></li>
        </ul>
    </li>
<?php endforeach; ?>
</ul>

<?php echo $this->Paginator->numbers(array('first' => 3, 'last' => 3)); ?>