<?php echo $this->Form->create('Actualite', array('type' => 'file')); ?>
    <!-- // Titrage + boutons d'actions + sous-nav -->
    <header>
        <h1>Modifier une actualité</h1>
        <nav class="clearfix">
            <ul id="nav-actions" class="clearfix">
                <li class="other">
                    <?php echo $this->Html->link('Retour', array(
                        'controller' => 'actualites',
                        'action' => 'index',
                        'admin' => true,
                    )); ?>
                </li>
                <li>
                    <?php echo $this->Form->submit('Enregistrer'); ?>
                </li>
            </ul>
        </nav>
    </header>
    
    <div>
        <?php echo $this->Session->flash(); ?>
        <div class="row-fluid">
            <div class="span12">
                <h2>Éditer votre actualité</h2>
                <fieldset class="block">
                    <div class="row-fluid">
                        <?php
                         echo $this->Form->input('Actualite.name', array(
                            'label' => "Titre",
                            'class' => "span12"
                        ));
                        echo $this->Form->input('Actualite.content', array('label' => "Contenu", 'class' => 'redactor span12'));
                        ?>
                    </div>
                </fieldset>
            </div>
        </div>
    </div>
<?php echo $this->Form->end(); ?>

<?php echo $this->Html->scriptStart(array('inline' => false)); ?>
    $(function () {
        $('.redactor').redactor({
            minHeight: 200,
            lang: 'fr',
            buttons: ['formatting', '|', 'bold', 'italic', 'underline', 'deleted', '|', 'unorderedlist', 'orderedlist', '|', 'outdent', 'indent', 'link', 'alignment', '|', 'image', 'video'],
            imageUpload: '<?php echo Router::url(array("controller" => "images", "action" => "upload_image_redactor", "admin" => true), true); ?>',            imageUploadErrorCallback: function(json) {
                alert(json.message);
            },
            linebreaks: true,
            dragUpload: true
        });
    });
<?php echo $this->Html->scriptEnd(); ?>