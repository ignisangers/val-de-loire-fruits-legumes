<?php
// Titrage + boutons d'actions + sous-nav
echo $this->start('header'); ?>
    <h1>Gestion des actualités</h1>
    <nav class="clearfix">
        <ul id="sous-nav" class="clearfix">
            <li>
                <p>Rédigez les actualités de votre site</p>
            </li>
        </ul>
        <ul id="nav-actions" class="clearfix">
            <li><?php echo $this->Html->link('Ajouter une actualité', array('action' => 'add')); ?>
            </li>
        </ul>
    </nav>
<?php echo $this->end(); ?>

<h2 class="add">Liste des actualités</h2>

<?php echo $this->Session->flash(); ?>

<ul class="pages-sortable">
<?php foreach($actualites as $k => $actualite): ?>

    <?php echo (($k + 1) == sizeof($actualites)) ? '<li class="last">' : '<li>'; ?>
        <p>
            <span><?php echo $this->Text->truncate(strip_tags($actualite['Actualite']['content'], 50)); ?> <em>– le <?php echo date('d/m/Y à H:i', strtotime($actualite['Actualite']['created'])) ?></em></span>
        </p>
        <ul>
            <?php if(AuthComponent::user('role') == "admin"): ?>
                <li class="hide"><?php echo $this->Html->link('Modifier', array('action' => 'edit', $actualite['Actualite']['id'])); ?></li>
                <li class="hide"><?php echo $this->Html->link('Supprimer', array('action' => 'delete', $actualite['Actualite']['id']), array(), "Supprimer cette actualité ?"); ?></li>
            <?php endif; ?>
            <li class="options"><?php echo $this->Html->link('Options', "#"); ?></li>
        </ul>
    </li>
<?php endforeach; ?>
</ul>

<?php echo $this->Paginator->numbers(array('first' => 3, 'last' => 3)); ?>