<?php echo $this->Form->create('ModuleGallery', array('type' => 'file')); ?>
    <?php
    // Titrage + boutons d'actions + sous-nav
    echo '<header>';
        echo '<h1>Gestion des pages : '. $this->Html->link($page['Page']['name'], array(
            'controller' => 'pages',
            'action' => 'edit_modules',
            $page_id
        )) .'</h1>';
        echo '<nav class="clearfix">';
            echo '<ul id="sous-nav" class="clearfix">';
                echo '<li>';
                    echo $this->Html->link('Votre galerie', $this->request->here, array('class' => 'current'));
                echo '</li>';
            echo '</ul>';
            echo '<ul id="nav-actions" class="clearfix">';
                echo '<li class="other">';
                    echo $this->Html->link('Retour aux modules', array(
                        'controller' => 'pages',
                        'action' => 'edit_modules',
                        $page_id
                    ));
                echo '</li>';
            echo '</ul>';
        echo '</nav>';
    echo '</header>';
    ?>

    <?php
    echo $this->Form->input('ModuleGallery.id');
    echo '<div><div>';
    ?>
        <?php echo $this->Session->flash(); ?>
        <div class="row-fluid">
            <div class="span12">
                <h2>
                    <span>Ajoutez une photo à votre galerie ci-dessous</span>
                    <div class="btn-group" style="float: right;">
                        <label for="cropImageInput_0" class="add-module">
                            <?php
                            echo $this->Html->image("admin/add-module.png", array('alt' => 'Ajouter'));
                            echo $this->Form->input("Gallery.file", array(
                                "type" => "file",
                                "label" => false,
                                "class" => "crop_image_input",
                                "style" => "display: none;",
                                "id" => "cropImageInput_0",
                            ));
                            ?>
                        </label>
                    </div>
                </h2>
                <fieldset class="update-list">
                    <?php
                    echo $this->element("Modules/Admin/admin_galleries");
                    ?>
                </fieldset>
            </div>
        </div>
    <?php echo '</div></div>'; ?>
<?php echo $this->Form->end(); ?>

<?php
echo $this->start("modale");
    echo $this->element("Components/Admin/admin_modale_gallery");
echo $this->end();
?>

<?php
echo $this->Html->css("jquery.Jcrop.min", null, array("inline" => false));
echo $this->Html->css("crop", null, array("inline" => false));
echo $this->Html->script("jquery.Jcrop.min", array("inline" => false));
echo $this->Html->script(array("ignis.upload", "ignis.crop", "ignis.modal"), array("inline" => false));
echo $this->Html->scriptStart(array("inline" => false)); ?>
$(function () {
    var $liste = $(".update-list"),
        loader = "update-list-loader",
        uploadEvents = 0;
        
    function createLoader() {
        if ($("." + loader).length === 0) {
            $liste.prepend("<div class=\"" + loader + "\"></div>");
        }
    }
    function removeLoader() {
        $("." + loader).remove();
    }
    
    /**
     * Nouvelle image
     */
    // Upload d'une image à cropper + création modale
    $("#cropImageInput_0").change(function () {        
        Ignis.Modal.create("<?php echo Router::url(array("controller" => "galleries", "action" => "modal_add", $module_gallery_id, "admin" => true)); ?>");        
        Ignis.Upload.read(this);        
        
        // Image uploadée et modale créée : on gère le crop
        $([Ignis.Upload, Ignis.Modal]).on("upload:done modal:created", function () {
            uploadEvents++;
            if (uploadEvents === 2) { // On n'exécute la suite que si les deux événements ont eu lieu
                Ignis.Crop.read(Ignis.Upload.getUploadedFile(0));        
                $([Ignis.Upload, Ignis.Modal]).off("upload:done modal:created");
            }
        });
        // Tout est prêt : on affiche la modale
        $(Ignis.Crop).on("crop:ready", function () {
            Ignis.Modal.open();
            uploadEvents = 0;
            $(Ignis.Crop).off("crop:ready");
        });    
    });    
    // Ajout d'une image croppée dans la galerie
    $(document).on("click", ".modal-submit--add", function () {
        var saveData = $(".modal-form").serialize(),
            saveUrl = "<?php echo Router::url(array("controller" => "galleries", "action" => "add", "admin" => true)); ?>";
            
        $.ajax({
            type: "POST",
            url: saveUrl,
            data: saveData,
            beforeSend: function () {
                createLoader();
            },
            complete: function () {
                removeLoader();
                Ignis.Modal.close();
            },
            success: function (html) {
                $liste.html(html);                
            }
        });
        
        return false;
    });
    
    /**
     * Édition d'une image
     */
    // Ouverture d'une modale avec formuaire d'édition d'une image
    $(document).on("click", ".edit-item", function () {
        var url = "<?php echo Router::url(array("controller" => "galleries", "action" => "modal_edit", "admin" => true)); ?>",
            args = {id: $(this).data("id")};
        Ignis.Modal.create(url, args);
        
        // Une fois la modale créée, on l'ouvre
        $(Ignis.Modal).one("modal:created", function () {
            Ignis.Modal.open();
        });
    });
    // Changement d'image
    $(document).on("change", ".crop-change", function () {
        Ignis.Upload.read(this);        
        
        // Une fois l'upload de l'image effectué, on lance le crop dessus
        $(Ignis.Upload).one("upload:done", function () {
            Ignis.Crop.read(Ignis.Upload.getUploadedFile(0));
        });
    });
    $(document).on("click", ".modal-submit--edit", function () {
        var editData = $(".modal-form").serialize(),
            editUrl = "<?php echo Router::url(array("controller" => "galleries", "action" => "edit", "admin" => true)); ?>";
        
        $.ajax({
            type: "POST",
            url: editUrl,
            data: editData,
            beforeSend: function () {
                createLoader();
            },
            complete: function () {
                removeLoader();
                Ignis.Modal.close();
            },
            success: function (html) {
                $liste.html(html);                
            }
        });
        
        return false;
    });
    // Changer l'ordre de deux images
    $(document).on("click", ".sortable__up, .sortable__down", function () {
        var id = $(this).data("id"),
            direction = $(this).data("move"),
            module_gallery_id = $("#ModuleGalleryId").val(),
            moveUrl = "<?php echo Router::url(array("controller" => "galleries", "action" => "move", "admin" => true)); ?>";
        
        if (!$(this).hasClass("sortable__up--disabled") && !$(this).hasClass("sortable__down--disabled")) {
            $.ajax({
                type: "POST",
                url: moveUrl,
                data: {
                    id: id,
                    module_gallery_id: module_gallery_id,
                    direction: direction
                },
                beforeSend: function () {
                    createLoader();
                },
                complete: function () {
                    removeLoader();
                },
                success: function (html) {
                    $liste.html(html);
                }
            });
        }
            
        return false;
    });    
    // Suppression d'une image
    $(document).on("click", ".delete-item", function () {
        if (confirm("Supprimer cette image de la galerie ?")) {
            var id = $(this).data("id"),
                module_gallery_id = $("#ModuleGalleryId").val(),
                deleteUrl = "<?php echo Router::url(array("controller" => "galleries", "action" => "delete", "admin" => true)); ?>";

            $.ajax({
                type: "POST",
                url: deleteUrl,
                data: {
                    id: id,
                    module_gallery_id: module_gallery_id
                },
                beforeSend: function () {
                    createLoader();
                },
                complete: function () {
                    removeLoader();
                },
                success: function (html) {
                    $liste.html(html);
                }
            });
        }
        
        return false;
    });
});
<?php echo $this->Html->scriptEnd(); ?>