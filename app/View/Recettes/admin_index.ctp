<?php
// Titrage + boutons d'actions + sous-nav
echo $this->start('header'); ?>
    <h1>Gestion des Recettes</h1>
    <nav class="clearfix">
        <ul id="sous-nav" class="clearfix">
            <li>
                <p>Rédigez le contenu des recettes de votre site</p>
            </li>
        </ul>
        <ul id="nav-actions" class="clearfix">
            <li><?php echo $this->Html->link('Ajouter une recette', array('action' => 'add')); ?>
            </li>
        </ul>
    </nav>
<?php echo $this->end(); ?>

<h2 class="add">Liste des recettes</h2>

<?php echo $this->Session->flash(); ?>

<ul class="pages-sortable">
<?php foreach($recettes as $k => $recette): ?>

    <?php echo (($k + 1) == sizeof($recettes)) ? '<li class="last">' : '<li>'; ?>
        <p>
            <span><?php echo $this->Text->truncate(strip_tags($recette['Recette']['name'], 50)); ?> <?php // echo date('d/m/Y à H:i', strtotime($recette['Recette']['created'])) ?></span>
        </p>
        <ul>
            <?php if(AuthComponent::user('role') == "admin"): ?>
                <li class="hide"><?php echo $this->Html->link('Modifier', array('action' => 'edit', $recette['Recette']['id'])); ?></li>
                <li class="hide"><?php echo $this->Html->link('Supprimer', array('action' => 'delete', $recette['Recette']['id']), array(), "Supprimer cette recette ?"); ?></li>
            <?php endif; ?>
            <li class="options"><?php echo $this->Html->link('Options', "#"); ?></li>
        </ul>
    </li>
<?php endforeach; ?>
</ul>

<?php echo $this->Paginator->numbers(array('first' => 3, 'last' => 3)); ?>