<?php echo $this->Form->create('Recette', array('type' => 'file')); ?>
<!-- // Titrage + boutons d'actions + sous-nav -->
<header>
    <h1>Modifier une recette</h1>
    <nav class="clearfix">
        <ul id="nav-actions" class="clearfix">
            <li class="other">
                <?php
                echo $this->Html->link('Retour', array(
                    'controller' => 'recettes',
                    'action' => 'index',
                    'admin' => true,
                ));
                ?>
            </li>
            <li>
                <?php echo $this->Form->submit('Enregistrer'); ?>
            </li>
        </ul>
    </nav>
</header>

<div>
    <?php echo $this->Session->flash(); ?>
    <div class="row-fluid"> 
        <div class="span12">
            <h2>Type de recette</h2>
            <fieldset class="block">
                <div class="row-fluid">
                    <?php
                    echo $this->Form->input('Recette.type', array(
                        'label' => "Type de recette",
                        'options' => array('entrée' => "entrée", 'plat' => "plat", 'dessert' => "dessert"),
                        'class' => 'span2',
                        'div' => false,
                        'placeholder' => "Date de début"));
                    ?>
                </div>
            </fieldset>
        </div>
    </div>
    <div class="row-fluid"> 
        <div class="span12">
            <h2>Préparation</h2>
            <fieldset class="block">
                <div class="row-fluid">
                    <?php
                    echo $this->Form->input('Recette.personne', array('label' => "Nombre de personnes", "type" => "number", 'class' => 'span2'));
                    ?>
                    <?php
                    echo $this->Form->input('Recette.difficulte', array(
                        'label' => "Difficulté de la recette",
                        'options' => array(1 => "1", 2 => "2", 3 => "3", 4 => "4"),
                        'class' => 'span2',
                        'div' => false,
                        'placeholder' => "Date de début"));
                    ?>

                    <?php
                    echo $this->Form->input('Recette.preparation', array('label' => "Temps de préparation (minutes)", "type" => "number", 'class' => 'span2'));
                    ?>

                    <?php
                    echo $this->Form->input('Recette.cuisson', array('label' => "Temps de cuisson (minutes)", "type" => "number", 'class' => 'span2'));
                    ?>
                </div>
            </fieldset>
        </div>
    </div>
    <div class="row-fluid">            
        <div class="span12">
            <h2>Saisonnalité de la recette</h2>
            <fieldset class="block">
                <div class="row-fluid">
                    <?php
                    echo $this->Form->input('Recette.debut', array(
                        'label' => "Période de début",
                        'options' => array(1 => "Janvier", 2 => "Fevrier", 3 => "Mars", 4 => "Avril", 5 => "Mai", 6 => "Juin", 7 => "juillet", 8 => "Aout", 9 => "Septembre", 10 => "Octobre", 11 => "Novembre", 12 => "Decembre"),
                        'class' => 'span2',
                        'div' => false,
                        'placeholder' => "Date de début"));
                    ?>
                    <br>
                    <?php
                    echo $this->Form->input('Recette.fin', array(
                        'label' => "Période de fin",
                        'options' => array(1 => "Janvier", 2 => "Fevrier", 3 => "Mars", 4 => "Avril", 5 => "Mai", 6 => "Juin", 7 => "juillet", 8 => "Aout", 9 => "Septembre", 10 => "Octobre", 11 => "Novembre", 12 => "Decembre"),
                        'class' => 'span2',
                        'div' => false,
                        'placeholder' => "Date de début"));
                    ?>
                    <br>
                </div>
            </fieldset>
        </div>
    </div>
    <div class="row-fluid">            
        <div class="span12">
            <h2><span>Diaporama/image fixe</span>
                <div class="btn-group" style="float: right;">
                    <label for="cropImageInput_0" class="add-module" data-type= "gallerie">
                        <?php
                        echo $this->Html->image("admin/add-module.png", array('alt' => 'Ajouter'));
                        echo $this->Form->input("GallerieRecette.file", array(
                            "type" => "file",
                            "label" => false,
                            "class" => "crop_image_input",
                            "style" => "display: none;",
                            "id" => "cropImageInput_0",
                            "data-type" => "gallerie_recettes"
                        ));
                        ?>
                    </label>
                </div>
            </h2>
            <fieldset class="update-list-gallerie_recettes">
                <?php
                echo $this->element("Modules/Admin/admin_gallerie_recettes");
                ?>
            </fieldset>
        </div>
    </div>
    <div class="row-fluid">            
        <div class="span12">
            <h2>
                <span>Vignette de la recette</span>
                <div class="btn-group" style="float: right;">
                    <label for="cropImageInput_1" class="add-module" data-type= "vignette">
                        <?php
                        echo $this->Html->image("admin/add-module.png", array('alt' => 'Ajouter'));
                        echo $this->Form->input("VignetteRecette.file", array(
                            "type" => "file",
                            "label" => false,
                            "class" => "crop_image_input",
                            "style" => "display: none;",
                            "id" => "cropImageInput_1",
                            "data-type" => "vignette_recettes"
                        ));
                        ?>
                    </label>
                </div>
            </h2>
            <fieldset class="update-list-vignette_recettes">
                <?php
                echo $this->element("Modules/Admin/admin_vignette_recettes");
                ?>
            </fieldset>
        </div>
    </div>
    <div class="row-fluid">            
        <div class="span12">
            <h2>Éditer votre recette</h2>
            <fieldset class="block">
                <div class="row-fluid">
                    <?php
                    echo $this->Form->input('Recette.name', array(
                        'label' => "Titre",
                        'class' => "span12"
                    ));
//                    echo $this->Form->input('Recette.resume', array('label' => "resume", 'class' => 'span12'));
                    echo $this->Form->input('Recette.content', array('label' => "Contenu", 'class' => 'redactor span12'));
                    ?>
                </div>
            </fieldset>
        </div>
    </div>
    <div class="row-fluid">        
        <div class="span12">
            <h2>Liéer des produits à votre recette</h2>
            <fieldset class="block">
                <?php if (!empty($produits)): ?>
                    <?php foreach ($produits as $key => $produit): ?>
                        <?php
                        $checked = false;
                        if (in_array($produit['Produit']['id'], $recetteProduitIds)) {
                            $checked = true;
                        }
                        ?>
                        <?php
                        echo $this->Form->input('RecetteProduit.id' . $key, array(
                            'label' => $produit['Produit']['name'],
                            'value' => $produit['Produit']['id'],
                            'checked' => $checked,
                            'type' => 'checkbox'));
                        ?>
                    <?php endforeach; ?>                    
                <?php endif; ?>

            </fieldset>
        </div>
    </div>
</div>
<?php echo $this->Form->end(); ?>

<!-- Ajout modale -->
<?php
echo $this->start("modale");
echo $this->element("Components/Admin/admin_modale_gallery");
echo $this->end();
?>
<!-- Ajout du crop -->
<?php
echo $this->Html->css("jquery.Jcrop.min", null, array("inline" => false));
echo $this->Html->css("crop", null, array("inline" => false));
echo $this->Html->script("jquery.Jcrop.min", array("inline" => false));
echo $this->Html->script(array("ignis.upload", "ignis.crop", "ignis.modal"), array("inline" => false));
echo $this->Html->scriptStart(array('inline' => false));
?>

$(function () {

$('.redactor').redactor({
minHeight: 200,
lang: 'fr',
buttons: ['formatting', '|', 'bold', 'italic', 'underline', 'deleted', '|', 'unorderedlist', 'orderedlist', '|', 'outdent', 'indent', 'link', 'alignment', '|', 'image', 'video'],
imageUpload: '<?php echo Router::url(array("controller" => "images", "action" => "upload_image_redactor", "admin" => true), true); ?>', imageUploadErrorCallback: function (json) {
alert(json.message);
},
linebreaks: true,
dragUpload: true
});


var $liste = $(".update-list"),
loader = "update-list-loader",
uploadEvents = 0;
function createLoader() {
if ($("." + loader).length === 0) {
$liste.prepend("<div class=\"" + loader + "\"></div>");
}
}
function removeLoader() {
$("." + loader).remove();
}

/**
* Nouvelle image
*/

//si il y a dejà une vignette ou une gallery on empeche l'ouverture de la modale
$(".add-module").click(function () {            
vignette = $("#countVignette").val();
gallery = $("#countGallery").val(); 
var type = $(this).attr("data-type");
if((vignette > 0 && type == "vignette") || (gallery > 0 && type == "gallerie")){
confirm("vous avez déjà une image, vous ne pouvez pas en ajouter de nouvelle !");
return false;
}
});

// Upload d'une image à cropper + création modale
$(".crop_image_input").change(function () {
var controller = $(this).attr("data-type");
var id = <?php echo $recette_id ?>;
//Ignis.Modal.create("/www/val-de-loire-fruits-legumes/admin/"+controller+"/modal_add/"+id);
Ignis.Modal.create("/admin/"+controller+"/modal_add/"+id);
Ignis.Upload.read(this,controller);

// Image uploadée et modale créée : on gère le crop
$([Ignis.Upload, Ignis.Modal]).on("upload:done modal:created", function () {
uploadEvents++;
if (uploadEvents === 2) { // On n'exécute la suite que si les deux événements ont eu lieu
Ignis.Crop.read(Ignis.Upload.getUploadedFile(0),controller);
$([Ignis.Upload, Ignis.Modal]).off("upload:done modal:created");
}
});
// Tout est prêt : on affiche la modale
$(Ignis.Crop).on("crop:ready", function () {
Ignis.Modal.open();
uploadEvents = 0;
$(Ignis.Crop).off("crop:ready");
});
});
// Ajout d'une image croppée dans la galerie
$(document).on("click", ".modal-submit--add", function () {
$(this).prop( "disabled", true );
var controller = $(this).attr("data-type");
var saveData = $(".modal-form").serialize(),
//saveUrl = "/www/val-de-loire-fruits-legumes/admin/"+controller+"/add";
saveUrl = "/admin/"+controller+"/add";

$.ajax({
type: "POST",
url: saveUrl,
data: saveData,
beforeSend: function () {
createLoader();
},
complete: function () {
removeLoader();
Ignis.Modal.close();
},
success: function (html) {
$(".update-list-"+controller).html(html);
}
});

return false;
});

/**
* Édition d'une image
*/
// Ouverture d'une modale avec formulaire d'édition d'une image
$(document).on("click", ".edit-item", function () {
var controller = $(this).attr("data-type");
var id = <?php echo $recette_id ?>;
//var url = "/www/val-de-loire-fruits-legumes/admin/"+controller+"/modal_edit/"+id;
var url = "/admin/"+controller+"/modal_edit/"+id;
args = {id: $(this).data("id")};
Ignis.Modal.create(url, args);

// Une fois la modale créée, on l'ouvre
$(Ignis.Modal).one("modal:created", function () {
Ignis.Modal.open();
});
});
// Changement d'image
$(document).on("change", ".crop-change", function () {
var controller = $(this).attr("data-type");
Ignis.Upload.read(this,controller);

// Une fois l'upload de l'image effectué, on lance le crop dessus
$(Ignis.Upload).one("upload:done", function () {
Ignis.Crop.read(Ignis.Upload.getUploadedFile(0));
});
});
$(document).on("click", ".modal-submit--edit", function () {
var controller = $(this).attr("data-type");
var editData = $(".modal-form").serialize(),
//editUrl = "/www/val-de-loire-fruits-legumes/admin/"+controller+"/edit";
editUrl = "/admin/"+controller+"/edit";

$.ajax({
type: "POST",
url: editUrl,
data: editData,
beforeSend: function () {
createLoader();
},
complete: function () {
removeLoader();
Ignis.Modal.close();
},
success: function (html) {
$(".update-list-"+controller).html(html);
}
});

return false;
});
// Changer l'ordre de deux images
$(document).on("click", ".sortable__up, .sortable__down", function () {
var controller = $(this).attr("data-type");
var id = $(this).data("id"),
direction = $(this).data("move"),
module_gallery_id = $("#ModuleRealisationId").val(),
moveUrl = "<?php echo Router::url(array("controller" => "gallerie_recettes", "action" => "move", "admin" => true)); ?>";

if (!$(this).hasClass("sortable__up--disabled") && !$(this).hasClass("sortable__down--disabled")) {
$.ajax({
type: "POST",
url: moveUrl,
data: {
id: id,
module_gallery_id: module_gallery_id,
direction: direction
},
beforeSend: function () {
createLoader();
},
complete: function () {
removeLoader();
},
success: function (html) {
$(".update-list-"+controller).html(html);
}
});
}

return false;
});
// Suppression d'une image
$(document).on("click", ".delete-item", function () {
if (confirm("Supprimer cette image de la galerie ?")) {
var controller = $(this).attr("data-type");
var id = $(this).data("id"),
module_gallery_id = $("#ModuleRealisationId").val(),
//deleteUrl = "/www/val-de-loire-fruits-legumes/admin/"+controller+"/delete";
deleteUrl = "/admin/"+controller+"/delete";

$.ajax({
type: "POST",
url: deleteUrl,
data: {
id: id,
module_gallery_id: module_gallery_id
},
beforeSend: function () {
createLoader();
},
complete: function () {
removeLoader();
},
success: function (html) {
$(".update-list-"+controller).html(html);
}
});
}

return false;
});


//controle pour saisonnalité du produit
//$( "#RecetteAdminEditForm" ).submit(function(event) {
//var periode_debut = $("#RecetteDebut").val();
//var periode_fin = $("#RecetteFin").val();   
//if(Number(periode_debut) > Number(periode_fin)){
//confirm( "La période de début est supérieur a la période de fin !" );
//return false;
//}
//});

});    
<?php echo $this->Html->scriptEnd(); ?>