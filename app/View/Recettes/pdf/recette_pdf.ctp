<style>
    .page{
        /*        background: url(../webroot/img/back_pdf.jpg);
                background-size: 80px 60px;
                background-repeat: no-repeat;
                background-position: left top;*/
    }
    .l-title{
        width: 100%;
        text-align: center;
    }
    .title {
        margin-bottom: 5px;
        text-transform: uppercase;
        font-family: Oswald;
        color: #ca0324;
        font-size: 30px;
        font-weight: 400;
    }
    .l-content{
        width: 60%;
        margin-top: 60px;
        float:left;

        font-family: Oswald;
        font-weight: 300;
        text-align: justify;
        line-height: 25px;
    }
    .l-image{
        position: fixed;
        width: 30%;
        top: 175px;
        right: 0;
    }
</style>
<?php echo header("Content-Type: text/html;charset=utf-8"); ?>
<div class="page">

    <div class="l-title">
        <h1 class="title"><?php echo htmlspecialchars_decode(htmlentities($recette['Recette']['name'], ENT_QUOTES, "UTF-8")); ?></h1>
    </div>

    <div class="l-content">
        <p class="recette-description">
            <?php echo htmlspecialchars_decode(htmlentities($recette['Recette']['content'], ENT_QUOTES, "UTF-8")); ?>
        </p>
    </div>

    <div class="l-image">
        <?php
        if (!empty($recette['VignetteRecette'])) {
            echo $this->Html->image("/files/recettes/vignette/" . $recette['VignetteRecette'][0]['filename'], array(
                "alt" => "",
                "fullBase" => true,
                "width" => 200,
                "heigth" => 200
            ));
        }
        ?>
    </div>

    <div class="l-footer">

    </div>

</div>