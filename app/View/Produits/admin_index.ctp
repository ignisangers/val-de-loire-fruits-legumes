<?php
// Titrage + boutons d'actions + sous-nav
echo $this->start('header'); ?>
    <h1>Gestion des Produits</h1>
    <nav class="clearfix">
        <ul id="sous-nav" class="clearfix">
            <li>
                <p>Rédigez le contenu des produits de votre site</p>
            </li>
        </ul>
        <ul id="nav-actions" class="clearfix">
            <li><?php echo $this->Html->link('Ajouter un produit', array('action' => 'add')); ?>
            </li>
        </ul>
    </nav>
<?php echo $this->end(); ?>

<h2 class="add">Liste des produits</h2>

<?php echo $this->Session->flash(); ?>

<ul class="pages-sortable">
<?php foreach($produits as $k => $produit): ?>

    <?php echo (($k + 1) == sizeof($produits)) ? '<li class="last">' : '<li>'; ?>
        <p>
            <span><?php echo $this->Text->truncate(strip_tags($produit['Produit']['name'], 50)); ?> <!--<em>– le <?php //echo date('d/m/Y à H:i', strtotime($produit['Produit']['created'])) ?></em>--></span>
        </p>
        <ul>
            <?php if(AuthComponent::user('role') == "admin"): ?>
                <li class="hide"><?php echo $this->Html->link('Modifier', array('action' => 'edit', $produit['Produit']['id'])); ?></li>
                <li class="hide"><?php echo $this->Html->link('Supprimer', array('action' => 'delete', $produit['Produit']['id']), array(), "Supprimer ce produit ?"); ?></li>
            <?php endif; ?>
            <li class="options"><?php echo $this->Html->link('Options', "#"); ?></li>
        </ul>
    </li>
<?php endforeach; ?>
</ul>

<?php echo $this->Paginator->numbers(array('first' => 3, 'last' => 3)); ?>