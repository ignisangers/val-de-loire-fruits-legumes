<?php
// Titrage + boutons d'actions + sous-nav
echo $this->start('header');
    echo '<h1>Bonjour, '. AuthComponent::user('prenom') .'</h1>';
    echo '<h2>Bienvenue sur votre espace d’administration</h2>';
echo $this->end();
?>
<div class="row-fluid">
    <div class="span8">
        <h2>Vos outils de gestion</h2>
        <div class="block">
            <h3>Gestion des pages</h3>
            <p>Gérez l'arborescence et les contenus des pages de votre site.</p>
            <div class="clearfix">
                <?php
                echo $this->Html->link("Accéder", array(
                    "controller" => "pages",
                    "action" => "index"
                ), array('class' => 'read_more'));
                ?>
            </div>
        </div>
        <div class="block">
            <h3>Gestion des utilisateurs</h3>
            <p>Gérez la liste des utilisateurs pouvant accéder à cet espace d'administration.</p>
            <div class="clearfix">
                <?php
                echo $this->Html->link("Accéder", array(
                    "controller" => "users",
                    "action" => "index"
                ), array('class' => 'read_more'));
                ?>
            </div>
        </div>
<!--        <div class="block">
            <h3>Gestion des médias</h3>
            <p>Gérez les documents et images utilisés dans vos pages.</p>
            <div class="clearfix">
                //<?php
//                echo $this->Html->link("Accéder", array(
//                    "controller" => "media",
//                    "action" => "index"
//                ), array('class' => 'read_more'));
//                ?>
            </div>
        </div>-->
    </div>
    <div class="span4">
        <h2>Vos autres espaces</h2>
        <div class="block">
            <h4>Espace pro Ignis</h4>
            <p>Factures, fichiers sources - <?php echo $this->Html->link("cliquez ici", "http://www.ignis.fr/espacepro/", array('target' => '_blank')); ?></p>
            <ul>
                <li style="line-height: 1.6em;"><strong>Login :</strong> login</li>
                <li style="line-height: 1.6em;"><strong>Mot de passe :</strong> password</li>
            </ul>
        </div>
        <div class="block">
            <h4>Google Analytics</h4>
            <p>Statistiques de visite de votre site - <?php echo $this->Html->link("cliquez ici", "http://www.google.fr/intl/fr/analytics/", array('target' => '_blank')); ?></p>
            <ul>
                <li style="line-height: 1.6em;"><strong>Login :</strong> login</li>
                <li style="line-height: 1.6em;"><strong>Mot de passe :</strong> password</li>
            </ul>
        </div>
    </div>
</div>