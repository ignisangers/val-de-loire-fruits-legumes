<?php echo $this->Form->create('File', array('type' => 'file')); ?>
    <!-- // Titrage + boutons d'actions + sous-nav -->
    <header>
        <h1>Modifier un fichier</h1>
        <nav class="clearfix">
            <ul id="nav-actions" class="clearfix">
                <li class="other">
                    <?php echo $this->Html->link('Retour', array(
                        'controller' => 'files',
                        'action' => 'index',
                        'admin' => true,
                    )); ?>
                </li>
                <li>
                    <?php echo $this->Form->submit('Enregistrer'); ?>
                </li>
            </ul>
        </nav>
    </header>
    
    <div>
        <?php echo $this->Session->flash(); ?>
        <div class="row-fluid">
            <div class="span12">
                <h2>Remplissez les informations du fichier à modifier</h2>
                <fieldset class="block">
                    <?php echo $this->Form->input('File.nom', array('label' => 'Nom', 'class' => 'span4', 'type' => 'text'));
                    echo $this->Form->input('File.fichier', array('label' => 'Fichier', 'class' => 'span4', 'type' => 'file'));
//                    echo $this->Form->input('File.description', array('label' => 'Description', 'class' => 'span4', 'placeholder' => "Entrez ici la description de votre fichier.<br>Celle-ci sera automatiquement terminée par un lien vers le fichier."));
                    
                    if (!empty($this->request->data['File']['fichier'])): ?>
                        <h3>Fichier actuel : </h3>
                        <em><?php echo $this->request->data['File']['fichier']; ?></em>
                    <?php endif; ?>
                    <?php echo $this->Form->input('File.module_file_id', array('label' => 'Module', 'class' => 'span4', 'type' => 'select', 'options' => $liste_modules)); ?>
                </fieldset>
            </div>
        </div>
    </div>
<?php echo $this->Form->end();