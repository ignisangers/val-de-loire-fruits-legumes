<?php foreach($files as $k => $file): ?>
    <?php echo (($k + 1) == sizeof($files)) ? '<li class="last">' : '<li>'; ?>
        <p>
            <span><?php echo $file['File']['nom']; ?></span>
        </p>
        <ul>
            <li class="hide">
                <?php echo $this->Html->link('Haut', array('action' => 'move', $file['File']['id'], 'up'), array('class' => 'up half', 'title' => "Monter le fichier d'un cran")); ?>
                <?php echo $this->Html->link('Bas', array('action' => 'move', $file['File']['id'], 'down'), array('class' => 'down half', 'title' => "Descendre le fichier d'un cran")); ?>
            </li>
            <?php if(AuthComponent::user('role') == "admin"): ?>
                <li class="hide"><?php echo $this->Html->link('Modifier', array('action' => 'edit', $file['File']['id'])); ?></li>
                <li class="hide"><?php echo $this->Html->link('Supprimer', array('action' => 'delete', $file['File']['id']), array(), "Supprimer cette personne ?"); ?></li>
            <?php endif; ?>
            <li class="options"><?php echo $this->Html->link('Options', "#"); ?></li>
        </ul>
    </li>
<?php endforeach; ?>