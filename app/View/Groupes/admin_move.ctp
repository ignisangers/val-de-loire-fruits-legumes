<?php foreach($groupes as $k => $groupe): ?>
    <?php echo (($k + 1) == sizeof($groupes)) ? '<li class="last">' : '<li>'; ?>
        <p>
            <span><?php echo $groupe['Groupe']['nom']; ?></span>
        </p>
        <ul>
            <li class="hide">
                <?php echo $this->Html->link('Haut', array('action' => 'move', $groupe['Groupe']['id'], 'up'), array('class' => 'up half', 'title' => "Monter le groupe d'un cran")); ?>
                <?php echo $this->Html->link('Bas', array('action' => 'move', $groupe['Groupe']['id'], 'down'), array('class' => 'down half', 'title' => "Descendre le groupe d'un cran")); ?>
            </li>
            <?php if(AuthComponent::user('role') == "admin"): ?>
                <li class="hide"><?php echo $this->Html->link('Modifier', array('action' => 'edit', $groupe['Groupe']['id'])); ?></li>
                <li class="hide"><?php echo $this->Html->link('Supprimer', array('action' => 'delete', $groupe['Groupe']['id']), array(), "Supprimer ce groupe ?"); ?></li>
            <?php endif; ?>
            <li class="options"><?php echo $this->Html->link('Options', "#"); ?></li>
        </ul>
    </li>
<?php endforeach; ?>