<?php echo $this->Form->create('File', array('type' => 'file')); ?>
    <!-- // Titrage + boutons d'actions + sous-nav -->
    <header>
        <h1>Ajouter un groupe</h1>
        <nav class="clearfix">
            <ul id="nav-actions" class="clearfix">
                <li class="other">
                    <?php echo $this->Html->link('Retour', array(
                        'controller' => 'groupes',
                        'action' => 'index',
                        'admin' => true,
                    )); ?>
                </li>
                <li>
                    <?php echo $this->Form->submit('Enregistrer'); ?>
                </li>
            </ul>
        </nav>
    </header>
    
    <div>
        <?php echo $this->Session->flash(); ?>
        <div class="row-fluid">
            <div class="span12">
                <h2>Remplissez les informations du fichier à ajouter</h2>
                <fieldset class="block">
                    <?php echo $this->Form->input('Groupe.nom', array('label' => 'Nom', 'class' => 'span4', 'type' => 'text')); ?>
                    <?php echo $this->Form->input('Groupe.photo', array('label' => 'Photo/Logo', 'class' => 'span4', 'type' => 'file')); ?>
                    <?php echo $this->Form->input('Groupe.adresse', array('label' => 'Adresse', 'class' => 'span6', 'type' => 'text')); ?>
                    <?php echo $this->Form->input('Groupe.code_postal', array('label' => 'Code postal', 'class' => 'span4', 'type' => 'text')); ?>
                    <?php echo $this->Form->input('Groupe.ville', array('label' => 'Ville', 'class' => 'span4', 'type' => 'text')); ?>
                    <?php echo $this->Form->input('Groupe.telephone', array('label' => 'Téléphone', 'class' => 'span4', 'type' => 'text')); ?>
                    <?php echo $this->Form->input('Groupe.autres', array('label' => 'Détails supplémentaires', 'class' => 'span12', 'type' => 'text')); ?>
                    <?php echo $this->Form->input('Groupe.module_groupe_id', array('label' => 'Catégorie du groupe', 'class' => 'span4', 'type' => 'select', 'options' => $liste_modules)); ?>
                </fieldset>
            </div>
        </div>
    </div>
<?php echo $this->Form->end(); ?>