<?php
// Titrage + boutons d'actions + sous-nav
echo $this->start('header'); ?>
    <h1>Gestion des groupes</h1>
    <nav class="clearfix">
        <ul id="sous-nav" class="clearfix">
            <li>
                <p>Gérez vos groupes</p>
            </li>
        </ul>
        <ul id="nav-actions" class="clearfix">
            <li><?php echo $this->Html->link('Ajouter un groupe', array('action' => 'add')); ?></li>
        </ul>
    </nav>
<?php echo $this->end(); ?>

<h2 class="add">Liste des groupes</h2>

<?php echo $this->Session->flash(); ?>

<div id='ajax-load'>
<?php foreach($module_groupes as $module_groupe): ?>
    <h3>Module groupe : <?php echo $module_groupe['ModuleGroupe']['title'] ?></h3>
    <ul class="pages-sortable">
    <?php foreach($module_groupe['Groupe'] as $k => $groupe): ?>
        <?php echo (($k + 1) == sizeof($module_groupe['Groupe'])) ? '<li class="last">' : '<li>'; ?>
            <p>
                <span><?php echo $groupe['nom']; ?></span>
            </p>
            <ul>
                <li class="hide">
                    <?php echo $this->Html->link('Haut', array('action' => 'move', $groupe['id'], 'up'), array('class' => 'up half', 'title' => "Monter le groupe d'un cran")); ?>
                    <?php echo $this->Html->link('Bas', array('action' => 'move', $groupe['id'], 'down'), array('class' => 'down half', 'title' => "Descendre le groupe d'un cran")); ?>
                </li>
                <?php if(AuthComponent::user('role') == "admin"): ?>
                    <li class="hide"><?php echo $this->Html->link('Modifier', array('action' => 'edit', $groupe['id'])); ?></li>
                    <li class="hide"><?php echo $this->Html->link('Supprimer', array('action' => 'delete', $groupe['id']), array(), "Supprimer ce groupe ?"); ?></li>
                <?php endif; ?>
                <li class="options"><?php echo $this->Html->link('Options', "#"); ?></li>
            </ul>
        </li>
    <?php endforeach; ?>
    </ul>
<?php endforeach; ?>
</div>
<?php echo $this->Html->scriptStart(array('inline' => false)); ?>
    $(document).ready(function() {
        $(document).on("click", ".up, .down", function() {
            var url = $(this).attr('href');
            var listeparent = $(this).closest('.pages-sortable');
            $.ajax({
                type: "GET",
                url: url,
                beforeSend: function() {
                    listeparent.css({ opacity: .5 });
                },
                success: function(html) {
                    listeparent.html(html).css({ opacity: 1 });
                }
            });
            return false;
        });
    });
<?php echo $this->Html->scriptEnd(); ?>