<div class="modal-group" style="display: none;">
    <div class="modal-backdrop"></div>
    <div class="modal">
        <?php echo $this->fetch('content'); ?>
    </div>
</div>