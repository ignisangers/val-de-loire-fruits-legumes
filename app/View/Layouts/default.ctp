<!DOCTYPE html>
<html lang="<?php echo substr(Configure::read('Config.language'), 0, 2) ?>">
    <head>
        <?php echo $this->Html->charset(); ?>
        <title><?php echo $metas['title']; ?></title>
        <?php
        // Metas
        echo $this->Html->meta('icon');
        echo $this->Html->meta('description', $metas['description']);
        echo $this->Html->meta(array('name' => 'viewport', 'content' => 'initial-scale=1.0'));
        // CSS
        echo $this->Html->css('http://fonts.googleapis.com/css?family=Oswald:300,400,400italic,600,700,200,900');
        echo $this->Html->css('structure');
        echo $this->Html->css('bootstrap');
        echo $this->Html->css('frontoffice');
        echo $this->fetch('css');
        //JS
        echo $this->Html->script('jquery-1.9.1.min');
        echo $this->Html->script('bootstrap.min');
        ?>
    </head>
    <body>
        <!-- HEADER -->
        <header role="banner" class="">
            <!-- MENU PRINCIPAL -->
            <nav role="navigation" class="clearfix">
                <?php
                // Affichage récursive du menu principal
                echo $this->element("Components/Site/site_menu", array(
                    "pages" => $menus["pages"],
                    "pageActive" => $menus["pageActive"],
                    "main" => true
                ));
                ?>
            </nav>
        </nav>
    </header>
    <!-- SITE -->

    <div class="container">
        <?php echo $this->fetch('content'); ?>
    </div>

    <div class="accueil-partenaires">
        <ul>
            <li>
                <a href="http://www.idfel.fr/"><?php echo $this->html->image('IDfel.png', array('alt' => 'IDfel')) ?></a>
            </li>
            <li>
                <a href="http://www.regioncentre-valdeloire.fr/"><?php echo $this->html->image('region-centre.jpg', array('alt' => 'IDfel')) ?></a>
            </li>
            <li>
                <a href="http://www.paysdelaloire.fr/"><?php echo $this->html->image('pays-loire.png', array('alt' => 'IDfel')) ?></a>
            </li>
        </ul>
    </div>
    <!-- FOOTER -->
    <!--<footer id="footer" class="container">-->
    <!--                        <nav>
                                <ul class="clearfix">
                                    <li>© IGNIS Communication</li>-->
    <?php
//                    if(!empty($annexes)) {
//                        // Menu annexe
//                        echo $this->element("Components/Site/site_menu_annexe", array("annexes" => $annexes));
//                    }
    ?>
    <!--                            </ul>
                            </nav>-->
    <!--</footer>-->
    <?php
    // JS
    echo $this->Html->script('jquery-1.9.1.min.js');
    echo $this->fetch('script');
    ?>
</body>
</html>
