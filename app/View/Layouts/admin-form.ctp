<!DOCTYPE html>
<html>
    <head>
        <?php echo $this->Html->charset(); ?>
        <title><?php echo "Administration : " . $title_for_layout; ?></title>
        <?php
        echo $this->Html->meta('icon');
        // CSS
        echo $this->Html->css('http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400italic,600,700,200,900');
        echo $this->Html->css('bootstrap');
        echo $this->Html->css('admin');
        echo $this->Html->css('jquery-ui-1.10.2.custom');
        echo $this->Html->css('redactor');
        echo $this->fetch('css');
        // JS
        echo $this->Html->script('modernizr.custom.10261.js');
        echo $this->Html->script('jquery-1.9.1.min.js');
        echo $this->Html->script('bootstrap.min.js');
        echo $this->Html->script('jquery-ui-1.10.2.custom.min');
        echo $this->Html->script('redactor/redactor.min');
        echo $this->Html->script('redactor/fr');
        // Metas
        echo '<meta name="viewport" content="initial-scale=1.0">';
        echo $this->fetch('meta');
        // Variables JS issues du Controller
        if (isset($jsVars)) {
            echo $this->Html->scriptBlock('var jsVars = ' . json_encode($jsVars) . ';');
        } else {
            echo $this->Html->scriptBlock('var jsVars = ' . json_encode(array()) . ';');
        }
        ?>
    </head>
    <body>
        <header role="banner" class="clearfix">
            <nav role="navigation" class="clearfix">
                <?php echo $this->element('Components/Admin/admin_menu'); ?>
            </nav>
        </header>
        <section id="content"<?php echo ($this->params['controller'] == "dashboards" && $this->params['action'] == "admin_index") ? ' class="accueil"' : ''; ?>>
            <?php echo $this->fetch('content'); ?>
        </section>
        <?php echo $this->fetch('modale'); ?>
        <?php echo $this->fetch('script'); ?>
        <script>
            $(function() {
                $(document).delegate("#actions a.disabled, .module a.disabled", "click", function() {
                    return false;
                });
                $("#languages").click(function() {
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo Router::url("/admin/languages/list/"); ?>',
                        success: function(html) {
                            $("body").append(html).find("#languages-list").slideDown("fast");
                        }
                    });
                    return false;
                });
                $(document).delegate("#languages-list a.fermer", "click", function() {
                    $("#languages-list").slideUp("fast", function() {
                        $(this).remove();
                    });
                    return false;
                });
            });
        </script>
    </body>
</html>
