<!DOCTYPE html>
<html>
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>Bienvenue sur votre espace d'administration</title>
        <?php
        echo $this->Html->meta('icon');
        // CSS
        echo $this->Html->css('http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400italic,600,700,200');
        echo $this->Html->css('bootstrap');
        echo $this->Html->css('admin');
        echo $this->fetch('css');
        // JS
        echo $this->Html->script('jquery-1.9.1.min.js');
        // Metas
        echo '<meta name="viewport" content="initial-scale=1.0">';
        echo $this->fetch('meta');
        ?>
    </head>
    <body>
        <div id="login">
            <?php //echo $this->Session->flash('auth'); ?>
            <?php echo $this->Form->create('User'); ?>
            <div style="text-align: center">
                <?php
                echo '<div class="input-prepend">';
                    echo '<span class="add-on"><i class="icon-user icon-white"></i></span>';
                    echo $this->Form->input('User.username', array('label' => false, 'div' => false, 'placeholder' => 'Identifiant'));
                echo '</div>';
                echo '<div class="input-prepend">';
                    echo '<span class="add-on"><i class="icon-lock icon-white"></i></span>';
                    echo $this->Form->input('User.password', array('label' => false, 'div' => false, 'placeholder' => 'Mot de passe'));
                echo '</div>';
                //echo $this->Form->input('Page.cookie', array('type' => 'checkbox', 'label' => 'Rester connecté(e) ?'));
                echo $this->Form->button('Connexion', array('type' => 'submit', 'class' => 'btn btn-info btn-large', 'style' => 'text-align: center;'))
                ?>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
        <p id="login-copyright">© Administration par <?php echo $this->Html->link('Ignis Communication', 'http://www.ignis.fr', array('target' => '_blank')); ?></p>
    </body>
</html>