<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo $title_for_layout; ?></title>
    </head>

    <body style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0; background-color: #F4FACB; font-family: 'Century Gothic', Arial, Helvetica, sans-serif;">
        <table cellpadding="0" cellspacing="0" border="0" width="100%" bgcolor="#F4FACB">
            <tr>
                <td style="padding: 25px 0;">
                    <table width="600" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" style="border-top: 2px solid #8EB252;">
                        <tr>
                            <td colspan="4" align="left" style="padding: 25px 0 35px;">
                                <p style="text-align: center; color: #888; font-size: 28px">Val de Loire Fruits et Légumes</p>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan="2" align="center" bgcolor="#8EB252" style="padding: 5px;">
                                <p style="font-size: 12px; text-transform: uppercase; color: #ffffff; margin: 0;">MESSAGE DU SITE Val de Loire Fruits et Légumes</p>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="4" height="25">&nbsp;</td>
                        </tr>
                        <?php echo $content_for_layout; ?>
                        <tr>
                            <td colspan="4" height="15">&nbsp;</td>
                        </tr>
                        <tr bgcolor="#F4FACB">
                            <td colspan="4" style="padding-top: 10px; font-size: 9px; line-height: 13px; font-family: Verdana, Geneva, sans-serif; text-align: center; color: #8EB252;">
                                Alerte e-mail automatique du site <a href="http://valdeloirefruitsetlegumes.fr">valdeloirefruitsetlegumes.fr</a>, le <?php echo date('d/m/Y à H\hi', time()); ?>.
                            </td>
                        </tr>
                    </table>
                    <p></p>
                </td>
            </tr>
        </table>
    </body>
</html>