<?php
if(empty($modules)) {
    echo '<div class="module">';
        echo '<p style="margin: 0; padding: 12px;">Aucun module trouvé.</p>';
    echo '</div>';
} else {
    foreach($modules as $key => $module):
        $model = Inflector::tableize($module['Content']['model']);
        echo '<div class="module '. $model .' clearfix">';
            // Liens d'actions
            echo '<aside>';
                echo '<ul>';
                    echo '<li class="edit">';
                        echo $this->Html->link("Modifier", array(
                            'controller' => $model,
                            'action' => 'edit',
                            $module[$module['Content']['model']][0]['id'],
                            $module['Page']['id']
                        ), array('title' => 'Modifier'));
                    echo '</li>';
                    echo '<li class="remove">';
                        echo $this->Html->link("Supprimer", array(
                            'controller' => 'contents',
                            'action' => 'delete',
                            $module['Page']['id'],
                            $module['Content']['zone'],
                            $module['Content']['rang'],
                            $module['Content']['id']
                        ), array('title' => 'Supprimer'), "Supprimer ce module ?");
                    echo '</li>';
                    echo '<li class="up">';
                        if($key == 0) {
                            echo $this->Html->link("Haut", '#', array('title' => 'Haut', 'class' => 'disabled'));
                        } else {
                            echo $this->Html->link("Haut", array(
                                'controller' => 'contents',
                                'action' => 'move',
                                $module['Content']['id'],
                                $module['Page']['id'],
                                $module['Content']['zone'],
                                $module['Content']['rang'],
                                ($order == "DESC") ? 'down': 'up'
                            ), array('title' => 'Haut'));
                        }
                    echo '</li>';
                    echo '<li class="down">';
                        if($key == (count($modules)-1)) {
                            echo $this->Html->link("Bas", '#', array('title' => 'Bas', 'class' => 'disabled'));
                        } else {
                            echo $this->Html->link("Bas", array(
                                'controller' => 'contents',
                                'action' => 'move',
                                $module['Content']['id'],
                                $module['Page']['id'],
                                $module['Content']['zone'],
                                $module['Content']['rang'],
                                ($order == "DESC") ? 'up': 'down'
                            ), array('title' => 'Bas'));
                        }
                    echo '</li>';
                echo '</ul>';
            echo '</aside>';
            // Affichage spécifique du module
            echo '<section>' . $this->element("Modules/Admin/admin_".$model, array('module' => $module)) . '</section>';
        echo '</div>';
    endforeach;
}
?>