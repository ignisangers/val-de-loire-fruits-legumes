<div class="container">
    <!-- FIL D'ARIANE -->
    <?php echo $this->element('breadcrumb', array('breadcrumb' => $breadcrumb)) ?>
    
    <!-- FORMULAIRE -->
    <?php echo $this->Session->flash(); ?>
    <h1><?php echo __("Contactez-nous"); ?></h1>
    <?php
    echo $this->Form->create("Contact");
        echo '<div class="row-fluid" style="">';
            echo '<div class="span4">';
                echo $this->Form->input("Contact.email", array(
                    'type' => 'text',
                    'label' => __('Votre adresse e-mail *'),
                    'class' => 'span12'
                ));
            echo '</div>';
        echo '</div>';
        echo '<div class="row-fluid">';
            echo '<div class="span4">';
                echo $this->Form->input("Contact.nom", array(
                    'type' => 'text',
                    'label' => __('Votre nom *'),
                    'class' => 'span12'
                ));
            echo '</div>';
            echo '<div class="span4">';
                echo $this->Form->input("Contact.prenom", array(
                    'type' => 'text',
                    'label' => __('Votre prénom *'),
                    'class' => 'span12'
                ));
            echo '</div>';
        echo '</div>';
        echo '<div class="row-fluid">';
            echo '<div class="span4">';
                echo $this->Form->input("Contact.telephone", array(
                    'type' => 'text',
                    'label' => __('Votre numéro de téléphone'),
                    'class' => 'span12'
                ));
            echo '</div>';
            echo '<div class="span4">';
                echo $this->Form->input("Contact.societe", array(
                    'type' => 'text',
                    'label' => __('Votre société'),
                    'class' => 'span12'
                ));
            echo '</div>';
        echo '</div>';
        echo '<div class="row-fluid">';
            echo '<div class="span8">';
                echo $this->Form->input("Contact.comment", array(
                    'type' => 'textarea',
                    'label' => __('Votre message *'),
                    'class' => 'span12',
                    'rows' => 10
                ));
            echo '</div>';
        echo '</div>';
        echo '<p style="margin-top: 0;"><small>'. __("* Les champs avec astérisque sont obligatoires") .'</small></p>';
        echo $this->Form->hidden("Contact.captcha");
        echo $this->Form->submit(__('Envoyer'));
    echo $this->Form->end();
    ?>
</div>