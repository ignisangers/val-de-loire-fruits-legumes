<?php
// Titrage + boutons d'actions + sous-nav
echo $this->start('header');
    echo '<h1>Gestion des pages</h1>';
    echo '<nav class="clearfix">';
        echo '<ul id="sous-nav" class="clearfix">';
            echo '<li>';
                echo '<p>Rédigez les contenus de votre site</p>';
            echo '</li>';
        echo '</ul>';
        echo '<ul id="nav-actions" class="clearfix">';
            echo '<li>';
//                echo $this->Html->link('Ajouter une page', array(
//                    'action' => 'add'
//                ));
            echo '</li>';
        echo '</ul>';
    echo '</nav>';
echo $this->end();
?>

<h2 class="add">Arborescence du site</h2>
<?php
echo $this->Session->flash();
echo '<ul class="pages-sortable">';
    echo $this->element("/Components/Admin/admin_pages_tree", array("pages" => $pages));
echo '</ul>';
?>
<?php echo $this->Html->scriptStart(array('inline' => false)); ?>
    $(document).ready(function() {
        $(document).on("click", ".up, .down", function() {
            var url = $(this).attr('href');
            $.ajax({
                type: "GET",
                url: url,
                beforeSend: function() {
                    $(".pages-sortable").css({ opacity: .5 });
                },
                success: function(html) {
                    $(".pages-sortable").html(html).css({ opacity: 1 });
                }
            });
            return false;
        });
    });
<?php echo $this->Html->scriptEnd(); ?>