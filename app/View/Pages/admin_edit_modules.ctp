<?php
// Titrage + boutons d'actions + sous-nav
echo $this->start('header');
    echo '<h1>Gestion des pages : '. $this->Html->link($page['Page']['name'], array(
        'controller' => 'pages',
        'action' => 'edit_modules',
        $page['Page']['id']
    )) .'</h1>';
    echo '<nav class="clearfix">';
        echo '<ul id="sous-nav" class="clearfix">';
            echo '<li>';
                echo $this->Html->link('Contenu de la page', array(
                    'action' => 'edit_modules',
                    $page['Page']['id']
                ), array('class' => 'current'));
            echo '</li>';
            echo '<li>';
                echo $this->Html->link('Paramètres', array(
                    'action' => 'edit',
                    $page['Page']['id']
                ));
            echo '</li>';
        echo '</ul>';
        echo '<ul id="nav-actions" class="clearfix">';
            echo '<li class="other">';
                echo $this->Html->link('Retour', array(
                    'action' => 'index'
                ));
            echo '</li>';
            echo '<li>';
                if($page['Page']['home'] == 1) { // Accueil
                    $href = array(
                        "controller" => "pages",
                        "action" => "index",
                        "admin" => false
                    );
                } else if($page['PageType']['slug'] !== "modules") { // Page spéficique
                    $href = array(
                        "controller" => $page['PageType']['controller'],
                        "action" => $page['PageType']['action'],
                        "admin" => false
                    );
                } else {
                    $href = array(
                        "controller" => "pages",
                        "action" => "display",
                        $page['Page']['alias'],
                        "admin" => false
                    );
                }
                echo $this->Html->link('Visualiser la page', $href, array('target' => '_blank'));
            echo '</li>';
        echo '</ul>';
    echo '</nav>';
echo $this->end();

echo $this->Session->flash();
echo $this->Form->create('Page');
    if ($page['PageType']['slug'] === "modules") { // Type de contenu "modules" : on affiche le template souhaité avec la gestion des modules
        echo $this->element('Templates/Admin/admin-' . $templateName, array("page_id" => $page['Page']['id']));
    } else { // Type de contenu autre que "modules" : on précise ce qu'est cette page et qu'elle n'est pas administrable
        echo $this->element('Templates/Admin/admin-specific-content', array("pageType" => $page['PageType']));
    }
echo $this->Form->end();