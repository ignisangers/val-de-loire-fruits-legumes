<?php
$i=1;
foreach($pages as $page):
    // On va gérer le retrait négatif en fonction de la profondeur de la page dans l'arborescence
    if(empty($page['Page']['parent_id'])) {
        $parents = array();
    } else if($page['Page']['parent_id'] != end($parents)) {
        if(!in_array($page['Page']['parent_id'], $parents)) {
            $parents[] = $page['Page']['parent_id'];
        } else {
            $new_parents = array();
            foreach($parents as $parent):
                $new_parents[] = $parent;
                if($parent == $page['Page']['parent_id']) {
                    break;
                }
            endforeach;
            $parents = $new_parents;
        }
    }

    echo (count($pages) == $i) ? '<li class="last">' : '<li>';
        echo (count($parents) > 0) ? '<p style="margin-left: '. (count($parents) * 2) .'%;" class="child">' : '<p>';
            if(count($parents) > 0) {
                for($j=0; $j<count($parents); $j++):
                    echo '--';
                endfor;
                echo ' ';
            }
            echo ($page['Page']['active'] == 0) ? '<span class="disabled">' : '<span>';
                echo $page['Page']['name'];
            echo '</span>';
        echo '</p>';
        echo '<ul>';
            if($page['Page']['active'] == 0) {
                $class = 'offline';
                $message = 'Remettre en ligne la page : "'. $page['Page']['name'] .'" ?';
                $active = 1;
            } else {
                $class = 'online';
                $message = 'Mettre hors-ligne la page : "'. $page['Page']['name'] .'" ?';
                $active = 0;
            }
            echo '<li class="hide">'. $this->Html->link('En ligne/hors-ligne', array('action' => 'active', $page['Page']['id'], $active), array('class' => $class), $message) .'</li>';
            echo '<li class="hide">';
                echo $this->Html->link('Haut', array('action' => 'move', $page['Page']['id'], 'up'), array('class' => 'up half')/*, "Monter la page d'un cran ?"*/);
                echo $this->Html->link('Bas', array('action' => 'move', $page['Page']['id'], 'down'), array('class' => 'down half')/*, "Descendre la page d'un cran ?"*/);
            echo '</li>';
            if($page['Page']['specific'] == 0 || AuthComponent::user('role') == "admin") {
                echo '<li class="hide">'. $this->Html->link('Modifier', array('action' => 'edit_modules', $page['Page']['id'])) .'</li>';
                echo '<li class="hide">'. $this->Html->link('Dupliquer', array('action' => 'duplicate', $page['Page']['id']), array(), "Dupliquer la page : \"". $page['Page']['name'] ."\" ?") .'</li>';
                echo '<li class="hide">'. $this->Html->link('Supprimer', array('action' => 'delete', $page['Page']['id']), array(), "Supprimer la page : \"". $page['Page']['name'] ."\" et tous ses contenus ?") .'</li>';
            }
            echo '<li class="options">'. $this->Html->link('Options', "#") .'</li>';
        echo '</ul>';
    echo '</li>';
    $i++;
endforeach;
?>