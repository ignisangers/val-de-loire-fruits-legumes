<?php echo $this->Form->create('Page'); ?>
    <?php
    // Titrage + boutons d'actions + sous-nav
    echo '<header>';
        echo '<h1>Gestion des pages : '. $this->Html->link($this->request->data['Page']['name'], array('action' => 'edit_modules', $this->request->data['Page']['id'])) .'</h1>';
        echo '<nav class="clearfix">';
            echo '<ul id="sous-nav" class="clearfix">';
                echo '<li>';
                    echo $this->Html->link('Contenu de la page', array(
                        'action' => 'edit_modules',
                        $this->request->data['Page']['id']
                    ));
                echo '</li>';
                echo '<li>';
                    echo $this->Html->link('Paramètres', array(
                        'action' => 'edit',
                        $this->request->data['Page']['id']
                    ), array('class' => 'current'));
                echo '</li>';
            echo '</ul>';
            echo '<ul id="nav-actions" class="clearfix">';
                echo '<li class="other">';
                    echo $this->Html->link('Retour', array(
                        'controller' => 'pages',
                        'action' => 'edit_modules',
                        $this->request->data['Page']['id']
                    ));
                echo '</li>';
                echo '<li>';
                    echo $this->Form->submit('Enregistrer');
                echo '</li>';
            echo '</ul>';
        echo '</nav>';
    echo '</header>';
    ?>

    <?php
    echo $this->Form->input('Page.id');
    echo '<div><div>';
    ?>
        <?php echo $this->Session->flash(); ?>
        <div class="row-fluid">
            <div class="span6">
                <h2>Informations générales</h2>
                <fieldset class="block" style="height: 280px;">
                    <?php
                    echo $this->Form->input('Page.name', array('label' => 'Nom de votre page', 'placeholder' => 'Le nom qui apparaîtra sur le site', 'class' => 'span12'));
                    echo $this->Form->input('Page.parent_id', array('empty' => 'Aucune', 'label' => 'Page parente', 'type' => 'select', 'options' => $listePages));
                    echo '<div style="margin-top: 20px;">';
                    echo $this->Form->input('Page.menu', array('label' => 'Présent dans le menu'));
                    echo $this->Form->input('Page.active', array('label' => 'En ligne/hors ligne'));
                    echo $this->Form->input('Page.home', array('label' => 'Est-ce la page d\'accueil ?'));
                    echo '</div>';
                    ?>
                </fieldset>
            </div>
            <div class="span6">
                <h2>Référencement</h2>
                <fieldset class="block" style="height: 280px;">
                    <?php
                    echo $this->Form->input('Page.alias', array('label' => 'URL', 'placeholder' => 'L\'URL de votre page', 'class' => 'span12'));
                    echo $this->Form->input('Page.meta_title', array('label' => 'Balise title', 'placeholder' => 'Le nom qui apparaîtra sur les moteurs de recherche', 'class' => 'span12'));
                    echo $this->Form->input('Page.meta_description', array('label' => 'Balise description', 'placeholder' => 'La description qui apparaîtra sur les moteurs de recherche', 'type' => 'textarea', 'class' => 'span12', 'rows' => 4));
                    ?>
                </fieldset>
            </div>
        </div>
        <?php if(AuthComponent::user("role") == "admin") { ?>
            <div class="row-fluid">
                <div class="span12">
                    <h2>Type de contenu</h2>
                    <fieldset class="block">
                        <?php
                        echo $this->Form->input('Page.page_type_id', array(
                            'type' => 'select',
                            'options' => $listePageTypes['list'],
                            'label' => "Déterminez le type de contenu de la page",
                            'data-type-module' => $listePageTypes['typeModule']
                        ));
                        ?>
                        <p><small><strong>Remarque :</strong> le type de contenu par défaut est la "gestion de modules", qui donne accès aux zones de modules définies par le template ci-dessous. <br>Un autre type de contenu pourra remplacer ce système en cas de contenu non administrable géré par une action spécifique (comme la page "actualités" qui liste toutes les actualités avec pagination, ne nécessitant aucune administration particulière). <br>Par défaut, un utilisateur n'aura accès qu'à la gestion de modules.</small></p>
                    </fieldset>
                </div>
            </div>
        <?php } ?>
        
            <div class="row-fluid" id="modules_templates"<?php if ($this->request->data['Page']['page_type_id'] !== $listePageTypes['typeModule']) { echo ' style="display: none;"'; } ?>>
                <div class="span12">
                    <h2>Modèle de mise en page</h2>
                    <fieldset class="block">
                        <p>Chaque modèle met à votre disposition plusieurs zones numérotées, dans lesquelles vous pourrez ajouter des modules de contenu par la suite.</p>
                        <?php
                        foreach($listeTemplates as $id => $name):
                            $listeTemplates[$id] = $this->Html->image("admin/templates/".$name.".jpg", array('alt' => $name));
                        endforeach;
                        echo $this->Form->input('Page.template_id', array('type' => 'radio', 'options' => $listeTemplates, 'before' => '<div class="choose_layout">', 'separator' => '</div><div class="choose_layout">', 'after' => '</div>', 'legend' => false, 'div' => false));
                        ?>
                    </fieldset>
                </div>
            </div>
    <?php echo '</div></div>'; ?>
<?php echo $this->Form->end(); ?>
<?php echo $this->Html->script('ckeditor/ckeditor.js', array('inline' => false)); ?>
<?php echo $this->Html->scriptStart(array('inline' => false)); ?>
    $(function() {
        $("#PageName").bind("keyup change", function() {
            var texte = $(this).val();
            $.ajax({
                type: 'GET',
                url: '<?php echo Router::url(array(
                    "controller" => "pages",
                    "action" => "ajax_alias",
                    "language" => Configure::read('Config.language'),
                    "admin" => true
                )); ?>/'+texte,
                success: function(data) {
                    $("#PageMetaTitle").val(texte);
                    $("#PageAlias").val(data);
                }
            });
        });
        $("#PagePageTypeId").change(function () {
            var moduleId = $(this).data("type-module"),
                selectedId = $(this).find("option:selected").val(),
                $templates = $("#modules_templates");
                
            if (Number(selectedId) === Number(moduleId)) {
                $templates.show();
                $templates.find("input:radio:first").click();
            } else {
                $templates.hide();
                $templates.find("input:radio:checked").attr("checked", false);
            }
        });
    });
<?php echo $this->Html->scriptEnd(); ?>