<?php echo $this->Form->create('Trombinoscope', array('type' => 'file')); ?>
    <!-- // Titrage + boutons d'actions + sous-nav -->
    <header>
        <h1>Ajouter une personne</h1>
        <nav class="clearfix">
            <ul id="nav-actions" class="clearfix">
                <li class="other">
                    <?php echo $this->Html->link('Retour', array(
                        'controller' => 'trombinoscopes',
                        'action' => 'index',
                        'admin' => true,
                    )); ?>
                </li>
                <li>
                    <?php echo $this->Form->submit('Enregistrer'); ?>
                </li>
            </ul>
        </nav>
    </header>
    
    <div>
        <?php echo $this->Session->flash(); ?>
        <div class="row-fluid">
            <div class="span12">
                <h2>Remplissez les informations de la personne à ajouter</h2>
                <fieldset class="block">
                    <?php echo $this->Form->input('Trombinoscope.nom', array('label' => 'Nom', 'class' => 'span4', 'type' => 'text')); ?>
                    <?php echo $this->Form->input('Trombinoscope.fonction', array('label' => 'Fonction', 'class' => 'span4', 'type' => 'text')); ?>
                    <?php echo $this->Form->input('Trombinoscope.photo', array('label' => 'Photo', 'class' => 'span4', 'type' => 'file')); ?>
                </fieldset>
            </div>
        </div>
    </div>
<?php echo $this->Form->end(); ?>