<?php echo $this->Form->create('Trombinoscope', array('type' => 'file')); ?>
    <!-- // Titrage + boutons d'actions + sous-nav -->
    <header>
        <h1>Modifier une personne</h1>
        <nav class="clearfix">
            <ul id="nav-actions" class="clearfix">
                <li class="other">
                    <?php echo $this->Html->link('Retour', array(
                        'controller' => 'trombinoscopes',
                        'action' => 'index',
                        'admin' => true,
                    )); ?>
                </li>
                <li>
                    <?php echo $this->Form->submit('Enregistrer'); ?>
                </li>
            </ul>
        </nav>
    </header>
    
    <div>
        <?php echo $this->Session->flash(); ?>
        <div class="row-fluid">
            <div class="span12">
                <h2>Modifiez les informations nécessaires</h2>
                <fieldset class="block">
                    <?php
                    echo $this->Form->input('Trombinoscope.nom', array('label' => 'Nom', 'class' => 'span4', 'type' => 'text'));
                    echo $this->Form->input('Trombinoscope.fonction', array('label' => 'Fonction', 'class' => 'span4', 'type' => 'text'));
                    echo $this->Form->input('Trombinoscope.photo', array('label' => 'Photo', 'class' => 'span4', 'type' => 'file'));
                    
                    if (!empty($this->request->data['Trombinoscope']['photo'])): ?>
                        <h3>Photo actuelle : </h3>
                        <?php echo $this->Html->image($this->request->data['Trombinoscope']['photo']);
                    endif; ?>
                </fieldset>
            </div>
        </div>
    </div>
<?php echo $this->Form->end(); ?>