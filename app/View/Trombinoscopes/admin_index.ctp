<?php
// Titrage + boutons d'actions + sous-nav
echo $this->start('header'); ?>
    <h1>Gestion du trombinoscope</h1>
    <nav class="clearfix">
        <ul id="sous-nav" class="clearfix">
            <li>
                <p>Gérez les membres de votre trombinoscope</p>
            </li>
        </ul>
        <ul id="nav-actions" class="clearfix">
            <li><?php echo $this->Html->link('Ajouter une personne', array('action' => 'add')); ?>
            </li>
        </ul>
    </nav>
<?php echo $this->end(); ?>

<h2 class="add">Liste des personnes</h2>

<?php echo $this->Session->flash(); ?>

<ul class="pages-sortable">
<?php foreach($trombinoscopes as $k => $trombinoscope): ?>

    <?php echo (($k + 1) == sizeof($trombinoscopes)) ? '<li class="last">' : '<li>'; ?>
        <p>
            <span><?php echo $trombinoscope['Trombinoscope']['nom'] . " - " . $trombinoscope['Trombinoscope']['fonction']; ?></span>
        </p>
        <ul>
            <li class="hide">
                <?php echo $this->Html->link('Haut', array('action' => 'move', $trombinoscope['Trombinoscope']['id'], 'up'), array('class' => 'up half', 'title' => "Monter la personne d'un cran")); ?>
                <?php echo $this->Html->link('Bas', array('action' => 'move', $trombinoscope['Trombinoscope']['id'], 'down'), array('class' => 'down half', 'title' => "Descendre la personne d'un cran")); ?>
            </li>
            <?php if(AuthComponent::user('role') == "admin"): ?>
                <li class="hide"><?php echo $this->Html->link('Modifier', array('action' => 'edit', $trombinoscope['Trombinoscope']['id'])); ?></li>
                <li class="hide"><?php echo $this->Html->link('Supprimer', array('action' => 'delete', $trombinoscope['Trombinoscope']['id']), array(), "Supprimer cette personne ?"); ?></li>
            <?php endif; ?>
            <li class="options"><?php echo $this->Html->link('Options', "#"); ?></li>
        </ul>
    </li>
<?php endforeach; ?>
</ul>
<?php echo $this->Html->scriptStart(array('inline' => false)); ?>
    $(document).ready(function() {
        $(document).on("click", ".up, .down", function() {
            var url = $(this).attr('href');
            $.ajax({
                type: "GET",
                url: url,
                beforeSend: function() {
                    $(".pages-sortable").css({ opacity: .5 });
                },
                success: function(html) {
                    $(".pages-sortable").html(html).css({ opacity: 1 });
                }
            });
            return false;
        });
    });
<?php echo $this->Html->scriptEnd(); ?>