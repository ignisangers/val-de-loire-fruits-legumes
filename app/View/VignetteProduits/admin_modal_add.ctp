<div class="modal-header">
    <h3>Rognez votre image</h3>
</div>
<div class="modal-body">
    <?php echo $this->Form->create(null, array("class" => "modal-form")); ?>
        <?php
        echo $this->Form->hidden("VignetteProduitAdd.produit_id");
        echo $this->Form->hidden("VignetteProduitAdd.filename", array("class" => "crop-hidden"));
//        echo $this->Form->input("VignetteProduitAdd.caption", array(
//            "label" => "Légende de votre photo (facultatif)",
//            "class" => "modal-body__input"
//        ));
        ?>
        <p><b>Déplacez</b> et <b>redimensionnez</b> le carré de sélection sur votre image, puis <b>validez</b>.</p>
        <div class="crop-image"></div>
    <?php echo $this->Form->end(); ?>
</div>
<div class="modal-footer">
    <button class="button modal-submit--add" data-type="vignette_produits">Valider l'opération</button>
    <button class="button button--secondary" onclick="Ignis.Modal.close(); return false;">Annuler</button>
</div>