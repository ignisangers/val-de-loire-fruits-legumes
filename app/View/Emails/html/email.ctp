<?php
if($to == "away") {
    $this->set('title_for_layout', "Confirmation d'envoi");
} else {
    $this->set('title_for_layout', "Message du site Nature et Stratégie");
}
?>
<tr>
    <td width="30">&nbsp;</td>
    <td>&nbsp;</td>
    <td width="540">
        <p style="font-size: 12px; margin: 0; color: #333333;">
            <?php
            if($to == "away") {
                echo "Veuillez trouver ci-après la copie de votre message :";
            } else {
                echo "Contenu du message :";
            }
            ?>
        </p>
    </td>
    <td width="30">&nbsp;</td>
</tr>
<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>
        <br><p style="font-size: 11px; margin: 0; color: #333333; margin-left: 25px;"><strong style="color: #000000;">- Identité :</strong> <?php echo mb_strtoupper($nom, 'UTF-8') . ' ' . $prenom; ?></p>
    </td>
    <td>&nbsp;</td>
</tr>
<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>
        <br><p style="font-size: 11px; margin: 0; color: #333333; margin-left: 25px;"><strong style="color: #000000;">- Adresse e-mail :</strong> <?php echo $email; ?></p>
    </td>
    <td>&nbsp;</td>
</tr>
<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>
        <br><p style="font-size: 11px; margin: 0; color: #333333; margin-left: 25px;"><strong style="color: #000000;">- Numéro de téléphone :</strong> <?php echo (!empty($telephone)) ? $telephone: '-'; ?></p>
    </td>
    <td>&nbsp;</td>
</tr>
<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>
        <br><p style="font-size: 11px; margin: 0; color: #333333; margin-left: 25px;"><strong style="color: #000000;">- Société :</strong> <?php echo (!empty($societe)) ? $societe: '-'; ?></p>
    </td>
    <td>&nbsp;</td>
</tr>
<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>
        <br><p style="font-size: 11px; margin: 0; color: #333333; margin-left: 25px;"><strong style="color: #000000;">- Message :</strong><br /><?php echo nl2br($comment); ?></p>
    </td>
    <td>&nbsp;</td>
</tr>
<tr>
    <td colspan="4" height="20">&nbsp;</td>
</tr>