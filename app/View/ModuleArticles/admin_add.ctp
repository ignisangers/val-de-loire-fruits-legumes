<?php echo $this->Form->create('Page'); ?>
    <?php
    // Titrage + boutons d'actions + sous-nav
    echo '<header>';
        echo '<h1>Gestion des pages : '. $this->Html->link($page['Page']['name'], array(
            'controller' => 'pages',
            'action' => 'edit_modules',
            $page_id
        )) .'</h1>';
        echo '<nav class="clearfix">';
            echo '<ul id="sous-nav" class="clearfix">';
                echo '<li>';
                    echo $this->Html->link('Nouveau article', $this->request->here, array('class' => 'current'));
                echo '</li>';
            echo '</ul>';
            echo '<ul id="nav-actions" class="clearfix">';
                echo '<li class="other">';
                    echo $this->Html->link('Retour', array(
                        'controller' => 'pages',
                        'action' => 'edit_modules',
                        $page_id
                    ));
                echo '</li>';
                echo '<li>';
                    echo $this->Form->submit('Enregistrer');
                echo '</li>';
            echo '</ul>';
        echo '</nav>';
    echo '</header>';
    ?>

    <?php
    echo $this->Form->hidden('Content.page_id', array('value' => $page_id));
    echo $this->Form->hidden('Content.zone', array('value' => $zone));
    echo $this->Form->hidden('Content.model', array('value' => 'ModuleArticle'));
    echo $this->Form->hidden('Content.rang', array('value' => 1));
    echo $this->Form->input('Content.id');
    echo $this->Form->input('ModuleArticle.id');
    echo '<div><div>';
    ?>
        <?php echo $this->Session->flash(); ?>
        <div class="row-fluid">
            <div class="span12">
                <h2>Rédigez votre article</h2>
                <fieldset class="block">
                    <?php
                    echo $this->Form->input('ModuleArticle.name', array('label' => 'Titre de l\'article', 'class' => 'span8'));
                    echo $this->Form->input('ModuleArticle.resume', array('label' => 'Résumé de l\'article', 'class' => 'span12', 'rows' => 4));
                    echo $this->Form->input('ModuleArticle.content', array('label' => 'Contenu de l\'article', 'class' => 'redactor span12'));
                    ?>
                </fieldset>
            </div>
        </div>
    <?php echo '</div></div>'; ?>
<?php echo $this->Form->end(); ?>


<?php echo $this->Html->scriptStart(array('inline' => false)); ?>
    $(function () {
        $('.redactor').redactor({
            minHeight: 200,
            lang: 'fr',
            buttons: ['formatting', '|', 'bold', 'italic', 'underline', 'deleted', '|', 'unorderedlist', 'orderedlist', '|', 'outdent', 'indent', 'link', 'alignment', '|', 'image', 'video'],
            imageUpload: '<?php echo Router::url(array("controller" => "images", "action" => "upload_image_redactor", "admin" => true), true); ?>',            imageUploadErrorCallback: function(json) {
                alert(json.message);
            }, 
            linebreaks: true,
            dragUpload: true
        });
    });
<?php
echo $this->Html->scriptEnd(); ?>