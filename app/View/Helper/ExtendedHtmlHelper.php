<?php
App::uses('HtmlHelper', 'View/Helper');

class ExtendedHtmlHelper extends HtmlHelper {

    public function url($url = null, $full = false) {
        if (!isset($url['language']) && is_array($url) && Configure::check('Config.language')) {
            $url['language'] = Configure::read('Config.language');
        } else if(!is_array($url) && Configure::check('Config.language')) {
            $languageExists = false;
            foreach(Configure::read('Config.languages') as $code => $language):
                if(preg_match("#/". $code ."/#", $url)) {
                    $languageExists = true;
                    break;
                }
            endforeach;
            if(!$languageExists) {
                $url = "/" . Configure::read('Config.language') . "/" . $url;
            }
        }
        return parent::url($url, $full);
    }

}
?>