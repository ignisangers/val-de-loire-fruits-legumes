<?php echo $this->Form->create('Page'); ?>
<header>
    <h1>Gestion des pages : <?php echo $this->Html->link($page['Page']['name'], array(
        'controller' => 'pages',
        'action' => 'edit_modules',
        $page_id
    )); ?></h1>
    <nav class="clearfix">
        <ul id="sous-nav" class="clearfix">
            <li>
                <?php echo $this->Html->link("Nouveau module de trombinoscope", $this->request->here, array('class' => 'current')); ?>
            </li>
        </ul>
        <ul id="nav-actions" class="clearfix">
            <li class="other">
                <?php echo $this->Html->link('Retour', array(
                    'controller' => 'pages',
                    'action' => 'edit_modules',
                    $page_id
                )); ?>
            </li>
            <li>
                <?php echo $this->Form->submit('Enregistrer'); ?>
            </li>
        </ul>
    </nav>
</header>

<?php
echo $this->Form->hidden('Content.page_id', array('value' => $page_id));
echo $this->Form->hidden('Content.zone', array('value' => $zone));
echo $this->Form->hidden('Content.model', array('value' => 'ModuleTrombinoscope'));
echo $this->Form->hidden('Content.rang', array('value' => 1));
echo $this->Form->input('Content.id');
echo $this->Form->input('ModuleTrombinoscope.id');
?>
<div>
    <div>
        <?php echo $this->Session->flash(); ?>
        <div class="row-fluid">
            <div class="span12">
                <h2>Configurez votre module de trombinoscope</h2>
                <fieldset class="block">
                    <?php
                    echo $this->Form->input('ModuleTrombinoscope.title', array('label' => "Titre du trombinoscope", 'class' => 'span4'));
                    ?>
                </fieldset>
            </div>
        </div>
    </div>
</div>
<?php echo $this->Form->end(); ?>