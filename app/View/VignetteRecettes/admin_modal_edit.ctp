<div class="modal-header">
    <h3>Rognez votre image</h3>
</div>
<div class="modal-body">
    <?php echo $this->Form->create(null, array("class" => "modal-form")); ?>
        <div class="row-fluid">
            <div class="span2">
                <?php
                echo $this->Html->image("../files/recettes/vignette/" . $this->request->data["VignetteRecette"]["filename"], array(
                    "alt" => "",
                    "id" => "GalleryEditThumb",
                    "style" => "width: 100%;"
                ));
                ?>
            </div>
            <div class="span10">
                <?php
                echo $this->Form->hidden("VignetteRecette.id");
                echo $this->Form->hidden("VignetteRecette.filename", array("class" => "crop-hidden", "value" => "","data-type" => "vignette_recettes"));
//                echo $this->Form->input("VignetteRealisation.caption", array(
//                    "label" => "Légende de votre photo (facultatif)",
//                    "class" => "modal-body__input"
//                ));
                echo $this->Form->input("VignetteRecette.filechange", array(
                    "label" => "Changer de photo",
                    "type" => "file",
                    "class" => "crop-change",
                    "data-type" => "vignette_recettes"
                ));
                ?>
            </div>
        </div>
        <div class="crop-image"></div>
    <?php echo $this->Form->end(); ?>
</div>
<div class="modal-footer">
    <button class="button modal-submit--edit" data-type="vignette_recettes">Valider l'opération</button>
    <button class="button button--secondary" onclick="Ignis.Modal.close(); return false;">Annuler</button>
</div>