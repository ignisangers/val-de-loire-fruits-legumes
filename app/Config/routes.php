<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */

        /**
         * GESTION PAGE D'ACCUEIL
         */
//	Router::connect('/', array('controller' => 'pages', 'action' => 'index'));
        Router::redirect('/', array('controller' => 'pages', 'action' => 'index', 'language' => 'fra'), array('status' => 301));
        
        /**
         * PAGES SPÉCIFIQUES
         */
        Router::connect('/:language/pages/sitemap',
                array('controller' => 'pages', 'action' => 'sitemap'),
                array('language' => '[a-z]{3}'));
        Router::connect('/:language/pages/contact',
                array('controller' => 'pages', 'action' => 'contact'),
                array('language' => '[a-z]{3}'));
        Router::connect('/:language/pages/adherent',
                array('controller' => 'pages', 'action' => 'adherent'),
                array('language' => '[a-z]{3}'));
        /**
         * ACTUALITÉS
         */
        Router::connect('/:language/actualites',
                array('controller' => 'actualites', 'action' => 'index'),
                array('language' => '[a-z]{3}'));
        Router::connect('/:language/actualites',
                array('controller' => 'actualites', 'action' => 'index', 'page' => 1),
                array('language' => '[a-z]{3}'));
        Router::connect('/:language/actualites/:page',
                array('controller' => 'actualites', 'action' => 'index'),
                array('language' => '[a-z]{3}', 'page' => '[0-9]+'));
        Router::connect('/:language/actualites/*',
                array('controller' => 'actualites', 'action' => 'display'),
                array('language' => '[a-z]{3}'));
        
        /**
         * PRODUIT
         */
        Router::connect('/:language/produits',
                array('controller' => 'produits', 'action' => 'index'),
                array('language' => '[a-z]{3}'));
        Router::connect('/:language/produits/*',
                array('controller' => 'produits', 'action' => 'display'),
                array('language' => '[a-z]{3}'));
        
        /**
         * RECETTE
         */
        Router::connect('/:language/recettes',
                array('controller' => 'recettes', 'action' => 'index', 'page' => 1),
                array('language' => '[a-z]{3}'));
        Router::connect('/:language/recettes/*',
                array('controller' => 'recettes', 'action' => 'display'),
                array('language' => '[a-z]{3}'));
        
        /**
         * AGENDA
         */
        Router::connect('/:language/agenda/*',
                array('controller' => 'evenements', 'action' => 'display'),
                array('language' => '[a-z]{3}'));
        
        /**
         * CAS GÉNÉRAL
         */
        Router::connect('/:language/pages/*',
                array('controller' => 'pages', 'action' => 'display'),
                array('language' => '[a-z]{3}'));

        Router::connect('/:language',
                array('controller' => 'pages', 'action' => 'index'),
                array('language' => '[a-z]{3}'));
        
        /**
         * ADMIN
         */        
        Router::connect('/admin', array('controller' => 'users', 'action' => 'login', 'admin' => true));
        Router::connect('/admin/:language/:controller/:action/*',
                array('controller' => 'controller', 'action' => 'action', 'admin' => true),
                array('language' => '[a-z]{3}'));       
        
/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
        Router::parseExtensions('pdf');
