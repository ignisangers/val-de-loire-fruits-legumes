-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mer 30 Septembre 2015 à 15:56
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `cms_ignis_starter`
--

-- --------------------------------------------------------

--
-- Structure de la table `actualites`
--

CREATE TABLE IF NOT EXISTS `actualites` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `content` text NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `contents`
--

CREATE TABLE IF NOT EXISTS `contents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `model` varchar(50) NOT NULL,
  `zone` tinyint(4) NOT NULL,
  `rang` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Récapitulatif des modules par zone de chaque page' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `evenements`
--

CREATE TABLE IF NOT EXISTS `evenements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content` text NOT NULL,
  `date_debut` datetime NOT NULL,
  `date_fin` datetime DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `files`
--

CREATE TABLE IF NOT EXISTS `files` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fichier` varchar(255) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `type` varchar(255) NOT NULL,
  `rang` int(10) unsigned NOT NULL,
  `module_file_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `galleries`
--

CREATE TABLE IF NOT EXISTS `galleries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `module_gallery_id` int(11) NOT NULL,
  `caption` varchar(150) DEFAULT NULL,
  `rang` tinyint(4) NOT NULL,
  `filename` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `groupes`
--

CREATE TABLE IF NOT EXISTS `groupes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `adresse` varchar(255) DEFAULT NULL,
  `code_postal` varchar(5) DEFAULT NULL,
  `ville` varchar(255) DEFAULT NULL,
  `telephone` varchar(20) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `autres` text,
  `rang` int(10) unsigned NOT NULL,
  `module_groupe_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `i18n`
--

CREATE TABLE IF NOT EXISTS `i18n` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `locale` varchar(6) NOT NULL,
  `model` varchar(255) NOT NULL,
  `foreign_key` int(10) NOT NULL,
  `field` varchar(255) NOT NULL,
  `content` text,
  PRIMARY KEY (`id`),
  KEY `locale` (`locale`),
  KEY `model` (`model`),
  KEY `row_id` (`foreign_key`),
  KEY `field` (`field`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=33 ;

--
-- Contenu de la table `i18n`
--

INSERT INTO `i18n` (`id`, `locale`, `model`, `foreign_key`, `field`, `content`) VALUES
(1, 'fra', 'Page', 1, 'name', 'Accueil'),
(2, 'fra', 'Page', 1, 'alias', 'accueil'),
(3, 'fra', 'Page', 1, 'meta_title', 'Accueil'),
(4, 'fra', 'Page', 1, 'meta_description', ''),
(5, 'eng', 'Page', 1, 'name', 'Home'),
(6, 'eng', 'Page', 1, 'alias', 'home'),
(7, 'eng', 'Page', 1, 'meta_title', 'Home'),
(8, 'eng', 'Page', 1, 'meta_description', ''),
(9, 'fra', 'Page', 2, 'name', 'Mentions légales'),
(10, 'fra', 'Page', 2, 'alias', 'mentions-legales'),
(11, 'fra', 'Page', 2, 'meta_title', 'Mentions légales'),
(12, 'fra', 'Page', 2, 'meta_description', ''),
(13, 'eng', 'Page', 2, 'name', 'Mentions légales'),
(14, 'eng', 'Page', 2, 'alias', 'mentions-legales'),
(15, 'eng', 'Page', 2, 'meta_title', 'Mentions légales'),
(16, 'eng', 'Page', 2, 'meta_description', ''),
(17, 'fra', 'Page', 3, 'name', 'Contact'),
(18, 'fra', 'Page', 3, 'alias', 'contact'),
(19, 'fra', 'Page', 3, 'meta_title', 'Contact'),
(20, 'fra', 'Page', 3, 'meta_description', ''),
(21, 'eng', 'Page', 3, 'name', 'Contact'),
(22, 'eng', 'Page', 3, 'alias', 'contact'),
(23, 'eng', 'Page', 3, 'meta_title', 'Contact'),
(24, 'eng', 'Page', 3, 'meta_description', ''),
(25, 'fra', 'Page', 4, 'name', 'Plan du site'),
(26, 'fra', 'Page', 4, 'alias', 'plan-du-site'),
(27, 'fra', 'Page', 4, 'meta_title', 'Plan du site'),
(28, 'fra', 'Page', 4, 'meta_description', ''),
(29, 'eng', 'Page', 4, 'name', 'Plan du site'),
(30, 'eng', 'Page', 4, 'alias', 'plan-du-site'),
(31, 'eng', 'Page', 4, 'meta_title', 'Plan du site'),
(32, 'eng', 'Page', 4, 'meta_description', '');

-- --------------------------------------------------------

--
-- Structure de la table `modules`
--

CREATE TABLE IF NOT EXISTS `modules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `model` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Modules disponibles/activés' AUTO_INCREMENT=6 ;

--
-- Contenu de la table `modules`
--

INSERT INTO `modules` (`id`, `name`, `model`) VALUES
(1, 'Article', 'ModuleArticle'),
(2, 'Lien', 'ModuleLien'),
(3, 'Galerie', 'ModuleGallery'),
(4, 'Actualités', 'ModuleActualite'),
(5, 'Grande image', 'ModulePicture');

-- --------------------------------------------------------

--
-- Structure de la table `module_actualites`
--

CREATE TABLE IF NOT EXISTS `module_actualites` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_id` int(10) unsigned NOT NULL,
  `afficher_nb` smallint(5) unsigned NOT NULL DEFAULT '5' COMMENT 'Nombre d''acutalités affichées',
  `pagination` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `module_articles`
--

CREATE TABLE IF NOT EXISTS `module_articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `resume` text,
  `content` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `module_contacts`
--

CREATE TABLE IF NOT EXISTS `module_contacts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `module_evenements`
--

CREATE TABLE IF NOT EXISTS `module_evenements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_id` int(10) unsigned NOT NULL,
  `afficher_nb` smallint(5) unsigned NOT NULL,
  `pagination` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `module_files`
--

CREATE TABLE IF NOT EXISTS `module_files` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_id` int(10) unsigned NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `module_galleries`
--

CREATE TABLE IF NOT EXISTS `module_galleries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `module_groupes`
--

CREATE TABLE IF NOT EXISTS `module_groupes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_id` int(10) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `module_htmls`
--

CREATE TABLE IF NOT EXISTS `module_htmls` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_id` int(10) unsigned NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `module_liens`
--

CREATE TABLE IF NOT EXISTS `module_liens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_id` int(11) NOT NULL,
  `name` varchar(150) DEFAULT NULL,
  `content` text,
  `page_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `module_pictures`
--

CREATE TABLE IF NOT EXISTS `module_pictures` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_id` int(11) NOT NULL,
  `name` varchar(150) DEFAULT NULL,
  `caption` varchar(150) DEFAULT NULL,
  `content` text,
  `image` varchar(150) NOT NULL,
  `full` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Image sur toute la largeur ?',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `module_trombinoscopes`
--

CREATE TABLE IF NOT EXISTS `module_trombinoscopes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_id` int(10) unsigned NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `module_videos`
--

CREATE TABLE IF NOT EXISTS `module_videos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_id` int(10) unsigned NOT NULL,
  `media_code` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `alias` varchar(255) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `template_id` int(11) DEFAULT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` text,
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `menu` tinyint(1) NOT NULL DEFAULT '1',
  `home` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Est-ce la page d''accueil ?',
  `page_type_id` int(11) NOT NULL DEFAULT '1' COMMENT 'Contenu de la page : gestion de modules, page d''accueil, pagination actualités...',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `pages`
--

INSERT INTO `pages` (`id`, `name`, `alias`, `parent_id`, `template_id`, `meta_title`, `meta_description`, `lft`, `rght`, `active`, `menu`, `home`, `page_type_id`) VALUES
(1, 'Accueil', 'accueil', NULL, 1, 'Accueil', '', 1, 2, 1, 1, 1, 1),
(2, 'Mentions légales', 'mentions-legales', NULL, 2, 'Mentions légales', '', 3, 4, 1, 0, 0, 1),
(3, 'Contact', 'contact', NULL, NULL, 'Contact', '', 5, 6, 1, 0, 0, 4),
(4, 'Plan du site', 'plan-du-site', NULL, NULL, 'Plan du site', '', 7, 8, 1, 0, 0, 3);

-- --------------------------------------------------------

--
-- Structure de la table `page_types`
--

CREATE TABLE IF NOT EXISTS `page_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(30) NOT NULL,
  `name` varchar(200) NOT NULL COMMENT 'Pour l''admin seulement',
  `description` text COMMENT 'Pour l''admin seulement',
  `link` varchar(150) DEFAULT NULL COMMENT 'Pour l''admin seulement',
  `controller` varchar(100) DEFAULT NULL,
  `action` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `page_types`
--

INSERT INTO `page_types` (`id`, `slug`, `name`, `description`, `link`, `controller`, `action`) VALUES
(1, 'modules', 'Gestion de modules (défaut)', NULL, NULL, NULL, NULL),
(2, 'news', 'Actualités', 'Le contenu de cette page n''est pas directement administrable. Elle affichera simplement la liste des actualités, avec pagination si nécessaire.', 'Administrer la liste des actualités', 'actualites', 'index'),
(3, 'sitemap', 'Plan du site', 'Générer automatiquement une arborescence de liens des pages du site.', NULL, 'pages', 'sitemap'),
(4, 'contact', 'Contact', NULL, NULL, 'pages', 'contact');

-- --------------------------------------------------------

--
-- Structure de la table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(20) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `roles`
--

INSERT INTO `roles` (`id`, `role`, `name`) VALUES
(1, 'admin', 'Administrateur'),
(2, 'client', 'Client'),
(3, 'rédacteur', 'Rédacteur');

-- --------------------------------------------------------

--
-- Structure de la table `templates`
--

CREATE TABLE IF NOT EXISTS `templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `libelle` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Mises en page disponibles/activées' AUTO_INCREMENT=5 ;

--
-- Contenu de la table `templates`
--

INSERT INTO `templates` (`id`, `name`, `libelle`) VALUES
(1, 'accueil', 'Accueil'),
(2, 'interieur', 'Page par défaut'),
(3, 'interieur-parent', 'Page avec liste des pages inférieures'),
(4, 'interieur-enfants', 'Page avec liste pages même niveau');

-- --------------------------------------------------------

--
-- Structure de la table `trombinoscopes`
--

CREATE TABLE IF NOT EXISTS `trombinoscopes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `fonction` varchar(255) DEFAULT NULL,
  `photo` text,
  `rang` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `role` enum('admin','client','rédacteur') DEFAULT 'client',
  `nom` varchar(150) NOT NULL,
  `prenom` varchar(150) NOT NULL,
  `avatar` varchar(200) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `role`, `nom`, `prenom`, `avatar`, `created`, `modified`) VALUES
(1, 'ignis', '5c47bf46b8ac596a4930c3de0d1412a679704abc', 'admin', 'Communication', 'Ignis', NULL, '2013-05-07 00:00:00', '2015-09-30 15:32:43'),
(2, 'web', '23d5625a13485c1bb69aeb1df66ca64c583a0a50', 'client', 'Master', 'Web', NULL, '2013-12-03 09:05:39', '2013-12-03 10:57:47');
