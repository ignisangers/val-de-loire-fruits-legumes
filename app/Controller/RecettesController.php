<?php

App::uses('AppController', 'Controller');

class RecettesController extends AppController {

    public $components = array("Paginator");
    public $paginate = array(
        'limit' => 10,
        'order' => 'name ASC'
    );

    public function beforeFilter() {
        parent::beforeFilter();

        // Pagination pour l'action index()
        if (isset($this->request->params['page'])) {
            $this->request->params['named']['page'] = $this->request->params['page'];
        }
    }

    /**
     * SITE
     * Page affichant la liste paginée des recettes
     */
    public function index() {

        // Construction de la page
        $this->loadModel('Page');
        $this->set($this->Page->construct($this->Recette->pageConditions));

        // Liste des recettes
        $this->Paginator->settings = $this->paginate;
        $recette = $this->Paginator->paginate('Recette');
        $this->set(compact("recette"));

        // Affichage du template (AppController)
        $this->setTemplate("recette");
    }

    /**
     * SITE
     * Page affichant une recette
     */
    public function display($slug = null) {

        //recette
        $recette = $this->Recette->findBySlug($slug);
        if (empty($recette)) {
            throw new NotFoundException("Nous n'avons pu trouver cette recette.");
        }
        //recette de saison ou non
        $recetteSaison = false;
        if ($recette['Recette']['debut'] > $recette['Recette']['fin']) {
            if (date('m') >= $recette['Recette']['debut'] || date('m') <= $recette['Recette']['fin']) {
                $recetteSaison = true;
            }
        } else {
            if (date('m') >= $recette['Recette']['debut'] && date('m') <= $recette['Recette']['fin']) {
                $recetteSaison = true;
            }
        }
        //conversion des date de début et de fin 
        $mois = array(
            1 => 'janvier',
            2 => 'fevrier',
            3 => 'mars',
            4 => 'avril',
            5 => 'mai',
            6 => 'juin',
            7 => 'juillet',
            8 => 'aout',
            9 => 'septembre',
            10 => 'octobre',
            11 => 'novembre',
            12 => 'decembre',
        );
        foreach ($mois as $key => $moi) {
            if ($recette['Recette']['debut'] == $key) {
                $recette['Recette']['debut'] = $moi;
            }
            if ($recette['Recette']['fin'] == $key) {
                $recette['Recette']['fin'] = $moi;
            }
        }

//        debug(date);
//        debug($recette);exit;
        $this->set(compact("recette"));

        /**
         * Construction de la page :
         * - Soit on utilise comme référence la page recettes
         * - Soit on utilise la page accueil (par défaut)
         */
        $this->loadModel('Page');
//        $recettePage = $this->Page->getInfo($this->Recette->pageConditions);
//        $recetteConditions = (!empty($recettePage)) ? $this->Recette->pageConditions : array();
//        $this->set($this->Page->construct($recetteConditions, $recette['Recette']['name']));
        $this->set($this->Page->construct(array('Page.id' => 6), $recette['Recette']['name']));

        // On va compléter le fil d'ariane, puisqu'une recette n'est pas une page "réelle"
        $extBreadcrumb = array(
            array(
                "name" => $recette['Recette']['name'],
                "link" => array(
                    "controller" => "recettes",
                    "action" => "display",
                    $slug
                )
            )
        );
        //@todo recuperation des recettes de saison
        $listeRecettes = $this->Recette->find('all', array(
            'conditions' => array(
//                    "Recette.id" => $recetteIds
//                "Recette.debut <=" => date('m'),
//                "Recette.fin >=" => date('m')
            ),
//            'limit' => 10
        ));
        $recettesSaison = array();
        $recetteCount = 0;
        foreach ($listeRecettes as $listeRecette) {
            if ($recetteCount <= 8) {
                if ($listeRecette['Recette']['debut'] > $listeRecette['Recette']['fin']) {
                    if (date('m') >= $listeRecette['Recette']['debut'] || date('m') <= $listeRecette['Recette']['fin']) {
                        $recettesSaison[] = $listeRecette;
                        $recetteCount ++;
                    }
                } else {
                    if (date('m') >= $listeRecette['Recette']['debut'] && date('m') <= $listeRecette['Recette']['fin']) {
                        $recettesSaison[] = $listeRecette;
                        $recetteCount ++;
                    }
                }
            }
        }
        //@todo recuperation d'un produit de saison (contenu dans la recette)
        //recuperation d'un produit de saison aleatoire
        $this->loadModel('Produit');
        $produits = $this->Produit->find('all');
        $produitsIds = array();
        foreach ($produits as $produit) {//produits de saison
            if ($produit['Produit']['debut'] > $produit['Produit']['fin']) {
                if (date('m') >= $produit['Produit']['debut'] || date('m') <= $produit['Produit']['fin']) {
                    $produitsIds[$produit['Produit']['id']] = $produit['Produit']['id'];
                }
            } else {
                if (date('m') >= $produit['Produit']['debut'] && date('m') <= $produit['Produit']['fin']) {
                    $produitsIds[$produit['Produit']['id']] = $produit['Produit']['id'];
                }
            }
        }
        $produitSaison = array();
        if (!empty($produitsIds)) {
            $produitId = array_rand($produitsIds);
            $produitSaison = $this->Produit->find('first', array(
                'recursive' => 2,
                'conditions' => array(
                    'Produit.id' => $produitId,
            )));
        }
//        debug($produitSaison);exit;

        $this->set(compact("extBreadcrumb", "recette", "recetteSaison", "recettesSaison", "produitSaison"));

        // Affichage du template (AppController)
        $this->setTemplate("recette");
    }

    /**
     * Génération du pdf recette
     * 
     * @param int $id id de la recette
     */
    public function recettePdf($id) {

        $this->layout = false;

        $recette = $this->Recette->find('first', array(
            'recursive' => 2,
            'conditions' => array('Recette.id' => $id)
        ));

        if (empty($recette)) {
            $this->Session->setFlash("Erreur : recette introuvable", 'flashError', array());
            $this->redirect($this->referer());
        }

        $this->set("recette", $recette);

        // Configuration PDF
        Configure::write('CakePdf', array(
            'engine' => 'CakePdf.DomPdf',
            'orientation' => 'portrait',
            'pageSize' => 'A4',
            'filename' => 'recette.pdf',
            'download' => false
        ));
    }

    /**
     * Listing des recettes actuellement existantes
     * sous forme de pagination
     */
    public function admin_index() {

        $title_for_layout = "Gestion des recettes";

//        $recettes = $this->Recette->find('all');
//        debug($recettes);exit;

        $this->Paginator->settings = $this->paginate;
        $recettes = $this->Paginator->paginate('Recette');
        $this->set(compact('recettes', 'title_for_layout'));
    }

    /**
     * Ajout d'une recette
     */
    public function admin_add() {
        $title_for_layout = "Ajout d'une recette";
        $this->loadModel("Produit");
        $this->loadModel("RecetteProduit");
        if ($this->request->is('post')) {
            $this->Recette->id = $this->request->data['Recette']['id'];
            $this->request->data['Recette']['slug'] = strtolower(Inflector::slug($this->request->data['Recette']['name'], "-"));            
            if ($this->Recette->save($this->request->data)) {
                //association RecetteProduit
                $this->RecetteProduit->addRecetteProduits($this->Recette->id, $this->request->data['RecetteProduit']);

                $this->Session->setFlash("Votre recettes a bien été ajoutée.", "Alerts/Success");
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash("Erreur lors de l'enregistrement de votre recette.", "Alerts/Error");
            }
        } else {
            //on crée immédiatement la recette pour pouvoir le lier à une image 
            $recette = $this->Recette->save(array(
                'name' => '',
                'slug' => '',
                'resume' => '',
                'content' => '',
            ));
            $recette_id = $recette['Recette']['id'];
        }
        //recuperations des produits pour affichage des checkbox     
        $produits = $this->Produit->find("all");
        $this->set(compact("produits"));

        // ajout des images déjà uploadée
        $this->loadModel('GallerieRecette');
        $gallery = $this->GallerieRecette->getGallerieRecette($recette_id);
        $this->set(compact("gallery"));

        // ajout de la vignette déjà uploadée
        $this->loadModel('VignetteRecette');
        $vignette = $this->VignetteRecette->getVignetteRecette($recette_id);
        $this->set(compact("vignette"));

        $this->initJsVars();
        $this->set(compact("title_for_layout", "recette_id"));
    }

    /**
     * Édition d'une recette
     * @param int $recette_id ID de recette à éditer
     */
    public function admin_edit($recette_id) {

        $title_for_layout = "Modification d'une Recette";

        $this->loadModel("Produit");
        $this->loadModel("RecetteProduit");

        // Confirmer l'existence de la recette à éditer
        $this->Recette->id = $recette_id;
        if (!$this->Recette->exists()) {
            $this->redirect(array('action' => 'index'));
        }
        //recuperation de la recette
        $recette = $this->Recette->findById($recette_id);
        //recuperations des produits pour affichage des checkbox           
        $produits = $this->Produit->find("all");
        $this->set(compact("produits"));
        // recuperation des produits déjà liés à la recette pour affichage des checkbox
        $recetteProduitIds = array();
        if (!empty($recette)) {
            foreach ($recette['RecetteProduit'] as $recetteProduit) {
                $recetteProduitIds[] = $recetteProduit['produit_id'];
            }
        }
        $this->set(compact("recetteProduitIds"));
        //traitement du formulaire
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->request->data['Recette']['slug'] = strtolower(Inflector::slug($this->request->data['Recette']['name'], "-"));
            if ($this->Recette->save($this->request->data)) {
                //association RecetteProduit
                $this->RecetteProduit->addRecetteProduits($recette_id, $this->request->data['RecetteProduit']);

                $this->Session->setFlash("Votre recette a bien été modifié.", "Alerts/Success");
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash("Erreur lors de la mise à jour de votre recette.", "Alerts/Error");
            }
        } else {
            $this->request->data = $this->Recette->read();
        }
        // ajout des images déjà uploadée
        $this->loadModel('GallerieRecette');
        $gallery = $this->GallerieRecette->getGallerieRecette($recette_id);
        $this->set(compact("gallery"));

        // ajout de la vignette déjà uploadée
        $this->loadModel('VignetteRecette');
        $vignette = $this->VignetteRecette->getVignetteRecette($recette_id);
        $this->set(compact("vignette"));

        $this->initJsVars();
        $this->set(compact('title_for_layout', 'recette_id', 'recette'));
    }

    /**
     * Suppression d'une recette
     * @param type $recette_id ID de la recette à supprimer
     */
    public function admin_delete($recette_id) {

        $this->layout = false;
        $this->autoRender = false;

        $recette = $this->Recette->find('first', array(
            'recursive' => 2,
            'conditions' => array('Recette.id' => $recette_id)
        ));

        if ($this->Recette->delete($recette_id, true)) {
            //suppression des RecetteProduit associées
            $this->loadModel('RecetteProduit');
            $this->RecetteProduit->deleteAll(array(
                'RecetteProduit.recette_id' => $recette_id,
                false
            ));
            //suppression physique des images associées au produit
            if (!empty($recette['VignetteProduit'])) {
                foreach ($recette['VignetteProduit'] as $vignette) {
                    @unlink(WWW_ROOT . 'files' . DS . 'produits/vignette' . DS . $vignette["filename"]);
                }
            }
            if (!empty($recette['GallerieProduit'])) {
                foreach ($recette['GallerieProduit'] as $gallerie) {
                    @unlink(WWW_ROOT . 'files' . DS . 'produits/gallerie' . DS . $gallerie["filename"]);
                }
            }
            $this->Session->setFlash("Cette recette a été supprimé.", "Alerts/Success");
        } else {
            $this->Session->setFlash("Cette recette n'a pas pu être supprimée ou n'existe pas.", "Alerts/Error");
        }
        $this->redirect(array('action' => 'index'));
    }

    /**
     * Setting Javascript Global Vars
     * 
     * @return void
     */
    private function initJsVars() {
        $this->setJsVar('cropConfig', array(
            'default' => array(
//                'maxNumImagesAllowed' => 1,
//                'maxFileSizeAllowed' => 10, //MBs
//                'qualityJpeg' => 0.85, //from 0.0 to 1 (normal default is 0.92)
//                'allowResize' => true,
//                'selMinSize' => array(50, 50), //pxs
//                'maxWidthPreview' => 530, //pxs
//                'maxHeightPreview' => false,
                'aspectRatio' => 16 / 9, //1 square (16/9, 4/3, etc)
//                'setSelect' => [0, 0, 100, 100], //initial cords area [x, y ,x2, y2]
//                'bgOpacity' => 0.5,
//                'bgColor' => 'black',
                'cropImageWidth' => 800, //if ommited and added cropImageHeight width is calculated according to aspectRatio
            ),
            'vignette' => array(
//                'maxNumImagesAllowed' => 1,
//                'maxFileSizeAllowed' => 10, //MBs
//                'qualityJpeg' => 0.85, //from 0.0 to 1 (normal default is 0.92)
//                'allowResize' => true,
//                'selMinSize' => array(50, 50), //pxs
//                'maxWidthPreview' => 530, //pxs
//                'maxHeightPreview' => false,
                'aspectRatio' => 1 / 1, //1 square (16/9, 4/3, etc)
//                'setSelect' => [0, 0, 100, 100], //initial cords area [x, y ,x2, y2]
//                'bgOpacity' => 0.5,
//                'bgColor' => 'black',
                'cropImageWidth' => 800, //if ommited and added cropImageHeight width is calculated according to aspectRatio
            ),
        ));
    }

}
