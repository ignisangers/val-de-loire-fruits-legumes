<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
    
    // Variables JS
    var $_jsVars = array();
    
    public $components = array(
        'RequestHandler',
        'Cookie',
        'Session',
        'Paginator',
//        'DebugKit.Toolbar',
        'Auth' => array(
            'authorize' => 'Controller',
            'loginAction' => array('controller' => 'users', 'action' => 'login', 'admin' => true),
            'logoutRedirect' => array('controller' => 'users', 'action' => 'login')
        )
    );
    public $helpers = array('Html' => array('className' => 'ExtendedHtml'), 'Text', 'Session', 'Form');
    
    public function beforeFilter() {
        
        $this->_setLanguage();
        
        if(isset($this->request->params['prefix']) && in_array($this->request->params['prefix'], array('admin'))) {
            $this->layout = (in_array($this->action, array('admin_add', 'admin_edit'))) ? "admin-form" : "admin";
        }
        if(!isset($this->request->params['prefix']) || empty($this->request->params['prefix'])) {
            $this->Auth->allow();
        }
        
    }
    
    public function beforeRender() {
        // Set the jsVars array which holds the variables to be used in js
        $this->set('jsVars', $this->_jsVars);
    }
    
    /**
     * SITE
     * Changement de langue
     */
    private function _setLanguage() {
        
        if(isset($this->params['language']))
        {
            $this->Session->write('Config.language', $this->params['language']);
            // set the application language
            Configure::write('Config.language',$this->params['language']);
        } else if(!isset($this->params['language']) && $this->Session->check('Config.language')){
            // set the application language
            Configure::write('Config.language',$this->Session->read('Config.language'));
        }
        $this->set('current_language', Configure::read('Config.language'));
        
    }
    
    /**
     * ADMIN
     * Autorisation d'accès à l'admin
     * @param int $user
     * @return boolean
     */
    public function isAuthorized($user) {
        
        if(in_array($user['role'], array('admin', 'client'))) {
            return true;
        } else {
            return false;
        }
        
    }
    
    /**
     * sets global javascript vars to be used in javascript scripts
     * 
     * @return void
     */
    public function setJsVar($name, $value) {
        $this->_jsVars[$name] = $value;
    }

    // TRAITEMENTS SPÉCIFIQUES AUX TEMPLATES    
    
    /**
     * Affichage du template d'une page + traitement spécifique le cas échéant
     * @param string $name : nom du template
     * @param array $page : toutes les infos de la page à transmettre aux fonctions spécifiques des templates
     */
    protected function setTemplate($name, $page = array()) {
        
        $this->loadModel('Template');
        // Un traitement spécifique est-il nécessaire pour ce template ?
        $template_method = str_replace("-", "_", $name);
        if (method_exists($this->Template, "template_" . $template_method)) {
            $datas = $this->Template->{"template_" . $template_method}($page);
            if (!empty($datas)) {
                foreach ($datas as $key => $data) {
                    $this->set($key, $data);
                }
            }
        }
        
        // On affiche le bon template (+ on transmet à la vue le nom du modèle)
        $this->set("template", $name);
        $this->render('/Elements/Templates/Site/site-'.$name);
    }
    
}