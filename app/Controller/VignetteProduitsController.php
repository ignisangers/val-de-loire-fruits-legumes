<?php

class VignetteProduitsController extends AppController {

    /**
     * AJAX
     * Ajout d'une nouvelle image dans la galerie
     * @param string $data["VignetteProduitAdd"]["filename"] : l'image en question
     * @param int $data["VignetteProduitAdd"]["module_gallery_id"] : ID du module galerie
     * @param string $data["VignetteProduitAdd"]["caption"] : légende de l'image
     */
    public function admin_add() {
        $this->layout = false;
        $this->autoRender = false;
        // Données à sauvegarder
        $data = $this->request->data;
        // Gestion de l'image
        $filename = $this->_uploadThumbnail($data["VignetteProduitAdd"]["filename"]);
        // On sauvegarde la nouvelle image
        if ($filename !== false) {
            $this->VignetteProduit->create();
            $saveData = array(
                "produit_id" => $data["VignetteProduitAdd"]["produit_id"],
                "filename" => $filename,
//                "caption" => $data["VignetteProduitAdd"]["caption"],
//                "rang" => 1
            );
            $this->VignetteProduit->save($saveData);

            // Puis on met à jour la galerie entière
//            $this->VignetteProduit->orderAfterCreate($data["VignetteProduitAdd"]["produit_id"], $this->VignetteProduit->id);
        }

        // On renvoie à la vue la liste à jour
        $this->_updatedList($data["VignetteProduitAdd"]["produit_id"]);
    }

    public function admin_edit() {

        $this->layout = false;
        $this->autoRender = false;

        // Données à sauvegarder
        $data = $this->request->data;
        // Gestion de l'image
        $filename = $this->_uploadThumbnail($data["VignetteProduit"]["filename"]);

//        $saveData = array(
//            "caption" => $data["VignetteProduit"]["caption"],            
//        );

        if ($filename !== false) {
            $saveData["filename"] = $filename;

            $this->VignetteProduit->id = $data["VignetteProduit"]["id"];
            $this->VignetteProduit->save($saveData);
        }

        // On renvoie à la vue la liste à jour
        $this->_updatedList($this->VignetteProduit->field("produit_id", array("id" => $data["VignetteProduit"]["id"])));
    }

    /**
     * AJAX
     * Intervertion de deux images d'une galerie
     * @param int $data["id"] : ID de l'image à bouger
     * @param int $data["module_gallery_id"] : ID du module galerie où se trouve l'image
     * @param string $data["direction"] : dans quelle direction veut-on bouger l'image (up/down) ?
     */
    public function admin_move() {

        $this->layout = false;
        $this->autoRender = false;

        // Données de suppression
        $data = $this->request->data;
        // Quelques infos de l'image
        $gallery = $this->VignetteProduit->find("first", array(
            "conditions" => array(
                "VignetteProduit.id" => $data["id"]
            ),
            "fields" => array(
                "VignetteProduit.id", "VignetteProduit.produit_id", "VignetteProduit.filename", "VignetteProduit.rang"
            )
        ));

        if ($this->VignetteProduit->exists($data["id"])) {
            // L'image en question
            $rang = $this->VignetteProduit->field("rang", array(
                "VignetteProduit.id" => $data["id"]
            ));

            // On bouge d'abord l'image adjacente dont on va prendre la place
            $this->VignetteProduit->updateAll(array(
                "VignetteProduit.rang" => $rang
                    ), array(
                "VignetteProduit.produit_id" => $gallery["VignetteProduit"]["produit_id"],
                "VignetteProduit.rang" => ($data["direction"] === "up") ? $rang - 1 : $rang + 1
            ));

            // Puis on bouge l'image souhaitée
            $this->VignetteProduit->id = $data["id"];
            $this->VignetteProduit->save(array(
                "rang" => ($data["direction"] === "up") ? $rang - 1 : $rang + 1
            ));
        }

        // On renvoie à la vue la liste à jour
        $this->_updatedList($gallery["VignetteProduit"]["produit_id"]);
    }

    /**
     * AJAX
     * Suppression d'une image de la galerie
     * @param int $data["id"] : ID de l'image à supprimer
     * @param int $data["module_gallery_id"]  : ID de la galerie où se trouve l'image
     */
    public function admin_delete() {

        $this->layout = false;
        $this->autoRender = false;

        // Données de suppression
        $data = $this->request->data;
        // Quelques infos de l'image
        $gallery = $this->VignetteProduit->find("first", array(
            "conditions" => array(
                "VignetteProduit.id" => $data["id"]
            ),
            "fields" => array(
                "VignetteProduit.id", "VignetteProduit.produit_id", "VignetteProduit.filename"
            )
        ));

        if (!empty($gallery)) {
            // On supprime l'image en BDD...
            if ($this->VignetteProduit->delete($gallery["VignetteProduit"]["id"])) {
                // ...puis physiquement
                @unlink($this->VignetteProduit->path . $gallery["VignetteProduit"]["filename"]);

                // Enfin on change l'ordre des autres images
//                $this->VignetteProduit->orderAfterDelete($data["id"], $gallery["VignetteProduit"]["rang"]);
            }
        }

        // On renvoie la vue
        $this->_updatedList($gallery["VignetteProduit"]["produit_id"]);
    }

    /**
     * AJAX - GET
     * Affichage de la modale d'ajout d'une image
     * @param int $id : ID du module galerie
     */
    public function admin_modal_add($id) {
        $this->layout = "modal";
        $this->request->data["VignetteProduitAdd"]["produit_id"] = $id;
    }

    /**
     * AJAX - GET
     * Affichage de la modale d'édition d'une image existante
     */
    public function admin_modal_edit() {
        $this->layout = "modal";

        // Informations sur l'image
        $id = $this->request->query["id"];
        $this->request->data = $this->VignetteProduit->findById($id);
        $this->set("gallery_path", $this->VignetteProduit->path);
    }

    /**
     * AJAX
     * Si on a mis une vignette, on l'upload et on l'ajoute aux données à sauvegarder
     * @param string $filename
     * @param string $imgType
     * @return mixed
     */
    private function _uploadThumbnail($filename, $imgType = null) {

        // Y a-t-il une vignette à uploader ?
        if (!is_array($filename) && !empty($filename)) {
            // On la renomme pour qu'elle soit bien formattée et unique
            preg_match('~image/(.*?);base64~', $filename, $imgType);
            $name = uniqid() . '.' . $imgType[1];

            // On récupère la photo et on l'upload
            App::uses('Folder', 'Utility');
            $photo_decoded = base64_decode(substr($filename, strpos($filename, "base64,") + 7));
            $dir = new Folder($this->VignetteProduit->path, true, 0755);
            if (file_put_contents($dir->path . $name, $photo_decoded) === false) {
                return false;
            }

            // On retourne le nom à sauvegarder
            return $name;
        }

        return false;
    }

    /**
     * On va afficher la version à jour de la galerie pour l'admin
     */
    private function _updatedList($id) {
        $vignette = $this->VignetteProduit->getVignetteProduit($id);
        $this->set(compact("vignette"));
        $this->render("/Elements/Modules/Admin/admin_vignette_produits");
        
    }

}
