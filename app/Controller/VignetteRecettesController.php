<?php

class VignetteRecettesController extends AppController {

    /**
     * AJAX
     * Ajout d'une nouvelle image dans la galerie
     * @param string $data["VignetteRecetteAdd"]["filename"] : l'image en question
     * @param int $data["VignetteRecetteAdd"]["module_gallery_id"] : ID du module galerie
     * @param string $data["VignetteRecetteAdd"]["caption"] : légende de l'image
     */
    public function admin_add() {
        $this->layout = false;
        $this->autoRender = false;
        // Données à sauvegarder
        $data = $this->request->data;
        // Gestion de l'image
        $filename = $this->_uploadThumbnail($data["VignetteRecetteAdd"]["filename"]);
        // On sauvegarde la nouvelle image
        if ($filename !== false) {
            $this->VignetteRecette->create();
            $saveData = array(
                "recette_id" => $data["VignetteRecetteAdd"]["recette_id"],
                "filename" => $filename,
//                "caption" => $data["VignetteProduitAdd"]["caption"],
//                "rang" => 1
            );
            $this->VignetteRecette->save($saveData);

            // Puis on met à jour la galerie entière
//            $this->VignetteProduit->orderAfterCreate($data["VignetteProduitAdd"]["produit_id"], $this->VignetteProduit->id);
        }

        // On renvoie à la vue la liste à jour
        $this->_updatedList($data["VignetteRecetteAdd"]["recette_id"]);
    }

    public function admin_edit() {

        $this->layout = false;
        $this->autoRender = false;

        // Données à sauvegarder
        $data = $this->request->data;
        // Gestion de l'image
        $filename = $this->_uploadThumbnail($data["VignetteRecette"]["filename"]);

//        $saveData = array(
//            "caption" => $data["VignetteProduit"]["caption"],            
//        );

        if ($filename !== false) {
            $saveData["filename"] = $filename;

            $this->VignetteRecette->id = $data["VignetteRecette"]["id"];
            $this->VignetteRecette->save($saveData);
        }

        // On renvoie à la vue la liste à jour
        $this->_updatedList($this->VignetteRecette->field("recette_id", array("id" => $data["VignetteRecette"]["id"])));
    }

    /**
     * AJAX
     * Intervertion de deux images d'une galerie
     * @param int $data["id"] : ID de l'image à bouger
     * @param int $data["module_gallery_id"] : ID du module galerie où se trouve l'image
     * @param string $data["direction"] : dans quelle direction veut-on bouger l'image (up/down) ?
     */
    public function admin_move() {

        $this->layout = false;
        $this->autoRender = false;

        // Données de suppression
        $data = $this->request->data;
        // Quelques infos de l'image
        $gallery = $this->VignetteRecette->find("first", array(
            "conditions" => array(
                "VignetteRecette.id" => $data["id"]
            ),
            "fields" => array(
                "VignetteRecette.id", "VignetteRecette.recette_id", "VignetteRecette.filename", "VignetteRecette.rang"
            )
        ));

        if ($this->VignetteRecette->exists($data["id"])) {
            // L'image en question
            $rang = $this->VignetteRecette->field("rang", array(
                "VignetteRecette.id" => $data["id"]
            ));

            // On bouge d'abord l'image adjacente dont on va prendre la place
            $this->VignetteRecette->updateAll(array(
                "VignetteRecette.rang" => $rang
                    ), array(
                "VignetteRecette.recette_id" => $gallery["VignetteRecette"]["recette_id"],
                "VignetteRecette.rang" => ($data["direction"] === "up") ? $rang - 1 : $rang + 1
            ));

            // Puis on bouge l'image souhaitée
            $this->VignetteRecette->id = $data["id"];
            $this->VignetteRecette->save(array(
                "rang" => ($data["direction"] === "up") ? $rang - 1 : $rang + 1
            ));
        }

        // On renvoie à la vue la liste à jour
        $this->_updatedList($gallery["VignetteRecette"]["recette_id"]);
    }

    /**
     * AJAX
     * Suppression d'une image de la galerie
     * @param int $data["id"] : ID de l'image à supprimer
     * @param int $data["module_gallery_id"]  : ID de la galerie où se trouve l'image
     */
    public function admin_delete() {

        $this->layout = false;
        $this->autoRender = false;

        // Données de suppression
        $data = $this->request->data;
        // Quelques infos de l'image
        $gallery = $this->VignetteRecette->find("first", array(
            "conditions" => array(
                "VignetteRecette.id" => $data["id"]
            ),
            "fields" => array(
                "VignetteRecette.id", "VignetteRecette.recette_id", "VignetteRecette.filename"
            )
        ));

        if (!empty($gallery)) {
            // On supprime l'image en BDD...
            if ($this->VignetteRecette->delete($gallery["VignetteRecette"]["id"])) {
                // ...puis physiquement
                @unlink($this->VignetteRecette->path . $gallery["VignetteRecette"]["filename"]);

                // Enfin on change l'ordre des autres images
//                $this->VignetteProduit->orderAfterDelete($data["id"], $gallery["VignetteProduit"]["rang"]);
            }
        }

        // On renvoie la vue
        $this->_updatedList($gallery["VignetteRecette"]["recette_id"]);
    }

    /**
     * AJAX - GET
     * Affichage de la modale d'ajout d'une image
     * @param int $id : ID du module galerie
     */
    public function admin_modal_add($id) {
        $this->layout = "modal";
        $this->request->data["VignetteRecetteAdd"]["recette_id"] = $id;
    }

    /**
     * AJAX - GET
     * Affichage de la modale d'édition d'une image existante
     */
    public function admin_modal_edit() {
        $this->layout = "modal";

        // Informations sur l'image
        $id = $this->request->query["id"];
        $this->request->data = $this->VignetteRecette->findById($id);
        $this->set("gallery_path", $this->VignetteRecette->path);
    }

    /**
     * AJAX
     * Si on a mis une vignette, on l'upload et on l'ajoute aux données à sauvegarder
     * @param string $filename
     * @param string $imgType
     * @return mixed
     */
    private function _uploadThumbnail($filename, $imgType = null) {

        // Y a-t-il une vignette à uploader ?
        if (!is_array($filename) && !empty($filename)) {
            // On la renomme pour qu'elle soit bien formattée et unique
            preg_match('~image/(.*?);base64~', $filename, $imgType);
            $name = uniqid() . '.' . $imgType[1];

            // On récupère la photo et on l'upload
            App::uses('Folder', 'Utility');
            $photo_decoded = base64_decode(substr($filename, strpos($filename, "base64,") + 7));
            $dir = new Folder($this->VignetteRecette->path, true, 0755);
            if (file_put_contents($dir->path . $name, $photo_decoded) === false) {
                return false;
            }

            // On retourne le nom à sauvegarder
            return $name;
        }

        return false;
    }

    /**
     * On va afficher la version à jour de la galerie pour l'admin
     */
    private function _updatedList($id) {
        $vignette = $this->VignetteRecette->getVignetteRecette($id);
        $this->set(compact("vignette"));
        $this->render("/Elements/Modules/Admin/admin_vignette_recettes");
        
    }

}
