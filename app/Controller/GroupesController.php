<?php
App::uses('AppController', 'Controller');

class GroupesController extends AppController {
    
    /**
     * Listing des groupes actuellement existantes
     */
    public function admin_index() {
        
        $title_for_layout = "Gestion des groupes";
        
        // Comment fait-on pour obtenir un ordre au niveau parent, et un autre ordre dans les enfants ?
        // Faute d'avoir trouvé pour l'instant, la requête se fera en deux fois
        $module_groupes = $this->Groupe->ModuleGroupe->find('all', array(
            'recursive' => -1,
            'order' => array('ModuleGroupe.id asc')
        ));
        foreach ($module_groupes as $k => $module_groupe) {
            $groupes = $this->Groupe->find('all', array(
                'recursive' => -1,
                'order' => array('Groupe.rang asc'),
                'conditions' => array('Groupe.module_groupe_id' => $module_groupe['ModuleGroupe']['id']),
            ));
            foreach ($groupes as $groupe) {
                $module_groupes[$k]['Groupe'][] = $groupe['Groupe'];
            }
        }
        
        $this->set(compact("title_for_layout", 'module_groupes'));
    }
    
    /**
     * Ajout d'un groupe
     */
    public function admin_add() {
        
        // Besoin de stocker le groupe dans un module
        $module_groupe = $this->Groupe->ModuleGroupe->find('first', array(
            'recursive' => -1,
        ));
        if (empty($module_groupe)) {
            $this->Session->setFlash("Il est nécessaire de créer un module de groupes avant d'en ajouter.", "Alerts/Error");
            $this->redirect(array('action' => 'index'));
        }
        
        $title_for_layout = "Ajout d'un groupe";
        
        if($this->request->is('post')) {
            if($this->Groupe->save($this->request->data)) {
                // Enregistrement du rang
                // Note : ne peut pas être dans un afterFind, avec le saveField cela ferait double emploi
                $this->Groupe->saveField('rang', $this->Groupe->id);
                
                $this->Session->setFlash("Le nouveau groupe a bien été ajouté.", "Alerts/Success");
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash("Erreur lors de l'enregistrement du groupe.", "Alerts/Error");
            }
        }
        
        $liste_modules = $this->Groupe->ModuleGroupe->find('list', array(
            'fields' => array('ModuleGroupe.id', 'ModuleGroupe.title'),
        ));
        
        $this->set(compact("title_for_layout", 'liste_modules'));
    }
    
    /**
     * Édition d'un groupe
     * @param int $groupe_id ID de la personne à éditer
     */
    public function admin_edit($groupe_id) {
        
        $title_for_layout = "Modification d'un groupe";
        
        // Confirmer l'existence du groupe à éditer
        $this->Groupe->id = $groupe_id;
        if (!$this->Groupe->exists()) {
            $this->redirect(array('action' => 'index'));
        }
        
        if($this->request->is('post') || $this->request->is('put')) {
            if($this->Groupe->save($this->request->data)) {
                $this->Session->setFlash("Le groupe a bien été modifié.", "Alerts/Success");
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash("Erreur lors de la mise à jour du groupe.", "Alerts/Error");
            }
        } else {
            $this->request->data = $this->Groupe->find('first', array(
                'conditions' => array('Groupe.id' => $groupe_id),
                'contain' => array('ModuleGroupe'),
            ));
        }
        
        $liste_modules = $this->Groupe->ModuleGroupe->find('list', array(
            'fields' => array('ModuleGroupe.id', 'ModuleGroupe.title'),
        ));
        
        $this->set(compact("title_for_layout", 'liste_modules'));
    }
    
    
    /**
     * Suppression d'un groupe
     * @param type $groupe_id ID de la personne à supprimer
     */
    public function admin_delete($groupe_id) {
        
        $this->layout = false;
        $this->autoRender = false;
        
        if ($this->Groupe->delete($groupe_id)) {
            $this->Session->setFlash("Ce groupe a été supprimé.", "Alerts/Success");
        } else {
            $this->Session->setFlash("Ce groupe n'a pas pu être supprimé ou n'existe pas.", "Alerts/Error");
        }
        $this->redirect(array('action' => 'index'));
    }
    
    /**
     * Modifier l'ordre des groupes
     * @param int $groupe_id ID du fichier à déplacer
     * @param string $direction "up" ou "down" pour monter/baisser
     */
    public function admin_move($groupe_id, $direction = "up") {
        
        $this->autoLayout = false;
        
        if($this->request->is('ajax')) {
            // Vérification
            $this->Groupe->id = $groupe_id;
            if (!$this->Groupe->exists()) {
               throw new NotFoundException("Le fichier n'existe pas.");
            }
            
            $module_groupe_id = $this->Groupe->field('module_groupe_id');
            $rang = $this->Groupe->field('rang');

            // On bouge le fichier
            $voisins = $this->Groupe->find('neighbors', array(
                'field' => 'rang',
                'value' => $rang,
                'conditions' => array('Groupe.module_groupe_id' => $module_groupe_id),
                'order' => array('rang desc'),
            ));
            
            // Inversion des places entre le prev/next et l'élément choisi
            if ($direction === 'up' && !empty($voisins['prev'])) {
                $this->Groupe->saveField('rang', $voisins['prev']['Groupe']['rang']);
                $this->Groupe->id = $voisins['prev']['Groupe']['id'];
                $this->Groupe->saveField('rang', $rang);
            } else if ($direction === 'down' && !empty($voisins['next'])) {
                $this->Groupe->saveField('rang', $voisins['next']['Groupe']['rang']);
                $this->Groupe->id = $voisins['next']['Groupe']['id'];
                $this->Groupe->saveField('rang', $rang);
            }
            
            $groupes = $this->Groupe->find('all', array(
                'recursive' => -1,
                'conditions' => array('Groupe.module_groupe_id' => $module_groupe_id),
                'order' => array('Groupe.rang asc'),
            ));
            $this->set(compact('groupes'));
        }
    }
}