<?php
App::uses('AppController', 'Controller');

class PartnersController extends AppController {



    /**
     * ADMIN
     * Liste des partenaires à afficher en footer du frontoffice
     */
    public function admin_index() {

        $d['title_for_layout'] = "Gestion des partenaires";

        $d['partners'] = $this->Partner->find('all');

        $this->set($d);

    }

    /**
     * ADMIN
     * Création d'un nouveau partenaire, qu'on ajoutera au début de la liste
     */
    public function admin_add() {

        $d['title_for_layout'] = "Gestion des partenaires";
        if ($this->request->is('post')) {
//            debug($this->request->data);exit;
            if ($this->Partner->save($this->request->data)) {
                $this->Session->setFlash('Votre partenaire a bien été ajouté', 'Alerts/Success');
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash('Erreur lors de la création de votre partenaire.', 'Alerts/Error');
            }
        }

        $this->set($d);

    }

    /**
     * ADMIN
     * Modification d'un partenaire
     * @param int $id
     */
    public function admin_edit($id = null) {

        $d['title_for_layout'] = "Gestion des partenaires";

        if (!$this->Partner->exists($id)) {
            $this->Session->setFlash('Le partenaire que vous avez essayé d\'éditer n\'existe pas ou plus.', 'Alerts/Error');
            $this->redirect(array(
                'action' => 'index'
            ));
        } else if ($this->request->is('put')) {
            if ($this->Partner->save($this->request->data)) {
                $this->Session->setFlash('Votre partenaire a bien été modifié', 'Alerts/Success');
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash('Erreur lors de la modification de votre partenaire.', 'Alerts/Error');
            }
        } else {
            $this->Partner->id = $id;
            $this->request->data = $this->Partner->read();
        }

        $this->set($d);

    }

    /**
     * ADMIN
     * Modification de l'ordre de deux partenaires
     * @param int $id
     * @param string $direction
     */
    public function admin_move($id, $direction, $delta = 1) {

        // Vérification
        $this->Partner->id = $id;
        if (!$this->Partner->exists()) {
            throw new NotFoundException('Le partenaire n\'existe pas ou plus');
        }

        // On bouge la page et tous ses enfants
        $move = false;
        $rang = $this->Partner->field('rang');
        switch($direction) {
            case 'up':
            $move = $this->Partner->moveUp($id, $rang, $delta);
            break;
            case 'down':
            $move = $this->Partner->moveDown($id, $rang, $delta);
            break;
        }

        if($move === true) {
            $this->Session->setFlash("Le partenaire a bien été déplacé.", "Alerts/Success");
        } else {
            $this->Session->setFlash("Le partenaire n'a pu être déplacé.", "Alerts/Error");
        }

        $this->redirect(array(
            'action' => 'index'
        ));

    }

    /**
     * ADMIN
     * Suppression d'un partenaire
     * @param int $id
     */
    public function admin_delete($id) {

        $this->Partner->id = $id;

        if (!$this->Partner->exists()) {
            $this->Session->setFlash('Ce partenaire n\'existe pas ou plus.', 'Alerts/Error');
        } else {
            $rang = $this->Partner->field('rang');
            if ($this->Partner->delete($id)) {
                // Avant de terminer, on déplace les partenaires suivants vers le haut
                $this->Partner->updateAll(array(
                    'Partner.rang' => 'rang - 1'
                ), array(
                    'Partner.rang >' => $rang
                ));

                $this->Session->setFlash('Le partenaire a bien été supprimé.', 'Alerts/Success');
            } else {
                $this->Session->setFlash('Erreur lors de la suppression du partenaire.', 'Alerts/Error');
            }
        }

        $this->redirect(array(
            'action' => 'index'
        ));

    }

}
