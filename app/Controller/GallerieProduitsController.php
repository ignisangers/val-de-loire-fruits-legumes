<?php

class GallerieProduitsController extends AppController {

    /**
     * AJAX
     * Ajout d'une nouvelle image dans la galerie
     * @param string $data["GallerieProduitAdd"]["filename"] : l'image en question
     * @param int $data["GallerieProduitAdd"]["module_gallery_id"] : ID du module galerie
     * @param string $data["GallerieProduitAdd"]["caption"] : légende de l'image
     */
    public function admin_add() {
        $this->layout = false;
        $this->autoRender = false;
        // Données à sauvegarder
        $data = $this->request->data;
        // Gestion de l'image
        $filename = $this->_uploadThumbnail($data["GallerieProduitAdd"]["filename"]);
        // On sauvegarde la nouvelle image
        if ($filename !== false) {
            $this->GallerieProduit->create();
            $saveData = array(
                "produit_id" => $data["GallerieProduitAdd"]["produit_id"],
                "filename" => $filename,
//                "caption" => $data["GallerieProduitAdd"]["caption"],
                "rang" => 1
            );
            $this->GallerieProduit->save($saveData);

            // Puis on met à jour la galerie entière
            $this->GallerieProduit->orderAfterCreate($data["GallerieProduitAdd"]["produit_id"], $this->GallerieProduit->id);
        }
        // On renvoie à la vue la liste à jour
        $this->_updatedList($data["GallerieProduitAdd"]["produit_id"]);
    }

    public function admin_edit() {

        $this->layout = false;
        $this->autoRender = false;

        // Données à sauvegarder
        $data = $this->request->data;
        // Gestion de l'image
        $filename = $this->_uploadThumbnail($data["GallerieProduit"]["filename"]);

//        $saveData = array(
//            "caption" => $data["GallerieProduit"]["caption"],            
//        );

        if ($filename !== false) {
            $saveData["filename"] = $filename;

            $this->GallerieProduit->id = $data["GallerieProduit"]["id"];
            $this->GallerieProduit->save($saveData);
        }


        // On renvoie à la vue la liste à jour
        $this->_updatedList($this->GallerieProduit->field("produit_id", array("id" => $data["GallerieProduit"]["id"])));
    }

    /**
     * AJAX
     * Intervertion de deux images d'une galerie
     * @param int $data["id"] : ID de l'image à bouger
     * @param int $data["module_gallery_id"] : ID du module galerie où se trouve l'image
     * @param string $data["direction"] : dans quelle direction veut-on bouger l'image (up/down) ?
     */
    public function admin_move() {

        $this->layout = false;
        $this->autoRender = false;

        // Données de suppression
        $data = $this->request->data;
        // Quelques infos de l'image
        $gallery = $this->GallerieProduit->find("first", array(
            "conditions" => array(
                "GallerieProduit.id" => $data["id"]
            ),
            "fields" => array(
                "GallerieProduit.id", "GallerieProduit.produit_id", "GallerieProduit.filename", "GallerieProduit.rang"
            )
        ));

        if ($this->GallerieProduit->exists($data["id"])) {
            // L'image en question
            $rang = $this->GallerieProduit->field("rang", array(
                "GallerieProduit.id" => $data["id"]
            ));

            // On bouge d'abord l'image adjacente dont on va prendre la place
            $this->GallerieProduit->updateAll(array(
                "GallerieProduit.rang" => $rang
                    ), array(
                "GallerieProduit.produit_id" => $gallery["GallerieProduit"]["produit_id"],
                "GallerieProduit.rang" => ($data["direction"] === "up") ? $rang - 1 : $rang + 1
            ));

            // Puis on bouge l'image souhaitée
            $this->GallerieProduit->id = $data["id"];
            $this->GallerieProduit->save(array(
                "rang" => ($data["direction"] === "up") ? $rang - 1 : $rang + 1
            ));
        }

        // On renvoie à la vue la liste à jour
        $this->_updatedList($gallery["GallerieProduit"]["produit_id"]);
    }

    /**
     * AJAX
     * Suppression d'une image de la galerie
     * @param int $data["id"] : ID de l'image à supprimer
     * @param int $data["module_gallery_id"]  : ID de la galerie où se trouve l'image
     */
    public function admin_delete() {

        $this->layout = false;
        $this->autoRender = false;

        // Données de suppression
        $data = $this->request->data;
        // Quelques infos de l'image
        $gallery = $this->GallerieProduit->find("first", array(
            "conditions" => array(
                "GallerieProduit.id" => $data["id"]
            ),
            "fields" => array(
                "GallerieProduit.id", "GallerieProduit.produit_id", "GallerieProduit.filename", "GallerieProduit.rang"
            )
        ));

        if (!empty($gallery)) {
            // On supprime l'image en BDD...
            if ($this->GallerieProduit->delete($gallery["GallerieProduit"]["id"])) {
                // ...puis physiquement
                @unlink($this->GallerieProduit->path . $gallery["GallerieProduit"]["filename"]);

                // Enfin on change l'ordre des autres images
                $this->GallerieProduit->orderAfterDelete($data["id"], $gallery["GallerieProduit"]["rang"]);
            }
        }

        // On renvoie la vue
        $this->_updatedList($gallery["GallerieProduit"]["produit_id"]);
    }

    /**
     * AJAX - GET
     * Affichage de la modale d'ajout d'une image
     * @param int $id : ID du module galerie
     */
    public function admin_modal_add($id) {
        $this->layout = "modal";
        $this->request->data["GallerieProduitAdd"]["produit_id"] = $id;
    }

    /**
     * AJAX - GET
     * Affichage de la modale d'édition d'une image existante
     */
    public function admin_modal_edit() {
        $this->layout = "modal";
        
        $this->initJsVars();
        // Informations sur l'image
        $id = $this->request->query["id"];
        $this->request->data = $this->GallerieProduit->findById($id);
        $this->set("gallery_path", $this->GallerieProduit->path);
    }

    /**
     * AJAX
     * Si on a mis une vignette, on l'upload et on l'ajoute aux données à sauvegarder
     * @param string $filename
     * @param string $imgType
     * @return mixed
     */
    private function _uploadThumbnail($filename, $imgType = null) {

        // Y a-t-il une vignette à uploader ?
        if (!is_array($filename) && !empty($filename)) {
            // On la renomme pour qu'elle soit bien formattée et unique
            preg_match('~image/(.*?);base64~', $filename, $imgType);
            $name = uniqid() . '.' . $imgType[1];

            // On récupère la photo et on l'upload
            App::uses('Folder', 'Utility');
            $photo_decoded = base64_decode(substr($filename, strpos($filename, "base64,") + 7));
            $dir = new Folder($this->GallerieProduit->path, true, 0755);
            if (file_put_contents($dir->path . $name, $photo_decoded) === false) {
                return false;
            }

            // On retourne le nom à sauvegarder
            return $name;
        }

        return false;
    }

    /**
     * On va afficher la version à jour de la galerie pour l'admin
     */
    private function _updatedList($id) {
        $gallery = $this->GallerieProduit->getGallerieProduit($id);
        $this->set(compact("gallery"));
        $this->render("/Elements/Modules/Admin/admin_gallerie_produits");
    }
    
    /**
     * Setting Javascript Global Vars
     * 
     * @return void
     */
    private function initJsVars() {
        $this->setJsVar('cropConfig', array(
            'default' => array(
//                'maxNumImagesAllowed' => 1,
//                'maxFileSizeAllowed' => 10, //MBs
//                'qualityJpeg' => 0.85, //from 0.0 to 1 (normal default is 0.92)
//                'allowResize' => true,
//                'selMinSize' => array(50, 50), //pxs
//                'maxWidthPreview' => 530, //pxs
//                'maxHeightPreview' => false,
                'aspectRatio' => 1 / 1, //1 square (16/9, 4/3, etc)
//                'setSelect' => [0, 0, 100, 100], //initial cords area [x, y ,x2, y2]
//                'bgOpacity' => 0.5,
//                'bgColor' => 'black',
                'cropImageWidth' => 800, //if ommited and added cropImageHeight width is calculated according to aspectRatio
            ),
        ));
    }

    
    
}
