<?php
App::uses('AppController', 'Controller');

class ModuleRecettesController extends AppController {
    
    /**
     * Ajout d'un module dans une zone d'une page
     * @param int $page_id
     * @param int $zone
     */
    public function admin_add($page_id, $zone) {
        
        $title_for_layout = "Gestion des pages";
        
        if($this->request->is('post')) {
            if($this->ModuleRecette->saveAssociated($this->request->data)) {
                // On va décaler les rangs des autres modules de la zone
                $this->{$this->modelClass}->Content->updateOrder($page_id, $zone, $this->{$this->modelClass}->Content->id);
                // On redirige
                $this->Session->setFlash("Votre module recettes a bien été ajouté.", "Alerts/Success");
                $this->redirect(array(
                    'controller' => 'pages',
                    'action' => 'edit_modules',
                    'language' => Configure::read('Config.language'),
                    $page_id
                ));
            } else {
                $this->Session->setFlash("Erreur lors de l'enregistrement de votre module recettes.", "Alerts/Error");
            }
        }
        
        $page = $this->ModuleRecette->Content->Page->find('first', array(
            'conditions' => array(
                'Page.id' => $page_id,
                'Page.active' => 1
            ),
            'recursive' => -1,
            'fields' => array('Page.name')
        ));
        
        $this->set(compact("title_for_layout"));
        $this->set('page', $page);
        $this->set('page_id', $page_id);
        $this->set('zone', $zone);
        
    }
    
    /**
     * Édition d'un module dans une zone d'une page
     * @param int $id
     */
    public function admin_edit($id, $page_id) {
        
        $title_for_layout = "Gestion des pages";
        
        $this->ModuleRecette->id = $id;
        
        if($this->request->is('post') || $this->request->is('put')) {
            
            if($this->ModuleRecette->save($this->request->data)) {
                $this->Session->setFlash("Votre module recettes a bien été modifié.", "Alerts/Success");
                $this->redirect(array(
                    'controller' => 'pages',
                    'action' => 'edit_modules',
                    'language' => Configure::read('Config.language'),
                    $page_id
                ));
            } else {
                $this->Session->setFlash("Erreur lors de la mise à jour de votre module recettes.", "Alerts/Error");
            }
        
        } else {
            $this->request->data = $this->ModuleRecette->read();
        }
        
        $page = $this->ModuleRecette->Content->Page->find('first', array(
            'conditions' => array(
                'Page.id' => $page_id,
                'Page.active' => 1
            ),
            'recursive' => -1,
            'fields' => array('Page.name')
        ));
        
        $this->set(compact('title_for_layout'));
        $this->set('page', $page);
        $this->set('page_id', $page_id);
        
    }
}