<?php
App::uses('AppController', 'Controller');

class EvenementsController extends AppController {
    
    public $components = array('Paginator');
    
    public $paginate = array(
        'limit' => 25,
        'order' => array(
            'Evenement.date_debut' => 'desc',
            'Evenement.created' => 'desc',
            'Evenement.id' => 'desc',
        ),
    );
    
    /**
     * SITE
     * 
     * @param int $id
     */
    public function display($id = null) {
        
        // L'actualité
        $event = $this->Evenement->findById($id);
        if (empty($event)) {
            throw new NotFoundException("Nous n'avons pu trouver cette événement de l'agenda.");
        }
        $this->set(compact("event"));

        /**
         * Construction de la page
         */
        $this->loadModel('Page');
        $this->set($this->Page->construct());

        // On va compléter le fil d'ariane, puisqu'une actualité n'est pas une page "réelle"
        $extBreadcrumb = array(
            array(
                "name" => "Événement du " . date("d/m/Y", strtotime($event['Evenement']['created'])),
                "link" => array(
                    "controller" => "evenements",
                    "action" => "display",
                    $id
                )
            )
        );
        $this->set(compact("extBreadcrumb"));


        // Affichage du template (AppController)
        $this->setTemplate("events-detail");
    }
    
    /**
     * Listing des évènements actuellement existants
     * sous forme de pagination
     */
    public function admin_index() {
        
        $title_for_layout = "Gestion des évènements";
        
        $this->Paginator->settings = $this->paginate;
        $evenements = $this->Paginator->paginate('Evenement');
        $this->set(compact('evenements', 'title_for_layout'));
    }
    
    /**
     * Ajout d'une évènement
     */
    public function admin_add() {
        
        $title_for_layout = "Ajout d'une évènement";
        
        if($this->request->is('post')) {
            if($this->Evenement->save($this->request->data)) {
                $this->Session->setFlash("Votre évènement a bien été ajouté.", "Alerts/Success");
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash("Erreur lors de l'enregistrement de votre évènement.", "Alerts/Error");
                // Ajout des datetime séparés
                $this->request->data['Evenement']['debut_date'] = date('Y-m-d', strtotime($this->request->data['Evenement']['date_debut']));
                $this->request->data['Evenement']['debut_heure'] = date('H:i:s', strtotime($this->request->data['Evenement']['date_debut']));
                if (!empty($this->request->data['Evenement']['date_fin'])) {
                    $this->request->data['Evenement']['fin_date'] = date('Y-m-d', strtotime($this->request->data['Evenement']['date_fin']));
                    $this->request->data['Evenement']['fin_heure'] = date('H:i:s', strtotime($this->request->data['Evenement']['date_fin']));
                } else {
                    $this->request->data['Evenement']['fin_date'] = '';
                    $this->request->data['Evenement']['fin_heure'] = '';
                }
            }
        }
        
        $this->set(compact("title_for_layout"));
    }
    
    /**
     * Édition d'une évènement
     * @param int $evenement_id ID de l'évènement à éditer
     */
    public function admin_edit($evenement_id) {
        
        $title_for_layout = "Modification d'une évènement";
        
        // Confirmer l'existence de l'évènement à éditer
        $this->Evenement->id = $evenement_id;
        if (!$this->Evenement->exists()) {
            $this->redirect(array('action' => 'index'));
        }
        
        if($this->request->is('post') || $this->request->is('put')) {
            if($this->Evenement->save($this->request->data)) {
                $this->Session->setFlash("Votre évènement a bien été modifié.", "Alerts/Success");
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash("Erreur lors de la mise à jour de votre évènement.", "Alerts/Error");
            }
        } else {
            $this->request->data = $this->Evenement->read();
        }
        // Ajout des datetime séparés
        $this->request->data['Evenement']['debut_date'] = date('Y-m-d', strtotime($this->request->data['Evenement']['date_debut']));
        $this->request->data['Evenement']['debut_heure'] = date('H:i:s', strtotime($this->request->data['Evenement']['date_debut']));
        if (!empty($this->request->data['Evenement']['date_fin'])) {
            $this->request->data['Evenement']['fin_date'] = date('Y-m-d', strtotime($this->request->data['Evenement']['date_fin']));
            $this->request->data['Evenement']['fin_heure'] = date('H:i:s', strtotime($this->request->data['Evenement']['date_fin']));
        } else {
            $this->request->data['Evenement']['fin_date'] = '';
            $this->request->data['Evenement']['fin_heure'] = '';
        }
        
        $this->set(compact('title_for_layout'));
    }
    
    
    /**
     * Suppression d'une évènement
     * @param type $evenement_id ID de l'évènement à supprimer
     */
    public function admin_delete($evenement_id) {
        
        $this->layout = false;
        $this->autoRender = false;
        
        if ($this->Evenement->delete($evenement_id)) {
            $this->Session->setFlash("Cette évènement a été supprimé.", "Alerts/Success");
        } else {
            $this->Session->setFlash("Cette évènement n'a pas pu être supprimé ou n'existe pas.", "Alerts/Error");
        }
        $this->redirect(array('action' => 'index'));
    }
}
