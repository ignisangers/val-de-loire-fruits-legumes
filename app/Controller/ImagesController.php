<?php
App::uses('AppController', 'Controller');
/**
 * Description of ImagesController
 *
 * @author carlos-ignis
 */
class ImagesController extends AppController {
    
    /**
     * Fonction d'upload AJAX d'une image avec l'éditeur Redactor
     * @return json
     */
    public function admin_upload_image_redactor() {

        $this->layout = false;
        $this->autoRender = false;

        // files storage folder
        $dir = IMAGES . "redactor/";
        $nfic = $_FILES['file'];
        
        $nfic['type'] = strtolower($nfic['type']);

        $tmp_name = $nfic["tmp_name"];
        $name = $nfic["name"];

        if ($nfic['type'] == 'image/png' || $nfic['type'] == 'image/jpg' || $nfic['type'] == 'image/gif' || $nfic['type'] == 'image/jpeg' || $nfic['type'] == 'image/pjpeg') {

            // Le fichier existe déjà, on le renomme
            if (file_exists($dir . $name)) {
                $numfic = 1;
                $position_p = strrpos($name, ".");
                $longueur = strlen($name);
                $name_base = substr($name, 0, $position_p);
                $ext = substr($name, $position_p, $longueur - $position_p);

                $name = $name_base . "_" . $numfic . $ext;

                while (file_exists($dir . $name) && $numfic < 1000) {
                    $numfic++;
                    $name = $name_base . "_" . $numfic . $ext;
                }
            }

            // copying
            move_uploaded_file($tmp_name, $dir . $name);
            
            // displaying file
            $array = array(
                'filelink' => Router::url("/img/redactor/") . $name
            );
        }
        return stripslashes(json_encode($array));
    }

}
