<?php
App::uses('AppController', 'Controller');

class ActualitesController extends AppController {
    
    public $components = array("Paginator");
    public $paginate = array(
        'limit' => 10
    );
    
    public function beforeFilter() {
        parent::beforeFilter();
        
        // Pagination pour l'action index()
        if (isset($this->request->params['page'])) {
            $this->request->params['named']['page'] = $this->request->params['page'];
        }
    }
    
    /**
     * SITE
     * Page affichant la liste paginée des actualités
     */
    public function index() {
        
        // Construction de la page
        $this->loadModel('Page');
        $this->set($this->Page->construct($this->Actualite->pageConditions));
        
        // Liste des actualités
        $this->Paginator->settings = $this->paginate;
        $news = $this->Paginator->paginate('Actualite');
        $this->set(compact("news"));
        
        // Affichage du template (AppController)
        $this->setTemplate("news");
        
    }
    
    /**
     * SITE
     * Page affichant une actualité
     */
    public function display($slug = null) {
        
        // L'actualité
        $news = $this->Actualite->findBySlug($slug);
        if (empty($news)) {
            throw new NotFoundException("Nous n'avons pu trouver cette actualité.");
        }
        $this->set(compact("news"));
        
        /**
         * Construction de la page :
         * - Soit on utilise comme référence la page actualités
         * - Soit on utilise la page accueil (par défaut)
         */
        $this->loadModel('Page');
        $newsPage = $this->Page->getInfo($this->Actualite->pageConditions);
        $newsConditions = (!empty($newsPage)) ? $this->Actualite->pageConditions : array();
        $this->set($this->Page->construct($newsConditions, $news['Actualite']['name']));
        
        // On va compléter le fil d'ariane, puisqu'une actualité n'est pas une page "réelle"
        $extBreadcrumb = array(
            array(
                "name" => $news['Actualite']['name'],
                "link" => array(
                    "controller" => "actualites",
                    "action" => "display",
                    $slug
                )
            )
        );
        $this->set(compact("extBreadcrumb"));
        
        
        // Affichage du template (AppController)
        $this->setTemplate("news-detail");
        
    }
    
    /**
     * Listing des actualités actuellement existantes
     * sous forme de pagination
     */
    public function admin_index() {
        
        $title_for_layout = "Gestion des actualités";
        
        $this->Paginator->settings = $this->paginate;
        $actualites = $this->Paginator->paginate('Actualite');
        $this->set(compact('actualites', 'title_for_layout'));
    }
    
    /**
     * Ajout d'une actualité
     */
    public function admin_add() {
        
        $title_for_layout = "Ajout d'une actualité";
        
        if($this->request->is('post')) {
            $this->request->data['Actualite']['slug'] = strtolower(Inflector::slug($this->request->data['Actualite']['name'], "-"));
            if($this->Actualite->save($this->request->data)) {
                $this->Session->setFlash("Votre actualité a bien été ajoutée.", "Alerts/Success");
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash("Erreur lors de l'enregistrement de votre actualité.", "Alerts/Error");
            }
        }
        
        $this->set(compact("title_for_layout"));
    }
    
    /**
     * Édition d'une actualité
     * @param int $actualite_id ID de l'actualité à éditer
     */
    public function admin_edit($actualite_id) {
        
        $title_for_layout = "Modification d'une actualité";
        
        // Confirmer l'existence de l'actualité à éditer
        $this->Actualite->id = $actualite_id;
        if (!$this->Actualite->exists()) {
            $this->redirect(array('action' => 'index'));
        }
        
        if($this->request->is('post') || $this->request->is('put')) {
            $this->request->data['Actualite']['slug'] = strtolower(Inflector::slug($this->request->data['Actualite']['name'], "-"));
            if($this->Actualite->save($this->request->data)) {
                $this->Session->setFlash("Votre actualité a bien été modifiée.", "Alerts/Success");
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash("Erreur lors de la mise à jour de votre actualité.", "Alerts/Error");
            }
        } else {
            $this->request->data = $this->Actualite->read();
        }
        $this->set(compact('title_for_layout'));
    }
    
    
    /**
     * Suppression d'une actualité
     * @param type $actualite_id ID de l'actualité à supprimer
     */
    public function admin_delete($actualite_id) {
        
        $this->layout = false;
        $this->autoRender = false;
        
        if ($this->Actualite->delete($actualite_id)) {
            $this->Session->setFlash("Cette actualité a été supprimée.", "Alerts/Success");
        } else {
            $this->Session->setFlash("Cette actualité n'a pas pu être supprimée ou n'existe pas.", "Alerts/Error");
        }
        $this->redirect(array('action' => 'index'));
    }
}