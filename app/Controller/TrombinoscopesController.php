<?php
App::uses('AppController', 'Controller');

class TrombinoscopesController extends AppController {
    
    /**
     * Listing des personnes actuellement existantes
     */
    public function admin_index() {
        
        $title_for_layout = "Gestion du trombinoscope";
        
        $trombinoscopes = $this->Trombinoscope->find('all', array(
            'order' => array('Trombinoscope.rang asc'),
        ));
        
        $this->set(compact('trombinoscopes', 'title_for_layout'));
    }
    
    /**
     * Ajout d'une personne
     */
    public function admin_add() {
        
        $title_for_layout = "Ajout d'une personne";
        
        if($this->request->is('post')) {
            if($this->Trombinoscope->save($this->request->data)) {
                // Enregistrement du rang
                // Note : ne peut pas être dans un afterFind, avec le saveField cela ferait double emploi
                $this->Trombinoscope->saveField('rang', $this->Trombinoscope->id);
                
                $this->Session->setFlash("La nouvelle personne a bien été ajoutée.", "Alerts/Success");
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash("Erreur lors de l'enregistrement de la nouvelle personne.", "Alerts/Error");
            }
        }
        
        $this->set(compact("title_for_layout"));
    }
    
    /**
     * Édition d'une personne
     * @param int $personne_id ID de la personne à éditer
     */
    public function admin_edit($personne_id) {
        
        $title_for_layout = "Modification d'une personne";
        
        // Confirmer l'existence de la personne à éditer
        $this->Trombinoscope->id = $personne_id;
        if (!$this->Trombinoscope->exists()) {
            $this->redirect(array('action' => 'index'));
        }
        
        if($this->request->is('post') || $this->request->is('put')) {
            if($this->Trombinoscope->save($this->request->data)) {
                $this->Session->setFlash("La personne a bien été modifiée.", "Alerts/Success");
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash("Erreur lors de la mise à jour de la personne.", "Alerts/Error");
            }
        } else {
            $this->request->data = $this->Trombinoscope->read();
        }
        $this->set(compact('title_for_layout'));
    }
    
    
    /**
     * Suppression d'une personne
     * @param type $personne_id ID de la personne à supprimer
     */
    public function admin_delete($personne_id) {
        
        $this->layout = false;
        $this->autoRender = false;
        
        if ($this->Trombinoscope->delete($personne_id)) {
            $this->Session->setFlash("Cette personne a été supprimée.", "Alerts/Success");
        } else {
            $this->Session->setFlash("Cette personne n'a pas pu être supprimée ou n'existe pas.", "Alerts/Error");
        }
        $this->redirect(array('action' => 'index'));
    }
    
    /**
     * Modifier l'ordre des personnes
     * @param int $personne_id ID de la personne à déplacer
     * @param string $direction "up" ou "down" pour monter/baisser
     */
    public function admin_move($personne_id, $direction = "up") {
        
        $this->autoLayout = false;
        $this->autoRender = false;
        
        if($this->request->is('ajax')) {
            // Vérification
            $this->Trombinoscope->id = $personne_id;
            if (!$this->Trombinoscope->exists()) {
               throw new NotFoundException("La personne n'existe pas.");
            }

            // On bouge la personne
            $voisins = $this->Trombinoscope->find('neighbors', array(
                'field' => 'rang',
                'value' => $personne_id,
            ));
            
            if ($direction === 'up' && !empty($voisins['prev'])) {
                $this->Trombinoscope->saveField('rang', $voisins['prev']['Trombinoscope']['rang']);
                $this->Trombinoscope->id = $voisins['prev']['Trombinoscope']['id'];
                $this->Trombinoscope->saveField('rang', $personne_id);
                return 'up';
            } else if ($direction === 'up' && !empty($voisins['prev'])) {
                $this->Trombinoscope->saveField('rang', $voisins['next']['Trombinoscope']['rang']);
                $this->Trombinoscope->id = $voisins['next']['Trombinoscope']['id'];
                $this->Trombinoscope->saveField('rang', $personne_id);
                return 'down';
            } else {
                return 'none';
            }
        }
    }
}