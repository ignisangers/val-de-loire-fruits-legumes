<?php
App::uses('AppController', 'Controller');

class ModuleGalleriesController extends AppController {
    
    /**
     * Ajout d'un module dans une zone d'une page :
     * On ajoute le module et on va directement à la page de modification
     * où l'on va gérer les images de la galerie
     * 
     * @param int $page_id
     * @param int $zone
     */
    public function admin_add($page_id, $zone) {
        
        // Création dans Content et ModuleGallery
        $data = array(
            'Content' => array(
                'id' => null,
                'page_id' => $page_id,
                'zone' => $zone,
                'model' => 'ModuleGallery',
                'rang' => 1
            ),
            'ModuleGallery' => array(
                'id' => null
            )
        );        
        if ($this->ModuleGallery->saveAssociated($data)) {
            // On va décaler les rangs des autres modules de la zone
            $content_id = $this->{$this->modelClass}->Content->id;
            $module_id = $this->{$this->modelClass}->id;
            $this->{$this->modelClass}->Content->updateOrder($page_id, $zone, $content_id);
            // On redirige
            $this->Session->setFlash("Votre galerie a bien été ajoutée : ajoutez-lui maintenant des images.", "Alerts/Success");
            $redirect = array(
                'controller' => 'module_galleries',
                'action' => 'edit',
                $module_id,
                $page_id
            );
        } else {
            $this->Session->setFlash("Erreur lors de la création de votre galerie.", "Alerts/Error");
            $redirect = array(
                'controller' => 'pages',
                'action' => 'edit_modules',
                $page_id
            );
        }
        
        $this->redirect($redirect);
    }
    
    /**
     * Édition d'un module dans une zone d'une page
     * @param int $id
     * @param int $page_id
     */
    public function admin_edit($id, $page_id) {
        
        $title_for_layout = "Gestion des pages";
        
        $this->ModuleGallery->id = $id;
        
        if($this->request->is('post') || $this->request->is('put')) {
            
            if($this->ModuleGallery->save($this->request->data)) {
                $this->Session->setFlash("Votre galerie a bien été modifiée.", "Alerts/Success");
                $this->redirect(array(
                    'controller' => 'pages',
                    'action' => 'edit_modules',
                    'language' => Configure::read('Config.language'),
                    $page_id
                ));
            } else {
                $this->Session->setFlash("Erreur lors de la mise à jour de votre galerie.", "Alerts/Error");
            }
        
        } else {
            $this->request->data = $this->ModuleGallery->read();
        }
        
        $gallery = $this->ModuleGallery->Gallery->getGallery($id);
        $this->set(compact("gallery"));
        
        $page = $this->ModuleGallery->Content->Page->find('first', array(
            'conditions' => array(
                'Page.id' => $page_id,
                'Page.active' => 1
            ),
            'recursive' => -1,
            'fields' => array('Page.name')
        ));
        
        $this->initJsVars();
        
        $this->set(compact('title_for_layout'));
        $this->set('module_gallery_id', $id);
        $this->set('page', $page);
        $this->set('page_id', $page_id);
        
    }
    
    
    /**
     * Setting Javascript Global Vars
     * 
     * @return void
     */
    private function initJsVars() {
        $this->setJsVar('cropConfig', array(
            'default' => array(
//                'maxNumImagesAllowed' => 1,
//                'maxFileSizeAllowed' => 10, //MBs
//                'qualityJpeg' => 0.85, //from 0.0 to 1 (normal default is 0.92)
//                'allowResize' => true,
//                'selMinSize' => array(50, 50), //pxs
//                'maxWidthPreview' => 530, //pxs
//                'maxHeightPreview' => false,
                'aspectRatio' => 16/9, //1 square (16/9, 4/3, etc)
//                'setSelect' => [0, 0, 100, 100], //initial cords area [x, y ,x2, y2]
//                'bgOpacity' => 0.5,
//                'bgColor' => 'black',
                'cropImageWidth' => 800, //if ommited and added cropImageHeight width is calculated according to aspectRatio
            ),
        ));
    }

}