<?php

App::uses('AppController', 'Controller');

class ProduitsController extends AppController {

    public $components = array("Paginator");
    public $paginate = array(
        'limit' => 10,
        'order' => 'name ASC'
    );

    public function beforeFilter() {
        parent::beforeFilter();

        // Pagination pour l'action index()
        if (isset($this->request->params['page'])) {
            $this->request->params['named']['page'] = $this->request->params['page'];
        }
    }

    /**
     * SITE
     * Page affichant la liste paginée des produits
     */
    public function index() {

        // Construction de la page
        $this->loadModel('Page');
        $this->set($this->Page->construct($this->Produit->pageConditions));

        // Liste des produits
        $this->Paginator->settings = $this->paginate;
        $produit = $this->Paginator->paginate('Produit');
        $this->set(compact("produit"));

        // Affichage du template (AppController)
        $this->setTemplate("produit");
    }

    /**
     * SITE
     * Page affichant un produits
     */
    public function display($slug = null) {

        //produit
        $produit = $this->Produit->find('first', array(
            'recursive' => 2,
            'conditions' => array(
                'Produit.slug' => $slug
            )
        ));
        //produit de saison ou non
        $produitSaison = false;
        if ($produit['Produit']['debut'] > $produit['Produit']['fin']) {
            if (date('m') >= $produit['Produit']['debut'] || date('m') <= $produit['Produit']['fin']) {
                $produitSaison = true;
            }
        } else {
            if (date('m') >= $produit['Produit']['debut'] && date('m') <= $produit['Produit']['fin']) {
                $produitSaison = true;
            }
        }
        //conversion des date de début et de fin 
        $mois = array(
            1 => 'janvier',
            2 => 'fevrier',
            3 => 'mars',
            4 => 'avril',
            5 => 'mai',
            6 => 'juin',
            7 => 'juillet',
            8 => 'aout',
            9 => 'septembre',
            10 => 'octobre',
            11 => 'novembre',
            12 => 'decembre',
        );
        foreach ($mois as $key => $moi) {
            if ($produit['Produit']['debut'] == $key) {
                $produit['Produit']['debut'] = $moi;
            }
            if ($produit['Produit']['fin'] == $key) {
                $produit['Produit']['fin'] = $moi;
            }
        }
        //fruits
        $listeFruits = $this->Produit->find('all', array(
            'recursive' => 2,
            'conditions' => array(
                'Produit.type' => "fruit"
            ),
            'order' => 'name asc'
//            'limit' => 8
        ));
        $fruits = array();
        $fruitCount = 0;
        foreach ($listeFruits as $listeFruit) {
            if ($fruitCount <= 8) {
                if ($listeFruit['Produit']['debut'] > $listeFruit['Produit']['fin']) {
                    if (date('m') >= $listeFruit['Produit']['debut'] || date('m') <= $listeFruit['Produit']['fin']) {
                        $fruits[] = $listeFruit;
                        $fruitCount ++;
                    }
                } else {
                    if (date('m') >= $listeFruit['Produit']['debut'] && date('m') <= $listeFruit['Produit']['fin']) {
                        $fruits[] = $listeFruit;
                        $fruitCount ++;
                    }
                }
            }
        }
        //légumes
        $listeLegumes = $this->Produit->find('all', array(
            'recursive' => 2,
            'conditions' => array(
                'Produit.type' => 'légume'
            ),
            'order' => 'name asc'
//            'limit' => 8
        ));
        $legumes = array();
        $legumeCount = 0;
        foreach ($listeLegumes as $listeLegume) {
            if ($legumeCount <= 8) {
                if ($listeLegume['Produit']['debut'] > $listeLegume['Produit']['fin']) {
                    if (date('m') >= $listeLegume['Produit']['debut'] || date('m') <= $listeLegume['Produit']['fin']) {
                        $legumes[] = $listeLegume;
                        $legumeCount ++;
                    }
                } else {
                    if (date('m') >= $listeLegume['Produit']['debut'] && date('m') <= $listeLegume['Produit']['fin']) {
                        $legumes[] = $listeLegume;
                        $legumeCount ++;
                    }
                }
            }
        }

        //recettes de saison pour le slider
        $this->loadModel('Recette');
        $allRecettes = $this->Recette->find('all', array(
            'conditions' => array(
                "not" => array(
                    "Recette.debut" => null
                )
            )
        ));
        $recettes = array();
        foreach ($allRecettes as $recette) {//produits de saison
            if ($recette['Recette']['debut'] > $recette['Recette']['fin']) {
                if (date('m') >= $recette['Recette']['debut'] || date('m') <= $recette['Recette']['fin']) {
                    $recettes[] = $recette;
                }
            } else {
                if (date('m') >= $recette['Recette']['debut'] && date('m') <= $recette['Recette']['fin']) {
                    $recettes[] = $recette;
                }
            }
        }

        //récupération de trois recettes composé du produit affiché
        $this->loadModel('RecetteProduit');
        $recettesProduit = $this->RecetteProduit->find('all', array(
            'conditions' => array(
                'RecetteProduit.produit_id' => $produit['Produit']['id']
            ),
            'limit' => 3
        ));
        $recetteIds = array();
        foreach ($recettesProduit as $recettesProduit) {
            $recetteIds[$recettesProduit['RecetteProduit']['recette_id']] = $recettesProduit['RecetteProduit']['recette_id'];
        }
//        debug($recettesProduit);
//        debug($recetteIds);exit;
        $recettesListe = $this->Recette->find('all', array(
            'conditions' => array(
                "Recette.id" => $recetteIds
            )
        ));
        if (empty($produit)) {
            throw new NotFoundException("Nous n'avons pu trouver ce produit.");
        }
        $this->set(compact("produit", "produitSaison", "fruits", "legumes", "recettes", "recettesListe"));

        /**
         * Construction de la page :
         * - Soit on utilise comme référence la page produits
         * - Soit on utilise la page accueil (par défaut)
         */
        $this->loadModel('Page');
//        $produitPage = $this->Page->getInfo($this->Produit->pageConditions);
//        $produitConditions = (!empty($produitPage)) ? $this->Produit->pageConditions : array();
//        $this->set($this->Page->construct($produitConditions, $produit['Produit']['name']));
        $this->set($this->Page->construct(array('Page.id' => 5), $produit['Produit']['name']));
        // On va compléter le fil d'ariane, puisqu'un produit n'est pas une page "réelle"
        $extBreadcrumb = array(
            array(
                "name" => $produit['Produit']['name'],
                "link" => array(
                    "controller" => "produits",
                    "action" => "display",
                    $slug
                )
            )
        );

        $this->set(compact("extBreadcrumb", "produit"));
        // Affichage du template (AppController)
        $this->setTemplate("produit");
    }

    /**
     * Génération du pdf produit
     * 
     * @param int $id id du produit
     */
    public function produitPdf($id) {

        $this->layout = false;

        $produit = $this->Produit->find('first', array(
            'recursive' => 2,
            'conditions' => array('Produit.id' => $id)
        ));

        if (empty($produit)) {
            $this->Session->setFlash("Erreur : produit introuvable", 'flashError', array());
            $this->redirect($this->referer());
        }

        $this->set("produit", $produit);

        // Configuration PDF
        Configure::write('CakePdf', array(
            'engine' => 'CakePdf.DomPdf',
            'orientation' => 'portrait',
            'pageSize' => 'A4',
            'filename' => 'produit-' . $produit['Produit']['name'] . '.pdf',
            'download' => false
        ));
    }

    /**
     * Listing des produits actuellement existantes
     * sous forme de pagination
     */
    public function admin_index() {

        $title_for_layout = "Gestion des produits";

        $this->Paginator->settings = $this->paginate;
        $produits = $this->Paginator->paginate('Produit');
        $this->set(compact('produits', 'title_for_layout'));
    }

    /**
     * Ajout d'un produit
     */
    public function admin_add() {
        $title_for_layout = "Ajout d'un produit";

        if ($this->request->is('post')) {
            $this->Produit->id = $this->request->data['Produit']['id'];
            $this->request->data['Produit']['slug'] = strtolower(Inflector::slug($this->request->data['Produit']['name'], "-"));
            if ($this->Produit->save($this->request->data)) {
                $this->Session->setFlash("Votre produits a bien été ajoutée.", "Alerts/Success");
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash("Erreur lors de l'enregistrement de votre produit.", "Alerts/Error");
            }
        } else {
            //on crée immédiatement le produit pour pouvoir le lier à une image 
            $product = $this->Produit->save(array());
            $produit_id = $product['Produit']['id'];
        }

        // ajout des images déjà uploadée
        $this->loadModel('GallerieProduit');
        $gallery = $this->GallerieProduit->getGallerieProduit($produit_id);
        $this->set(compact("gallery"));

        // ajout de la vignette déjà uploadée
        $this->loadModel('VignetteProduit');
        $vignette = $this->VignetteProduit->getVignetteProduit($produit_id);
        $this->set(compact("vignette"));

        $this->initJsVars();
        $this->set(compact("title_for_layout", "produit_id"));
    }

    /**
     * Édition d'un produits
     * @param int $produit_id ID de produit à éditer
     */
    public function admin_edit($produit_id) {

        $title_for_layout = "Modification d'un produit";
        // Confirmer l'existence du produit à éditer
        $this->Produit->id = $produit_id;
        if (!$this->Produit->exists()) {
            $this->redirect(array('action' => 'index'));
        }
//        debug($this->request->data);exit;
        //recuperation du produit
        $produit = $this->Produit->findById($produit_id);

        if ($this->request->is('post') || $this->request->is('put')) {
            $this->request->data['Produit']['slug'] = strtolower(Inflector::slug($this->request->data['Produit']['name'], "-"));
            if ($this->Produit->save($this->request->data)) {
                $this->Session->setFlash("Votre produit a bien été modifié.", "Alerts/Success");
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash("Erreur lors de la mise à jour de votre produit.", "Alerts/Error");
            }
        } else {
            $this->request->data = $this->Produit->read();
        }
        // ajout des images déjà uploadée
        $this->loadModel('GallerieProduit');
        $gallery = $this->GallerieProduit->getGallerieProduit($produit_id);
        $this->set(compact("gallery"));

        // ajout de la vignette déjà uploadée
        $this->loadModel('VignetteProduit');
        $vignette = $this->VignetteProduit->getVignetteProduit($produit_id);
        $this->set(compact("vignette"));

        $this->initJsVars();
        $this->set(compact('title_for_layout', 'produit_id', 'produit'));
    }

    /**
     * Suppression d'un produit
     * @param type $produit_id ID du produits à supprimer
     */
    public function admin_delete($produit_id) {

        $this->layout = false;
        $this->autoRender = false;

        $produit = $this->Produit->find('first', array(
            'recursive' => 2,
            'conditions' => array('Produit.id' => $produit_id)
        ));

        if ($this->Produit->delete($produit_id, true)) {
            //suppression physique des images associées au produit
            if (!empty($produit['VignetteProduit'])) {
                foreach ($produit['VignetteProduit'] as $vignette) {
                    @unlink(WWW_ROOT . 'files' . DS . 'produits/vignette' . DS . $vignette["filename"]);
                }
            }
            if (!empty($produit['GallerieProduit'])) {
                foreach ($produit['GallerieProduit'] as $gallerie) {
                    @unlink(WWW_ROOT . 'files' . DS . 'produits/gallerie' . DS . $gallerie["filename"]);
                }
            }

            $this->Session->setFlash("Ce produit a été supprimé.", "Alerts/Success");
        } else {
            $this->Session->setFlash("Ce produit n'a pas pu être supprimée ou n'existe pas.", "Alerts/Error");
        }
        $this->redirect(array('action' => 'index'));
    }

    /**
     * Setting Javascript Global Vars
     * 
     * @return void
     */
    private function initJsVars() {
        $this->setJsVar('cropConfig', array(
            'default' => array(
//                'maxNumImagesAllowed' => 1,
//                'maxFileSizeAllowed' => 10, //MBs
//                'qualityJpeg' => 0.85, //from 0.0 to 1 (normal default is 0.92)
//                'allowResize' => true,
//                'selMinSize' => array(50, 50), //pxs
//                'maxWidthPreview' => 530, //pxs
//                'maxHeightPreview' => false,
                'aspectRatio' => 4 / 3, //1 square (16/9, 4/3, etc)
//                'setSelect' => [0, 0, 100, 100], //initial cords area [x, y ,x2, y2]
//                'bgOpacity' => 0.5,
//                'bgColor' => 'black',
                'cropImageWidth' => 1300, //if ommited and added cropImageHeight width is calculated according to aspectRatio
            ),
            'vignette' => array(
//                'maxNumImagesAllowed' => 1,
//                'maxFileSizeAllowed' => 10, //MBs
//                'qualityJpeg' => 0.85, //from 0.0 to 1 (normal default is 0.92)
//                'allowResize' => true,
//                'selMinSize' => array(50, 50), //pxs
//                'maxWidthPreview' => 530, //pxs
//                'maxHeightPreview' => false,
                'aspectRatio' => 1 / 1, //1 square (16/9, 4/3, etc)
//                'setSelect' => [0, 0, 100, 100], //initial cords area [x, y ,x2, y2]
//                'bgOpacity' => 0.5,
//                'bgColor' => 'black',
                'cropImageWidth' => 400, //if ommited and added cropImageHeight width is calculated according to aspectRatio
            ),
        ));
    }

}
