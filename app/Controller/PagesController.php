<?php

App::uses('AppController', 'Controller');

class PagesController extends AppController {

    public $helpers = array('Tree');

    /**
     * SITE
     * Afficher une page du site
     * 
     * Fonction "générale"
     * C'est par là que passent les pages utilisant le système de modules
     * (cas particulier de l'accueil qui passe d'abord par la fonction PagesController::index())
     * 
     * Traitement spécifique aux templates
     * Chaque template peut éventuellement disposer d'une fonction dans AppController qui sera exécutée juste avant de rendre la vue
     * (utile si un template nécessite un traitement léger mais spécifique)
     * Convention de nommage de ces fonctions : public function template_[nom du template]()
     * (ex. : public function template_accueil() pour un template nommé site-accueil.ctp)
     * 
     * @param string $alias : slug de la page
     * @param array $conditions : page ayant ses propres conditions (cas de la page d'accueil notamment)
     */
    public function display($alias = null, $conditions = array()) {       
        
        // Informations sur la page (AppController)
        if (empty($conditions)) {
            // Conditions par défaut
            $conditions = array(
                'Page.i18n_alias' => $alias,
                'Page.active' => 1,
                'Page.home' => 0
            );
        }
        $constructPage = $this->Page->construct($conditions);
        $this->set($constructPage);


        //recuperation d'un produit de saison aleatoire
        $this->loadModel('Produit');
        $allProduits = $this->Produit->find('all', array(
            'recursive' => 2,
            'conditions' => array(
                "not" => array(
                    "Produit.debut" => null
                )
            )
        ));
        $produits = array();
        foreach ($allProduits as $produit) {//produits de saison
            if ($produit['Produit']['debut'] > $produit['Produit']['fin']) {
                if (date('m') >= $produit['Produit']['debut'] || date('m') <= $produit['Produit']['fin']) {
                    $produits[] = $produit;
                }
            } else {
                if (date('m') >= $produit['Produit']['debut'] && date('m') <= $produit['Produit']['fin']) {
                    $produits[] = $produit;
                }
            }
        }
//        debug($produits);exit;
        $this->set("produits", $produits);
        //recuperation des recettes de saison pour le slider
        $this->loadModel('Recette');
        $allRecettes = $this->Recette->find('all', array(
            'conditions' => array(
                "not" => array(
                    "Recette.debut" => null
                )
            )
        ));
        $recettes = array();
        foreach ($allRecettes as $recette) {//recette de saison
            if ($recette['Recette']['debut'] > $recette['Recette']['fin']) {
                if (date('m') >= $recette['Recette']['debut'] || date('m') <= $recette['Recette']['fin']) {
                    $recettes[] = $recette;
                }
            } else {
                if (date('m') >= $recette['Recette']['debut'] && date('m') <= $recette['Recette']['fin']) {
                    $recettes[] = $recette;
                }
            }
        }

        $this->set("recettes", $recettes);

        //param page prouduit
        $type = null;
        if(!empty($this->request->query['type'])){
            $type = $this->request->query['type'];
        }
        // Liste complète des modules de la page
        $modules = $this->Page->Content->getModulesByPage($constructPage['page']['Page']['id'], $for = null, $type);
        $this->set("modules", $modules);

        // Affichage du template + traitement spécifique lié au template le cas échéant (AppController)
        $this->setTemplate($constructPage['page']['Template']['name'], $constructPage['page']);
    }

    /**
     * SITE
     * Page d'accueil du site
     */
    public function index() {

        // Informations de la page
        $conditions = array(
            'Page.home' => 1,
            'Page.active' => 1
        );
        //recuperation d'un produit de saison aleatoire
//        $this->loadModel('Produit');
//        $produits = $this->Produit->find('all');
//        $produitsIds = array();
//        foreach ($produits as $produit) {//produits de saison
//            if ($produit['Produit']['debut'] > $produit['Produit']['fin']) {
//                if (date('m') >= $produit['Produit']['debut'] || date('m') <= $produit['Produit']['fin']) {
//                    $produitsIds[$produit['Produit']['id']] = $produit['Produit']['id'];
//                }
//            } else {
//                if (date('m') >= $produit['Produit']['debut'] && date('m') <= $produit['Produit']['fin']) {
//                    $produitsIds[$produit['Produit']['id']] = $produit['Produit']['id'];
//                }
//            }
//        }
//        $produit = array();
//        if (!empty($produitsIds)) {
//            $produitId = array_rand($produitsIds);
//            $produit = $this->Produit->find('first', array(
//                'recursive' => 2,
//                'conditions' => array(
//                    'Produit.id' => $produitId,
//            )));
//        }
//        $this->set("produit", $produit);
//        debug($produit);exit;
//        //recuperation des produits de saison pour le slider
        $this->loadModel('Produit');
        $allProduits = $this->Produit->find('all', array(
            'recursive' => 2,
            'conditions' => array(
                "not" => array(
                    "Produit.debut" => null
                )
            )
        ));
        $produits = array();
        foreach ($allProduits as $produit) {//produits de saison
            if ($produit['Produit']['debut'] > $produit['Produit']['fin']) {
                if (date('m') >= $produit['Produit']['debut'] || date('m') <= $produit['Produit']['fin']) {
                    $produits[] = $produit;
                }
            } else {
                if (date('m') >= $produit['Produit']['debut'] && date('m') <= $produit['Produit']['fin']) {
                    $produits[] = $produit;
                }
            }
        }
//        debug($produits);exit;
        $this->set("produits", $produits);
        //recuperation des recettes de saison pour le slider
        $this->loadModel('Recette');
        $allRecettes = $this->Recette->find('all', array(
            'conditions' => array(
                "not" => array(
                    "Recette.debut" => null
                )
            )
        ));
        $recettes = array();
        foreach ($allRecettes as $recette) {//produits de saison
            if ($recette['Recette']['debut'] > $recette['Recette']['fin']) {
                if (date('m') >= $recette['Recette']['debut'] || date('m') <= $recette['Recette']['fin']) {
                    $recettes[] = $recette;
                }
            } else {
                if (date('m') >= $recette['Recette']['debut'] && date('m') <= $recette['Recette']['fin']) {
                    $recettes[] = $recette;
                }
            }
        }
//        debug($recettes);exit;
        $this->set("recettes", $recettes);
        $this->display(null, $conditions);
    }

    /**
     * SITE
     * Page contact (formulaire + envoi mail)
     * TODO : faire passer par la fonction display ?
     */
    public function contact() {
        
        //recuperation d'un produit de saison aleatoire
        $this->loadModel('Produit');
        $ids = $this->Produit->find('list', array(
            'conditions' => array(
                "not" => array(
                    "Produit.debut" => null
                )
            )
        ));
        $produitSaison = array();
        if (!empty($ids)) {
            $produitId = array_rand($ids);
            $produitSaison = $this->Produit->find('first', array(
                'recursive' => 2,
                'conditions' => array(
                    'Produit.id' => $produitId,
            )));
        }
        $this->set("produitSaison", $produitSaison);
        //recuperation de recette pour le slider
        $this->loadModel('Recette');
        $recettes = $this->Recette->find('all', array(
            'conditions' => array(
                "not" => array(
                    "Recette.debut" => null
                )
            )
        ));
        $this->set("recettes", $recettes); 
        

        // Traitement du formulaire
        if ($this->request->is('post') && isset($this->request->data['Contact']['captcha']) && empty($this->request->data['Contact']['captcha'])) {

            $this->loadModel('Contact');
            $this->Contact->set($this->request->data);

            if ($this->Contact->validates()) { // Les données sont valides, on envoie le mail
                if ($this->Contact->sendEmail($this->request->data)) {
                    $this->Session->setFlash("Votre message a bien été envoyé. Une copie vous a été transférée à l'adresse e-mail indiquée.", "Alerts/ContactSuccess");
                    $this->request->data = array();
                } else {
                    $this->Session->setFlash("Erreur durant l'envoi : veuillez réessayer ultérieurement.", "Alerts/ContactError");
                }
            }
        }

        // Informations sur la page
        $conditions = array(
            'PageType.slug' => 'contact',
            'Page.active' => 1
        );
        $this->set($this->Page->construct($conditions));

        // Affichage du template
        $this->setTemplate('contact');
    }

    public function adherent() {

        // Informations sur la page
        $conditions = array(
            'PageType.slug' => 'adherent',
            'Page.active' => 1
        );
        $this->set($this->Page->construct($conditions));
        
        //recuperation d'un produit de saison aleatoire
        $this->loadModel('Produit');
        $ids = $this->Produit->find('list', array(
            'conditions' => array(
                "not" => array(
                    "Produit.debut" => null
                )
            )
        ));
        $produitSaison = array();
        if (!empty($ids)) {
            $produitId = array_rand($ids);
            $produitSaison = $this->Produit->find('first', array(
                'recursive' => 2,
                'conditions' => array(
                    'Produit.id' => $produitId,
            )));
        }
        $this->set("produitSaison", $produitSaison);
        //recuperation de recette pour le slider
        $this->loadModel('Recette');
        $recettes = $this->Recette->find('all', array(
            'conditions' => array(
                "not" => array(
                    "Recette.debut" => null
                )
            )
        ));
        $this->set("recettes", $recettes);                
        //recuperation des adhérents
        $this->loadModel('Partner');
        $adherents = $this->Partner->find('all');
        $this->set("adherents", $adherents);
        // Affichage du template
        $this->setTemplate('adherent');
    }

    /**
     * SITE
     * Plan du site
     * TODO : à faire en XML aussi ? (bon pour Google)
     * TODO : faire passer par la fonction display ?
     */
    public function sitemap() {

        // Liste des pages ordonnées
        $children = $this->Page->find('threaded', array(
            'recursive' => 0,
            'conditions' => array(
                'Page.active' => 1
            ),
            'fields' => array(
                'Page.name', 'Page.alias', 'Page.parent_id', 'Page.home',
                'PageType.slug', 'PageType.controller', 'PageType.action'
            ),
            'order' => array('Page.menu DESC', 'Page.lft ASC')
        ));
        $this->set('children', $children);

        // Informations sur la page
        $conditions = array(
            'PageType.slug' => 'sitemap',
            'Page.active' => 1
        );
        $this->set($this->Page->construct($conditions));

        // Affichage du template
        $this->setTemplate('sitemap');
    }

    /**
     * ADMIN
     * Gestion arborescence des pages
     */
    public function admin_index() {
        $d['title_for_layout'] = "Gestion des pages";
        $d['pages'] = $this->Page->children(null, false, array("Page.*", "PageType.*"), null, null, 1, 0);
//        debug($d['pages']);exit;
        $this->set($d);
    }

    /**
     * ADMIN
     * Monter la page d'un cran à son niveau
     * @param int $id
     */
    public function admin_move($id, $direction, $delta = 1) {

        // Vérification
        $this->Page->id = $id;
        if (!$this->Page->exists()) {
            throw new NotFoundException('La page n\'existe pas ou plus');
        }

        // On bouge la page et tous ses enfants
        $move = false;
        switch ($direction) {
            case 'up':
                $move = $this->Page->moveUp($this->Page->id, $delta);
                break;
            case 'down':
                $move = $this->Page->moveDown($this->Page->id, $delta);
                break;
        }

        if ($this->request->is('ajax')) {

            $this->Page->id = null;
            $pages = $this->Page->children(null, false, array("Page.*", "PageType.*"), null, null, 1, 0);
            $this->set(compact('pages'));
            $this->render("/Elements/Components/Admin/admin_pages_tree");
        } else {

            if ($move === true) {
                $this->Session->setFlash("La page a bien été déplacée.", "Alerts/Success");
            } else {
                $this->Session->setFlash("La page n'a pu être déplacée.", "Alerts/Error");
            }

            $this->redirect(array(
                'action' => 'index',
                'language' => Configure::read('Config.language')
            ));
        }
    }

    /**
     * ADMIN
     * Mettre en ligne/hors-ligne une page
     * @param int $id
     * @param bool $active
     */
    public function admin_active($id, $active = 1) {

        // Vérification
        $this->Page->id = $id;
        if (!$this->Page->exists()) {
            throw new NotFoundException('La page n\'existe pas ou plus');
        }

        if ($this->Page->saveField('active', $active)) {
            if ($active == 0) {
                $this->Session->setFlash("La page a bien été mise hors-ligne.", "Alerts/Success");
            } else {
                $this->Session->setFlash("La page a bien été remise en ligne.", "Alerts/Success");
            }
        } else {
            $this->Session->setFlash("Erreur lors de la mise en ligne/hors-ligne de la page.", "Alerts/Error");
        }

        $this->redirect(array(
            'action' => 'index',
            'language' => Configure::read('Config.language')
        ));
    }

    /**
     * ADMIN
     * Création d'une page (puis redirection vers modification du contenu)
     */
    public function admin_add() {

        $d['title_for_layout'] = "Gestion des pages";

        if ($this->request->is('post')) {
            if ($this->Page->save($this->request->data)) {
                $this->Session->setFlash("La nouvelle page a bien été créée.", "Alerts/Success");
                $this->redirect(array(
                    'action' => 'index',
                    'language' => Configure::read('Config.language')
                ));
            } else {
                $this->Session->setFlash("Erreur inconnue lors de l'enregistrement de votre page.", "Alerts/Error");
            }
        } else {
            $this->request->data['Page']['menu'] = $this->request->data['Page']['active'] = 'on';
        }

        // Types des page disponibles
        $d['listePageTypes'] = $this->Page->PageType->getList();
        // Pages parentes disponibles
        $d['listePages'] = $this->Page->generateTreeList();
        // Templates disponibles
        $d['listeTemplates'] = $this->Page->Template->find('list', array(
            'fields' => array('Template.name'),
            'order' => array('Template.name ASC')
        ));
        $this->set($d);
    }

    /**
     * ADMIN
     * Modification des paramètrse généraux de la page (titre, métas...)
     * @param int $id
     */
    public function admin_edit($id) {

        $d['title_for_layout'] = "Gestion des pages";

        if ($this->request->is('post') || $this->request->is('put')) {
            // Si la page n'a pas pour vocation la gestion de modules : on vire le template associé
            $typeModule = $this->Page->PageType->getModuleType();
            if ($this->request->data['Page']['page_type_id'] !== $typeModule) {
                $this->request->data['Page']['template_id'] = null;
            }
            // Enregistrement
            if ($this->Page->save($this->request->data)) {
                $this->Session->setFlash("Les modifications de votre page ont bien été enregistrées.", "Alerts/Success");
            } else {
                $this->Session->setFlash("Erreur lors de la modification de la page.", "Alerts/Error");
            }
        } else {
            $this->Page->recursive = -1;
            $this->request->data = $this->Page->findById($id);
        }

        // Types des page disponibles
        $d['listePageTypes'] = $this->Page->PageType->getList();
        // Pages parentes disponibles
        $d['listePages'] = $this->Page->generateTreeList();
        // Templates disponibles
        $d['listeTemplates'] = $this->Page->Template->find('list', array(
            'fields' => array('Template.name'),
            'order' => array('Template.name ASC')
        ));
        $this->set($d);
    }

    /**
     * ADMIN
     * Modification des modules d'une page
     * @param int $id : Page.id
     */
    public function admin_edit_modules($id) {

        $title_for_layout = "Gestion des pages";
        $this->set(compact('title_for_layout'));

        // On récupère les infos templates de la page et les infos de la page
        $page = $this->Page->find('first', array(
            'fields' => array('Template.name', 'Page.id', 'Page.name', 'PageType.*', 'Page.home', 'Page.alias'),
            'conditions' => array(
                'Page.id' => $id
            )
        ));
        $this->set('templateName', $page['Template']['name']);
        $this->set('page', $page);

        // Si c'est une page de gestion de modules
        if ($page['PageType']['slug'] === "modules") {
            // On récupère les modules
            $modules = $this->Page->Content->getModulesByPage($id, "admin");
            $this->set("modules", $modules);
            // On récupère la liste des collections de modules disponibles
            $this->loadModel("Module");
            $listModules = $this->Module->getList();
            $listModules["page_id"] = $id; // On ajoute l'ID de la page pour faire plus simple
            $this->set("listModules", $listModules);
        }
    }

    /**
     * ADMIN
     * Suppression d'une page
     * @param int $id : Page.id
     */
    public function admin_delete($id) {

        if ($this->Page->delete($id)) {
            $this->Session->setFlash("La page a bien été supprimée.", "Alerts/Success");
        } else {
            $this->Session->setFlash("Suppression impossible : erreur lors de la suppression.", "Alerts/Error");
        }

        $this->redirect($this->referer());
    }

    /**
     * AJAX
     * /admin/pages/add/
     * Retourner un alias à partir d'une chaîne de caractère
     * @param string $str
     * @return type
     */
    public function admin_ajax_alias($str = null) {
        $this->autoRender = false;
        $this->layout = false;
        return strtolower(Inflector::slug($str, '-'));
    }

}
