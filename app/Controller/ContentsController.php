<?php
App::uses('AppController', 'Controller');

class ContentsController extends AppController {
    
    /**
     * ADMIN
     * Modification de l'ordre des modules
     * @param int $id
     * @param int $page_id
     * @param int $zone
     * @param int $rang
     * @param string $direction
     * @param int $delta
     */
    public function admin_move($id, $page_id, $zone, $rang, $direction, $delta = 1) {
        
        $this->autoLayout = false;
        $this->autoRender = false;
        
        switch($direction) {
            case "up": default:
                $new_rang = $rang - $delta;
                break;
            case "down":
                $new_rang = $rang + $delta;
                break;
        }
        
        $this->Content->updateAll(array(
            'Content.rang' => $rang
        ), array(
            'Content.page_id' => $page_id,
            'Content.zone' => $zone,
            'Content.rang' => $new_rang
        ));
        
        $this->Content->id = $id;
        $this->Content->save(array(
            'rang' => $new_rang
        ));
        
        $this->redirect($this->referer());
        
    }
    
    /**
     * ADMIN
     * Suppression d'un module et mise à jour des rangs des modules suivants
     * @param int $page_id
     * @param int $zone
     * @param int $rang
     * @param int $id
     */
    public function admin_delete($page_id, $zone, $rang, $id) {
        
        $this->Content->id = $id;
        if($this->Content->exists()) {
            // Suppression du module et de son association à la page
            $this->Content->delete($id, true);
            // Mise à jour des rangs suivants
            $this->Content->updateAll(array(
                'Content.rang' => 'Content.rang - 1'
            ), array(
                'Content.rang >' => $rang,
                'Content.page_id' => $page_id,
                'Content.zone' => $zone
            ));
            // Message de succès
            $this->Session->setFlash("Le module a bien été supprimé.", "Alerts/Success");
        } else {
            // Message d'erreur
            $this->Session->setFlash("Impossible de supprimer le module.", "Alerts/Error");
        }
        
        $this->redirect(array(
            'controller' => 'pages',
            'action' => 'edit_modules',
            'language' => Configure::read('Config.language'),
            $page_id
        ));
        
    }
    
}
?>