<?php
App::uses('AppController', 'Controller');

class MediaController extends AppController {
    
    public function admin_index() {
        
        $title_for_layout = "Gestion des médias";
        
        $this->set(compact('title_for_layout'));
        
    }
    
}
?>