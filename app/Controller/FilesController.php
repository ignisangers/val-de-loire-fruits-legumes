<?php
App::uses('AppController', 'Controller');

class FilesController extends AppController {
    
    /**
     * Listing des fichiers actuellement existantes
     */
    public function admin_index() {
        
        $title_for_layout = "Gestion des fichiers";
        
        // Comment fait-on pour obtenir un ordre au niveau parent, et un autre ordre dans les enfants ?
        // Faute d'avoir trouvé pour l'instant, la requête se fera en deux fois
        
        $module_files = $this->File->ModuleFile->find('all', array(
            'recursive' => -1,
            'order' => array('ModuleFile.id asc')
        ));
        foreach ($module_files as $k => $module_file) {
            $files = $this->File->find('all', array(
                'recursive' => -1,
                'order' => array('File.rang asc'),
                'conditions' => array('File.module_file_id' => $module_file['ModuleFile']['id']),
            ));
            foreach ($files as $file) {
                $module_files[$k]['File'][] = $file['File'];
            }
        }
        
        $this->set(compact("title_for_layout", 'module_files'));
    }
    
    /**
     * Ajout d'un fichier
     */
    public function admin_add() {
        
        // Besoin de stocker le fichier dans un module
        $module_file = $this->File->ModuleFile->find('first', array(
            'recursive' => -1,
        ));
        if (empty($module_file)) {
            $this->Session->setFlash("Il est nécessaire de créer un module de fichiers avant d'en ajouter.", "Alerts/Error");
            $this->redirect(array('action' => 'index'));
        }
        
        $title_for_layout = "Ajout d'un fichier";
        
        if($this->request->is('post')) {
            if($this->File->save($this->request->data)) {
                // Enregistrement du rang
                // Note : ne peut pas être dans un afterFind, avec le saveField cela ferait double emploi
                $this->File->saveField('rang', $this->File->id);
                
                $this->Session->setFlash("Le nouveau fichier a bien été ajouté.", "Alerts/Success");
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash("Erreur lors de l'enregistrement du fichier.", "Alerts/Error");
            }
        }
        
        $liste_modules = $this->File->ModuleFile->find('list', array(
            'fields' => array('ModuleFile.id', 'ModuleFile.title'),
        ));
        
        $this->set(compact("title_for_layout", 'liste_modules'));
    }
    
    /**
     * Édition d'un fichier
     * @param int $file_id ID de la personne à éditer
     */
    public function admin_edit($file_id) {
        
        $title_for_layout = "Modification d'un fichier";
        
        // Confirmer l'existence du fichier à éditer
        $this->File->id = $file_id;
        if (!$this->File->exists()) {
            $this->redirect(array('action' => 'index'));
        }
        
        if($this->request->is('post') || $this->request->is('put')) {
            if($this->File->save($this->request->data)) {
                $this->Session->setFlash("Le fichier a bien été modifié.", "Alerts/Success");
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash("Erreur lors de la mise à jour du fichier.", "Alerts/Error");
            }
        } else {
            $this->request->data = $this->File->find('first', array(
                'conditions' => array('File.id' => $file_id),
                'contain' => array('ModuleFile'),
            ));
        }
        
        $liste_modules = $this->File->ModuleFile->find('list', array(
            'fields' => array('ModuleFile.id', 'ModuleFile.title'),
        ));
        
        $this->set(compact("title_for_layout", 'liste_modules'));
    }
    
    
    /**
     * Suppression d'un fichier
     * @param type $file_id ID de la personne à supprimer
     */
    public function admin_delete($file_id) {
        
        $this->layout = false;
        $this->autoRender = false;
        
        if ($this->File->delete($file_id)) {
            $this->Session->setFlash("Ce fichier a été supprimé.", "Alerts/Success");
        } else {
            $this->Session->setFlash("Ce fichier n'a pas pu être supprimé ou n'existe pas.", "Alerts/Error");
        }
        $this->redirect(array('action' => 'index'));
    }
    
    /**
     * Modifier l'ordre des fichiers
     * @param int $file_id ID du fichier à déplacer
     * @param string $direction "up" ou "down" pour monter/baisser
     */
    public function admin_move($file_id, $direction = "up") {
        
        $this->autoLayout = false;
        
        if($this->request->is('ajax')) {
            // Vérification
            $this->File->id = $file_id;
            if (!$this->File->exists()) {
               throw new NotFoundException("Le fichier n'existe pas.");
            }
            
            $module_file_id = $this->File->field('module_file_id');
            $rang = $this->File->field('rang');

            // On bouge le fichier
            $voisins = $this->File->find('neighbors', array(
                'field' => 'rang',
                'value' => $rang,
                'conditions' => array('File.module_file_id' => $module_file_id),
                'order' => array('rang desc'),
            ));
            
            // Inversion des places entre le prev/next et l'élément choisi
            if ($direction === 'up' && !empty($voisins['prev'])) {
                $this->File->saveField('rang', $voisins['prev']['File']['rang']);
                $this->File->id = $voisins['prev']['File']['id'];
                $this->File->saveField('rang', $rang);
            } else if ($direction === 'down' && !empty($voisins['next'])) {
                $this->File->saveField('rang', $voisins['next']['File']['rang']);
                $this->File->id = $voisins['next']['File']['id'];
                $this->File->saveField('rang', $rang);
            }
            
            $files = $this->File->find('all', array(
                'recursive' => -1,
                'conditions' => array('File.module_file_id' => $module_file_id),
                'order' => array('File.rang asc'),
            ));
            $this->set(compact('files'));
        }
    }
}