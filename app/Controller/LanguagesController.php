<?php
App::uses('AppController', 'Controller');

class LanguagesController extends AppController {
    
    /**
     * ADMIN
     * Affichage Ajax de la liste des langues
     */
    public function admin_list() {
        
        if($this->request->is('ajax')) {
            
            $languages = Configure::read('Config.languages');
            /*debug($this->referer());
            $url = array(
                'controller' => $this->params['controller'],
                'action' => $this->params['action']
            );
            if(!empty($this->params['pass'])) {
                foreach($this->params['pass'] as $pass):
                    $url[] = $pass;
                endforeach;
            }*/
            $url = $this->request->referer();
            $this->set(compact('languages', 'url'));
            
        }
        
    }
    
    /**
     * ADMIN
     * Changement de langue pour les traductions
     * @param string $language
     */
    /*public function admin_change($language) {
        
        $this->Session->write('Config.language', $language);
        $this->redirect($this->referer());
        
    }*/
    
}
?>