<?php
App::uses('AppController', 'Controller');

class ModuleEvenementsController extends AppController {
    
    /**
     * Ajout d'un module dans une zone d'une page
     * @param int $page_id
     * @param int $zone
     */
    public function admin_add($page_id, $zone) {
        
        $title_for_layout = "Gestion des pages";
        
        if($this->request->is('post')) {
            if($this->ModuleEvenement->saveAssociated($this->request->data)) {
                // On va décaler les rangs des autres modules de la zone
                $this->{$this->modelClass}->Content->updateOrder($page_id, $zone, $this->{$this->modelClass}->Content->id);
                // On redirige
                $this->Session->setFlash("Votre module d'évènements a bien été ajouté.", "Alerts/Success");
                $this->redirect(array(
                    'controller' => 'pages',
                    'action' => 'edit_modules',
                    'language' => Configure::read('Config.language'),
                    $page_id
                ));
            } else {
                $this->Session->setFlash("Erreur lors de l'enregistrement de votre module d'évènements.", "Alerts/Error");
            }
        }
        
        $page = $this->ModuleEvenement->Content->Page->find('first', array(
            'conditions' => array(
                'Page.id' => $page_id,
                'Page.active' => 1
            ),
            'recursive' => -1,
            'fields' => array('Page.name')
        ));
        
        $this->set(compact("title_for_layout"));
        $this->set('page', $page);
        $this->set('page_id', $page_id);
        $this->set('zone', $zone);
        
    }
    
    /**
     * Édition d'un module dans une zone d'une page
     * @param int $id
     */
    public function admin_edit($id, $page_id) {
        
        $title_for_layout = "Gestion des pages";
        
        $this->ModuleEvenement->id = $id;
        
        if($this->request->is('post') || $this->request->is('put')) {
            
            if($this->ModuleEvenement->save($this->request->data)) {
                $this->Session->setFlash("Votre module d'évènements a bien été modifié.", "Alerts/Success");
                $this->redirect(array(
                    'controller' => 'pages',
                    'action' => 'edit_modules',
                    'language' => Configure::read('Config.language'),
                    $page_id
                ));
            } else {
                $this->Session->setFlash("Erreur lors de la mise à jour de votre module d'évènements.", "Alerts/Error");
            }
        
        } else {
            $this->request->data = $this->ModuleEvenement->read();
        }
        
        $page = $this->ModuleEvenement->Content->Page->find('first', array(
            'conditions' => array(
                'Page.id' => $page_id,
                'Page.active' => 1
            ),
            'recursive' => -1,
            'fields' => array('Page.name')
        ));
        
        $this->set(compact('title_for_layout'));
        $this->set('page', $page);
        $this->set('page_id', $page_id);
        
    }
}