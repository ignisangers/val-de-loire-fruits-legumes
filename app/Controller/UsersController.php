<?php
App::uses('AppController', 'Controller');

class UsersController extends AppController {
    
    public function admin_login() {
        $this->layout = "login";
        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                $this->User->id = $this->Auth->user('id');
                $this->User->save();
                $this->redirect(array('controller' => 'dashboards', 'action' => 'index', 'language' => Configure::read('Config.language'), 'admin' => true));
            } else {
                $this->Session->setFlash('Your username or password was incorrect.');
            }
        }
    }

    public function admin_logout() {
        $this->redirect($this->Auth->logout());
    }
    
    /**
     * ADMIN
     * Liste des utilisateurs du CMS
     */
    public function admin_index() {
        
        $title_for_layout = "Gestion des utilisateurs";
        
        $this->User->bindModel(array(
            "belongsTo" => array(
                "Role" => array(
                    "className" => "Role",
                    "foreignKey" => "role",
                    "fields" => array("Role.name")
                )
            )
        ));
        $conditions = array();
        if($this->Auth->user('role') == 'client') {
            $conditions['User.role !='] = "admin";
        }
        $users = $this->User->find('all', array(
            'order' => array('User.nom ASC', 'User.prenom ASC'),
            'conditions' => $conditions
        ));
        
        $this->set(compact('title_for_layout', 'users'));
        
    }
    
    /**
     * ADMIN
     * Ajout d'un utilisateur du CMS
     */
    public function admin_add() {
        
        $title_for_layout = "Gestion des utilisateurs";
        
        if($this->request->is('post')) {
            
            $this->User->create();
            $password = $this->request->data['User']['password'];
            if($this->User->save($this->request->data)) {
                $this->Session->setFlash("Le nouvel utilisateur a bien été enregistré. Son mot de passe est : <strong>" . $password . "</strong>.", "Alerts/Success");
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash("Erreur lors de l'enregistrement de l'utilisateur.", "Alerts/Error");
            }
            
        }
        
        $this->loadModel('Role');
        $options = $this->Role->find('list', array(
            'fields' => array('Role.role', 'Role.name'),
            'order' => array('Role.name ASC'),
            'conditions' => $conditions
        ));
        $suffixe = "";
        $i=0;
        foreach($options as $key => $option):
            if($i > 0) {
                $suffixe .= "--";
            }
            $options[$key] = $suffixe . " " . $option;
            $i++;
        endforeach;
        
        $this->set(compact('title_for_layout', 'options'));
        
    }
    
    /**
     * ADMIN
     * Modification d'un utilisateur du CMS
     * @param int $id
     */
    public function admin_edit($id) {
        
        $title_for_layout = "Gestion des utilisateurs";
        
        $this->User->id = $id;
        if(!$this->User->exists()) {
            $this->Session->setFlash("Cet utilisateur n'existe pas ou plus.", "Alerts/Error");
            $this->redirect($this->referer());
        }
        
        if($this->request->is('post') || $this->request->is('put')) {
            if(empty($this->request->data['User']['password'])) {
                unset($this->request->data['User']['password']);
            }
            if($this->User->save($this->request->data)) {
                $this->Session->setFlash("L'utilisateur a bien été mis à jour.", "Alerts/Success");
                $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash("Erreur lors de la mise à jour de l'utilisateur.", "Alerts/Error");
            }
            
        } else {
            $this->request->data = $this->User->read();
            unset($this->request->data['User']['password']);
        }
        
        $conditions = array();
        if($this->Auth->user('role') == 'client') {
            $conditions['Role.role !='] = "admin";
        }
        
        $this->loadModel('Role');
        $options = $this->Role->find('list', array(
            'fields' => array('Role.role', 'Role.name'),
            'order' => array('Role.name ASC'),
            'conditions' => $conditions
        ));
        $suffixe = "";
        $i=0;
        foreach($options as $key => $option):
            if($i > 0) {
                $suffixe .= "--";
            }
            $options[$key] = $suffixe . " " . $option;
            $i++;
        endforeach;
        
        $this->set(compact('title_for_layout', 'options'));
        
    }
    
    /**
     * ADMIN
     * Dupliquer un utilisateur
     * @param int $id
     */
    public function admin_duplicate($id) {
        
        $this->User->Behaviors->unload("Uploader.Attachment");
        if($this->User->exists($id) && $this->User->copy($id)) {
            $this->Session->setFlash("L'utilisateur a bien été dupliqué.", "Alerts/Success");
        } else {
            $this->Session->setFlash("Duplication impossible : erreur lors de la duplication.", "Alerts/Error");
        }
        
        $this->redirect($this->referer());
        
    }
    
    /**
     * ADMIN
     * Suppression d'un utilisateur
     * @param int $id
     */
    public function admin_delete($id) {
        
        if($this->User->exists($id) && $this->User->delete($id)) {
            $this->Session->setFlash("L'utilisateur a bien été supprimé.", "Alerts/Success");
        } else {
            $this->Session->setFlash("Suppression impossible : erreur lors de la suppression.", "Alerts/Error");
        }
        
        $this->redirect($this->referer());
        
    }
    
}
?>