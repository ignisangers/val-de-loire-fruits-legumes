<?php
App::uses('AppController', 'Controller');

class ModulesController extends AppController {
    
    /**
     * Collections de modules
     * Pour restreindre la liste des modules dispos en fonction des zones de contenus
     */
//    public $collections = array(
//        "all" => array(
//            "ModuleArticle",
//            "ModuleActualite",
//            "ModuleGallery",
//            "ModuleLien",
//            "ModuleEvenement",
//            "ModuleFile",
//            "ModuleTrombinoscope",
//            "ModuleGroupe",
//            "ModuleHtml",
//        ),
//        "link" => array(
//            "ModuleLien"
//        )
//    );
    
    /**
     * ADMIN
     * Liste des modules disponibles pour gérer le contenu d'une page
     * Appelé notamment depuis les layouts (/View/Elements/layout[x].ctp)
     */
//    public function admin_liste_modules_disponibles($page_id, $zone, $collection = "all") {
//        
//        // Collection de modules choisi
//        if(!isset($this->collections[$collection])) {
//            $collection = "all";
//        }
//        
//        $liste = $this->Module->find('all', array(
//            'fields' => array('Module.name', 'Module.model'),
//            'order' => array('Module.name ASC'),
//            'conditions' => array(
//                'Module.model' => $this->collections[$collection]
//            )
//        ));
//        
//        $this->set('modules', $liste);
//        $this->set('page_id', $page_id);
//        $this->set('zone', $zone);
//        
//    }
    
}