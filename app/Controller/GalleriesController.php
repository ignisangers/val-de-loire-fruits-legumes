<?php
class GalleriesController extends AppController {
    
    /**
     * AJAX
     * Ajout d'une nouvelle image dans la galerie
     * @param string $data["GalleryAdd"]["filename"] : l'image en question
     * @param int $data["GalleryAdd"]["module_gallery_id"] : ID du module galerie
     * @param string $data["GalleryAdd"]["caption"] : légende de l'image
     */
    public function admin_add() {

        $this->layout = false;
        $this->autoRender = false;

        // Données à sauvegarder
        $data = $this->request->data;
        // Gestion de l'image
        $filename = $this->_uploadThumbnail($data["GalleryAdd"]["filename"]);

        // On sauvegarde la nouvelle image
        if ($filename !== false) {
            $this->Gallery->create();
            $saveData = array(
                "module_gallery_id" => $data["GalleryAdd"]["module_gallery_id"],
                "filename" => $filename,
                "caption" => $data["GalleryAdd"]["caption"],
                "rang" => 1
            );
            $this->Gallery->save($saveData);

            // Puis on met à jour la galerie entière
            $this->Gallery->orderAfterCreate($data["GalleryAdd"]["module_gallery_id"], $this->Gallery->id);    
        }
        
        // On renvoie à la vue la liste à jour
        $this->_updatedList($data["GalleryAdd"]["module_gallery_id"]);
    }
    
    public function admin_edit() {
        
        $this->layout = false;
        $this->autoRender = false;
        
        // Données à sauvegarder
        $data = $this->request->data;
        // Gestion de l'image
        $filename = $this->_uploadThumbnail($data["Gallery"]["filename"]);
        
        $saveData = array(
            "caption" => $data["Gallery"]["caption"],            
        );
        if ($filename !== false) {
            $saveData["filename"] = $filename;
        }
        $this->Gallery->id = $data["Gallery"]["id"];
        $this->Gallery->save($saveData);
        
        // On renvoie à la vue la liste à jour
        $this->_updatedList($this->Gallery->field("module_gallery_id", array("id" => $data["Gallery"]["id"])));
    }
    
    /**
     * AJAX
     * Intervertion de deux images d'une galerie
     * @param int $data["id"] : ID de l'image à bouger
     * @param int $data["module_gallery_id"] : ID du module galerie où se trouve l'image
     * @param string $data["direction"] : dans quelle direction veut-on bouger l'image (up/down) ?
     */
    public function admin_move() {
        
        $this->layout = false;
        $this->autoRender = false;

        // Données de suppression
        $data = $this->request->data;
        
        if ($this->Gallery->exists($data["id"])) {
            // L'image en question
            $rang = $this->Gallery->field("rang", array(
                "Gallery.id" => $data["id"]
            ));

            // On bouge d'abord l'image adjacente dont on va prendre la place
            $this->Gallery->updateAll(array(
                "Gallery.rang" => $rang
            ), array(
                "Gallery.module_gallery_id" => $data["module_gallery_id"],
                "Gallery.rang" => ($data["direction"] === "up") ? $rang - 1 : $rang + 1
            ));

            // Puis on bouge l'image souhaitée
            $this->Gallery->id = $data["id"];
            $this->Gallery->save(array(
                "rang" => ($data["direction"] === "up") ? $rang - 1 : $rang + 1
            ));
        }
        
        // On renvoie à la vue la liste à jour
        $this->_updatedList($data["module_gallery_id"]);
        
    }
    
    /**
     * AJAX
     * Suppression d'une image de la galerie
     * @param int $data["id"] : ID de l'image à supprimer
     * @param int $data["module_gallery_id"]  : ID de la galerie où se trouve l'image
     */
    public function admin_delete() {
        
        $this->layout = false;
        $this->autoRender = false;

        // Données de suppression
        $data = $this->request->data;
        
        // Quelques infos de l'image
        $gallery = $this->Gallery->find("first", array(
            "conditions" => array(
                "Gallery.id" => $data["id"]
            ),
            "fields" => array(
                "Gallery.id", "Gallery.filename", "Gallery.rang"
            )
        ));
        
        if (!empty($gallery)) {
            // On supprime l'image en BDD...
            if ($this->Gallery->delete($gallery["Gallery"]["id"])) {
                // ...puis physiquement
                @unlink($this->Gallery->path . $gallery["Gallery"]["filename"]);

                // Enfin on change l'ordre des autres images
                $this->Gallery->orderAfterDelete($data["module_gallery_id"], $gallery["Gallery"]["rang"]);
            }
        }
        
        // On renvoie la vue
        $this->_updatedList($data["module_gallery_id"]);
    }
    
    /**
     * AJAX - GET
     * Affichage de la modale d'ajout d'une image
     * @param int $id : ID du module galerie
     */
    public function admin_modal_add($id) {
        $this->layout = "modal";
        $this->request->data["GalleryAdd"]["module_gallery_id"] = $id;
    }
    
    /**
     * AJAX - GET
     * Affichage de la modale d'édition d'une image existante
     */
    public function admin_modal_edit() {
        $this->layout = "modal";
        
        // Informations sur l'image
        $id = $this->request->query["id"];
        $this->request->data = $this->Gallery->findById($id);
        $this->set("gallery_path", $this->Gallery->path);
        
    }

    /**
     * AJAX
     * Si on a mis une vignette, on l'upload et on l'ajoute aux données à sauvegarder
     * @param string $filename
     * @param string $imgType
     * @return mixed
     */
    private function _uploadThumbnail($filename, $imgType = null) {

        // Y a-t-il une vignette à uploader ?
        if (!is_array($filename) && !empty($filename)) {
            // On la renomme pour qu'elle soit bien formattée et unique
            preg_match('~image/(.*?);base64~', $filename, $imgType);
            $name = uniqid() . '.' . $imgType[1];

            // On récupère la photo et on l'upload
            App::uses('Folder', 'Utility');
            $photo_decoded = base64_decode(substr($filename, strpos($filename, "base64,") + 7));
            $dir = new Folder($this->Gallery->path, true, 0755);
            if (file_put_contents($dir->path . $name, $photo_decoded) === false) {
                return false;
            }

            // On retourne le nom à sauvegarder
            return $name;
        }

        return false;
    }
    
    /**
     * On va afficher la version à jour de la galerie pour l'admin
     */
    private function _updatedList($id) {
        $gallery = $this->Gallery->getGallery($id);
        $this->set(compact("gallery"));
        $this->render("/Elements/Modules/Admin/admin_galleries");
    }

}