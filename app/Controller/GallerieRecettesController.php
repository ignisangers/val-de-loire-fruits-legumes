<?php

class GallerieRecettesController extends AppController {

    /**
     * AJAX
     * Ajout d'une nouvelle image dans la galerie
     * @param string $data["GallerieRecetteAdd"]["filename"] : l'image en question
     * @param int $data["GallerieRecetteAdd"]["module_gallery_id"] : ID du module galerie
     * @param string $data["GallerieRecetteAdd"]["caption"] : légende de l'image
     */
    public function admin_add() {
        $this->layout = false;
        $this->autoRender = false;
        // Données à sauvegarder
        $data = $this->request->data;
        // Gestion de l'image
        $filename = $this->_uploadThumbnail($data["GallerieRecetteAdd"]["filename"]);
        // On sauvegarde la nouvelle image
        if ($filename !== false) {
            $this->GallerieRecette->create();
            $saveData = array(
                "recette_id" => $data["GallerieRecetteAdd"]["recette_id"],
                "filename" => $filename,
//                "caption" => $data["GallerieProduitAdd"]["caption"],
                "rang" => 1
            );
            $this->GallerieRecette->save($saveData);

            // Puis on met à jour la galerie entière
            $this->GallerieRecette->orderAfterCreate($data["GallerieRecetteAdd"]["recette_id"], $this->GallerieRecette->id);
        }
        // On renvoie à la vue la liste à jour
        $this->_updatedList($data["GallerieRecetteAdd"]["recette_id"]);
    }

    public function admin_edit() {

        $this->layout = false;
        $this->autoRender = false;

        // Données à sauvegarder
        $data = $this->request->data;
        // Gestion de l'image
        $filename = $this->_uploadThumbnail($data["GallerieRecette"]["filename"]);

//        $saveData = array(
//            "caption" => $data["GallerieProduit"]["caption"],            
//        );

        if ($filename !== false) {
            $saveData["filename"] = $filename;

            $this->GallerieRecette->id = $data["GallerieRecette"]["id"];
            $this->GallerieRecette->save($saveData);
        }


        // On renvoie à la vue la liste à jour
        $this->_updatedList($this->GallerieRecette->field("recette_id", array("id" => $data["GallerieRecette"]["id"])));
    }

    /**
     * AJAX
     * Intervertion de deux images d'une galerie
     * @param int $data["id"] : ID de l'image à bouger
     * @param int $data["module_gallery_id"] : ID du module galerie où se trouve l'image
     * @param string $data["direction"] : dans quelle direction veut-on bouger l'image (up/down) ?
     */
    public function admin_move() {

        $this->layout = false;
        $this->autoRender = false;

        // Données de suppression
        $data = $this->request->data;
        // Quelques infos de l'image
        $gallery = $this->GallerieRecette->find("first", array(
            "conditions" => array(
                "GallerieRecette.id" => $data["id"]
            ),
            "fields" => array(
                "GallerieRecette.id", "GallerieRecette.recette_id", "GallerieRecette.filename", "GallerieRecette.rang"
            )
        ));
        if ($this->GallerieRecette->exists($data["id"])) {
            // L'image en question
            $rang = $this->GallerieRecette->field("rang", array(
                "GallerieRecette.id" => $data["id"]
            ));

            // On bouge d'abord l'image adjacente dont on va prendre la place
            $this->GallerieRecette->updateAll(array(
                "GallerieRecette.rang" => $rang
                    ), array(
                "GallerieRecette.recette_id" => $gallery["GallerieRecette"]["recette_id"],
                "GallerieRecette.rang" => ($data["direction"] === "up") ? $rang - 1 : $rang + 1
            ));

            // Puis on bouge l'image souhaitée
            $this->GallerieRecette->id = $data["id"];
            $this->GallerieRecette->save(array(
                "rang" => ($data["direction"] === "up") ? $rang - 1 : $rang + 1
            ));
        }

        // On renvoie à la vue la liste à jour
        $this->_updatedList($gallery["GallerieRecette"]["recette_id"]);
    }

    /**
     * AJAX
     * Suppression d'une image de la galerie
     * @param int $data["id"] : ID de l'image à supprimer
     * @param int $data["module_gallery_id"]  : ID de la galerie où se trouve l'image
     */
    public function admin_delete() {

        $this->layout = false;
        $this->autoRender = false;

        // Données de suppression
        $data = $this->request->data;
        // Quelques infos de l'image
        $gallery = $this->GallerieRecette->find("first", array(
            "conditions" => array(
                "GallerieRecette.id" => $data["id"]
            ),
            "fields" => array(
                "GallerieRecette.id", "GallerieRecette.recette_id", "GallerieRecette.filename", "GallerieRecette.rang"
            )
        ));

        if (!empty($gallery)) {
            // On supprime l'image en BDD...
            if ($this->GallerieRecette->delete($gallery["GallerieRecette"]["id"])) {
                // ...puis physiquement
                @unlink($this->GallerieRecette->path . $gallery["GallerieRecette"]["filename"]);

                // Enfin on change l'ordre des autres images
                $this->GallerieRecette->orderAfterDelete($data["id"], $gallery["GallerieRecette"]["rang"]);
            }
        }

        // On renvoie la vue
        $this->_updatedList($gallery["GallerieRecette"]["recette_id"]);
    }

    /**
     * AJAX - GET
     * Affichage de la modale d'ajout d'une image
     * @param int $id : ID du module galerie
     */
    public function admin_modal_add($id) {
        $this->layout = "modal";
        $this->request->data["GallerieRecetteAdd"]["recette_id"] = $id;
    }

    /**
     * AJAX - GET
     * Affichage de la modale d'édition d'une image existante
     */
    public function admin_modal_edit() {
        $this->layout = "modal";
        
        $this->initJsVars();
        // Informations sur l'image
        $id = $this->request->query["id"];
        $this->request->data = $this->GallerieRecette->findById($id);
        $this->set("gallery_path", $this->GallerieRecette->path);
    }

    /**
     * AJAX
     * Si on a mis une vignette, on l'upload et on l'ajoute aux données à sauvegarder
     * @param string $filename
     * @param string $imgType
     * @return mixed
     */
    private function _uploadThumbnail($filename, $imgType = null) {

        // Y a-t-il une vignette à uploader ?
        if (!is_array($filename) && !empty($filename)) {
            // On la renomme pour qu'elle soit bien formattée et unique
            preg_match('~image/(.*?);base64~', $filename, $imgType);
            $name = uniqid() . '.' . $imgType[1];

            // On récupère la photo et on l'upload
            App::uses('Folder', 'Utility');
            $photo_decoded = base64_decode(substr($filename, strpos($filename, "base64,") + 7));
            $dir = new Folder($this->GallerieRecette->path, true, 0755);
            if (file_put_contents($dir->path . $name, $photo_decoded) === false) {
                return false;
            }

            // On retourne le nom à sauvegarder
            return $name;
        }

        return false;
    }

    /**
     * On va afficher la version à jour de la galerie pour l'admin
     */
    private function _updatedList($id) {
        $gallery = $this->GallerieRecette->getGallerieRecette($id);
        $this->set(compact("gallery"));
        $this->render("/Elements/Modules/Admin/admin_gallerie_recettes");
    }
    
    /**
     * Setting Javascript Global Vars
     * 
     * @return void
     */
    private function initJsVars() {
        $this->setJsVar('cropConfig', array(
            'default' => array(
//                'maxNumImagesAllowed' => 1,
//                'maxFileSizeAllowed' => 10, //MBs
//                'qualityJpeg' => 0.85, //from 0.0 to 1 (normal default is 0.92)
//                'allowResize' => true,
//                'selMinSize' => array(50, 50), //pxs
//                'maxWidthPreview' => 530, //pxs
//                'maxHeightPreview' => false,
                'aspectRatio' => 1 / 1, //1 square (16/9, 4/3, etc)
//                'setSelect' => [0, 0, 100, 100], //initial cords area [x, y ,x2, y2]
//                'bgOpacity' => 0.5,
//                'bgColor' => 'black',
                'cropImageWidth' => 800, //if ommited and added cropImageHeight width is calculated according to aspectRatio
            ),
        ));
    }

    
    
}
