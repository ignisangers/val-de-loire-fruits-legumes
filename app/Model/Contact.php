<?php
class Contact extends AppModel {
    
    public $useTable = false;
    
    public $validate = array(
        "nom" => array(
            "rule" => array("notEmpty"),
            "message" => "Vous devez indiquer votre nom de famille"
        ),
        "prenom" => array(
            "rule" => array("notEmpty"),
            "message" => "Vous devez indiquer votre prénom"
        ),
        "email" => array(
            "rule" => array("email"),
            "message" => "Vous devez indiquer un email valide"
        ),
        "comment" => array(
            "rule" => array("notEmpty"),
            "message" => "Vous devez écrire un message"
        )
    );
    
    public function sendEmail($data) {

        App::uses('CakeEmail', 'Network/Email');

        $mail = new CakeEmail('default');
        $data = current($data);
        
        // Confirmation d'envoi
        $data['to'] = "away";
        $mail->to($data['email'])
                ->subject('Val de Loire Fruits et Légumes : confirmation d\'envoi')
                ->emailFormat('html')
                ->template('email')
                ->viewVars($data);
        $mail->send();
        
        // Envoi à Nature et Stratégie
        $data['to'] = "home";
        
        $mail->to("carlos.lusi@gmail.com")
                ->subject('Message du site : Val de Loire Fruits et Légumes')
                ->emailFormat('html')
                ->template('email')
                ->viewVars($data);
        if($mail->send()) {
            return true;
        } else {
            return false;
        }
    }
    
}
?>