<?php
class ModuleHtml extends AppModel {
    
    public $belongsTo = array('Content');
    
    public function beforeSave($options = array()) {
        parent::beforeSave($options);
        // Soucis pour les caractères spéciaux, en particulier le point d'interrogation
        $this->data[$this->alias]['content'] = $this->data[$this->alias]['content'];
        return true;
    }
    
    /**
     * ADMIN
     * À la création d'un module, on crée toutes les traductions
     * @param int $id
     * @param array $datas
     * @return boolean
     */
    /*public function afterSave($created) {
        parent::afterSave($created);
        
        if($created) {
            // On enregistre chaque traduction par défaut
            foreach(Configure::read('Config.languages') as $code => $language):
                if($code != Configure::read('Config.language')) {
                    $this->locale = $code;
                    $this->save($this->data['ModuleArticle']);
                }
            endforeach;
        }
        
        return true;
    }*/
    
}
?>