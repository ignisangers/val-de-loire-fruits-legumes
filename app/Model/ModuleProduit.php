<?php
class ModuleProduit extends AppModel {
    
    public $belongsTo = array('Content');
    public $recursive = -1;
    
    /**
     * Override de la méthode dans AppModel
     *
     * On va récupérer les informations du module + les actualités correspondant à ces informations
     * 
     * @param int $content_id
     * @param string $type (fruit, legume)
     * @return array
     */
    public function getModule($content_id, $type) {
        
        // On récupère le module en lui-même d'abord
        $module = $this->findByContentId($content_id);
        
        // On le complète avec une liste d'actualités correspondantes
        // Si on transmet un paramètre de pagination et qu'on a imposé un maximum à afficher, on crée une pagination
        $urlArgs = Router::getParam("named");
        $page = (isset($urlArgs["page"])) ? $urlArgs["page"] : 1;
        
        // Récupération des produits
        $this->Produit = ClassRegistry::init('Produit');
        $conditions = array();
        if(!empty($type)){
            $conditions = array('type' => $type);
        }
        $produits["Produits"] = $this->Produit->find("all", array(
            "conditions" => $conditions,
            "order" => array(
                "Produit.name ASC"
            ),
            "limit" => $module["ModuleProduit"]["afficher_nb"],
            "page" => $page
        ));
        
        // Création d'une pagination (liste de liens) ?
        $pagination = array();
        $newsLink = array();
        if ($module["ModuleProduit"]["pagination"]) {
            $pagination["Pagination"] = $this->_setPagination($page, $module["ModuleProduit"]["afficher_nb"]);
        } else {
            // Si pas de pagination, on prévoit un lien vers la page Actualités (si elle existe)
            $pageProduits = $this->Content->Page->getInfo($this->Produit->pageConditions);
            if (!empty($pageProduits)) {
                $newsLink["hrefPageProduits"] = array(
                    "controller" => $pageNews["PageType"]["controller"],
                    "action" => $pageNews["PageType"]["action"],
                );
            }
        }
        
        return array_merge($module, $produits, $pagination, $newsLink);
        
    }
    
    /**
     * Préparation d'une pagination pour un module actualités
     * On utilisera une fonction de AppModel pour composer les liens
     * @param int $currentPage
     * @param int $maxPages
     * @return array
     */
    private function _setPagination($currentPage, $maxPages) {
        
        // Combien de liens faut-il générer ?
        $nbProduits = $this->Produit->find("count");
        $nbLinks = ceil($nbProduits / $maxPages);

        return array(
            "current" => $currentPage*1,
            "links" => $this->setPaginationLinks($nbLinks) // Fonction AppModel
        );
        
    }
}
