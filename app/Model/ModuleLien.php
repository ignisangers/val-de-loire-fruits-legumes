<?php
class ModuleLien extends AppModel {
    
    public $belongsTo = array('Content', 'Page');
    public $actsAs = array(
        'Translate' => array('name' => 'nameTranslations', 'content' => 'contentTranslations')
    );
    
    /**
     * Récupération d'un module d'une page
     * @param int $content_id
     * @return array
     */
    public function getModule($content_id) {
        
        $lien = $this->find("first", array(
            "recursive" => -1,
            "conditions" => array(
                "ModuleLien.content_id" => $content_id
            ),
            "fields" => array(
                "ModuleLien.id", "ModuleLien.name", "ModuleLien.content", "ModuleLien.page_id"
            )
        ));
        
        $page = $this->Page->find("first", array(
            "recursive" => -1,
            "conditions" => array(
                "Page.id" => $lien["ModuleLien"]["page_id"]
            ),
        ));
        
        return array_merge($lien, $page);
        
    }
    
//    public function afterFind($results, $primary = false) {        
//        // Pb : CakePHP ne peut pas récupérer les traductions si ça ne concerne pas le modèle sur lequel on fait le find
//        // Solution (temporaire ?) : on refait une requête pour récupérer la traduction...
//        if(isset($results[0]['ModuleLien']['page_id'])) {
//            $this->bindModel(
//                array('belongsTo' => array(
//                    'Page' => array(
//                        'fields' => array(
//                            'Page.name', 'Page.alias'
//                        )
//                    )
//                ))
//            );
//            $page = $this->Page->find('first', array(
//                'recursive' => -1,
//                'contains' => false,
//                'conditions' => array(
//                    'Page.id' => $results[0]['ModuleLien']['page_id']
//                ),
//                'fields' => array('Page.alias')
//            ));
//            $results[0]['ModuleLien']['Page'] = $page['Page'];
//        }
//        
//        return $results;
//    }
    
}
?>