<?php

/**
 * Description of Evenement
 *
 * @author meowcate
 */
class Evenement extends AppModel {
    
    public $recursive = -1;
    public $validate = array(
        'date_debut' => array(
            'datetime' => array(
                'rule' => array('datetime'),
                'message' => "Vous devez entrer une date valide",
            ),
            'notEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => "La date de début doit être renseignée.",
                'allowEmpty' => false,
            ),
        ),
        'date_fin' => array(
            'datetime' => array(
                'rule' => array('datetime'),
                'message' => "Vous devez entrer une date valide",
                'allowEmpty' => true,
            ),
            'checkdate' => array(
                'rule' => array('check_dates'),
                'message' => "La date de fin doit être supérieure à la date de début.",
            ),
        ),
    );
    
    /**
     * Vérification de la bonne conformité des dates : date de début inférieure à date de fin si renseignée
     * @return bool 
     */
    public function check_dates() {
        
        if (empty($this->data[$this->alias]['date_debut'])) {
            return false;
        }
        if (!empty($this->data[$this->alias]['date_fin'])) {
            return strtotime($this->data[$this->alias]['date_debut']) < strtotime($this->data[$this->alias]['date_fin']);
        }
        return true;
    }
    
    public function beforeValidate($options = array()) {
        parent::beforeValidate($options);
        
        // Reconstruction des dates
        if (!empty($this->data['Evenement']['debut_date'])) {
            if (!empty($this->data['Evenement']['debut_heure'])) {
                $this->data['Evenement']['date_debut'] = $this->data['Evenement']['debut_date'] . ' ' . $this->data['Evenement']['debut_heure'];
            } else {
                $this->data['Evenement']['date_debut'] = $this->data['Evenement']['debut_date'] . ' 00:00:00';
            }
        } else {
            $this->data['Evenement']['date_debut'] = null;
        }
        if (!empty($this->data['Evenement']['fin_date'])) {
            if (!empty($this->data['Evenement']['fin_heure'])) {
                $this->data['Evenement']['date_fin'] = $this->data['Evenement']['fin_date'] . ' ' . $this->data['Evenement']['fin_heure'];
            } else {
                $this->data['Evenement']['date_fin'] = $this->data['Evenement']['fin_date'] . ' 00:00:00';
            }
        } else {
            $this->data['Evenement']['date_fin'] = null;
        }
        return true;
    }
    
}
