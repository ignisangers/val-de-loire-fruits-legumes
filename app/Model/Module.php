<?php
class Module extends AppModel {
    
    /**
     * Collections de modules
     * Pour restreindre la liste des modules dispos en fonction des zones de contenus
     */
    public $collections = array(
        "link" => array(
            "ModuleLien"
        ),
        "gallery" => array(
            "ModuleGallery",
            "ModuleLien"
        )
    );
    
    /**
     * Liste complète de modules + collections
     * @return array
     */
    public function getList() {
        
        return array(
            "collections" => $this->collections,
            "modules" => $this->find('list', array(
                'fields' => array('Module.model', 'Module.name'),
                'order' => array('Module.name ASC')
            ))
        );
        
    }
    
}