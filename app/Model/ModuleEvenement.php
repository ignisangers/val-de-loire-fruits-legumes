<?php
/**
 * Description of ModuleFile
 *
 * @author meowcate
 */
class ModuleEvenement extends AppModel {
    
    public $actsAs = array(
        'Containable',
    );
    
    public $belongsTo = array('Content');
}
