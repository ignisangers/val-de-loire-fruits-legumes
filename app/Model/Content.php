<?php
class Content extends AppModel {
    
    public $actsAs = array('Containable');
    public $belongsTo = array('Page');
    // Appeler une fonction listant les modules activés, plutôt que de le faire manuellement ?
    public $hasMany = array(
        'ModuleActualite' => array(
            'dependent' => true
        ),
        'ModuleArticle' => array(
            'dependent' => true
        ),
        'ModuleProduit' => array(
            'dependent' => true
        ),
        'ModuleRecette' => array(
            'dependent' => true
        ),
        'ModuleEvenement' => array(
            'dependent' => true
        ),
        'ModuleFile' => array(
            'dependent' => true
        ),
        'ModulePicture' => array(
            'dependent' => true
        ),
        'ModuleGallery' => array(
            'dependent' => true
        ),
        'ModuleGroupe' => array(
            'dependent' => true
        ),
        'ModuleHtml' => array(
            'dependent' => true
        ),
        'ModuleLien' => array(
            'dependent' => true
        ),
        'ModuleTrombinoscope' => array(
            'dependent' => true
        ),
    );
    
    /**
     * Récupération des modules d'une page, organisés par zone
     * @param int $page_id
     * @param string $for : formatage des données différent entre frontoffice et backoffice (on a besoin de plus de données en backoffice)
     */
    public function getModulesByPage($page_id, $for = "site", $type = null) {
        
        // On récupère d'abord les contenus de la page (zones + ID des modules)
        $contents = $this->find('all', array(
            'recursive' => -1,
            'contain' => false,
            'conditions' => array(
                'Content.page_id' => $page_id
            ),
            'order' => array('Content.zone ASC', 'Content.rang ASC')
        ));
        
        // Puis on récupère les modules et on réorganise tout ça par zone
        $modules = array();
        foreach ($contents as $c):
            $z = $c['Content']['zone'];
            $r = $c['Content']['rang'];
            $m = $c['Content']['model'];
            
            // On récupère manuellement le contenu de chaque module, sinon CakePHP ne livre pas les traductions...
            // Chaque module DOIT disposer d'une fonction getModule() dans son Model
            if (!method_exists($this->{$m}, "getModule")) {
                // Si la méthode n'existe pas, on renvoie une exception (au moins c'est clair)
                throw new MethodNotAllowedException("Impossible de récupérer un module (méthode \"getModule()\" absente du Model \"". $m ."\")");
            }
            $module_content = $this->{$m}->getModule($c['Content']['id'],$type);
            
            // Données de base à retourner pour le site et l'admin
            $module = array(
                "model" => Inflector::tableize($m), // Model du module
                "content" => (!empty($module_content)) ? $module_content : array() // Contenu du module
            );
            if ($for === "admin") { // Données à retourner pour l'admin spécifiquement
                $module["id"] =  $c['Content']['id'];
                $module["page_id"] =  $page_id;
                $module["zone"] = $z;
            }
            $modules[$z][$r] = $module;
        endforeach;
        
        return $modules;
        
    }
    
    /**
     * On met à jour l'ordre des modules (on décale tout un cran vers le bas), pour une zone d'une page
     * @param int $page_id
     * @param int $zone
     * @param int $new_id
     */
    public function updateOrder($page_id, $zone, $new_id) {
        
        $this->updateAll(array(
            'Content.rang' => 'rang + 1'
        ), array(
            'Content.page_id' => $page_id,
            'Content.zone' => $zone,
            'Content.id !=' => $new_id
        ));
        
    }
}