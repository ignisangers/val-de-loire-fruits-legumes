<?php
class Partner extends AppModel {

    public $order = 'Partner.rang ASC';
    public $validate = array(
        'name' => array(
            'rule' => 'notEmpty',
            'required' => true,
            'message' => 'Vous devez saisir le nom du partenaire.'
        ),
    );
    public $actsAs = array(
        'Uploader.Attachment' => array(
            'logo' => array(
                'dbColumn' => 'logo',
                'uploadDir' => 'files/partners/',
                'overwrite' => true,
                'saveAsFilename' => true,
                'allowEmpty' => true,
                'stopSave' => true,
                'transforms' => array(
//                    array(
//                        'method' => 'resize',
//                        'width' => 300,
//                        'heigth' => 200,
//                        'overwrite' => true,
//                        'prepend' => false,
//                        'append' => false,
//                        'dbColumn' => 'logo'
//                    )
                )
            ),
            'fichier' => array(
                'dbColumn' => 'fichier',
                'uploadDir' => 'files/partners/',
                'overwrite' => true,
                'saveAsFilename' => true,
                'allowEmpty' => true,
                'stopSave' => true,
//                'transforms' => array(
//                    array(
//                        'method' => 'resize',
//                        'width' => 200,
//                        'overwrite' => true,
//                        'prepend' => false,
//                        'append' => false,
//                        'dbColumn' => 'fichier'
//                    )
//                )
            ),
        ),
        'Uploader.FileValidation' => array(
            'logo' => array(
                'required' => array(
                    'on' => 'create',
                    'allowEmpty' => false
                )
            ),
            'fichier' => array(
                'required' => array(
                    'value' => false,
                    'on' => 'create',
                    'allowEmpty' => true
                )
            )
        ),
    );

    public function beforeValidate($options = array()) {
        parent::beforeValidate($options);

        // Si le champ URL a été laissé "vide" (on avait mis par défaut "http://"), on supprime toute valeur
        if ($this->data['Partner']['url'] === 'http://') {
            $this->data['Partner']['url'] = null;
        }

        return true;
    }

    public function afterSave($created, $options = array()) {
        parent::afterSave($created, $options);

        // On vient de créer un partenaire : on va décaler les partenaires pour qu'il se retrouve en premier
        if ($created) {
            $this->updateAll(array(
                'Partner.rang' => 'rang + 1'
            ));
        }

    }

    /**
     * On bouge un partenaire vers le haut
     * @param int $id
     * @param int $rang
     * @param int $delta
     */
    public function moveUp($id, $rang, $delta) {

        // On bouge le précédent vers le bas
        $this->updateAll(array(
            'Partner.rang' => $rang
        ), array(
            'Partner.rang' => $rang - $delta
        ));

        // On bouge le partenaire voulu vers le haut
        $this->id = $id;
        $this->saveField('rang', $rang - $delta);

        return true;

    }

    /**
     * On bouge un partenaire vers le bas
     * @param int $id
     * @param int $rang
     * @param int $delta
     */
    public function moveDown($id, $rang, $delta) {

        // On bouge le suivant vers le haut
        $this->updateAll(array(
            'Partner.rang' => $rang
        ), array(
            'Partner.rang' => $rang + $delta
        ));

        // On bouge le partenaire voulu vers le bas
        $this->id = $id;
        $this->saveField('rang', $rang + $delta);

        return true;

    }

    /**
     * On veut récupérer la liste des partenaires pour le footer du frontoffice
     */
    public function findForFooter() {

        return $this->find('all', array(
            'fields' => array(
                'name', 'logo', 'url'
            )
        ));

    }

}
