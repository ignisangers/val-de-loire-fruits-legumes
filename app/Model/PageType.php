<?php
class PageType extends AppModel {
    
    /**
     * Liste des types de contenus
     * On retourne aussi le type "modules", pour savoir si on affiche ou non le choix d'un template (réservé aux modules)
     * @return array
     */
    public function getList() {
        
        $types = $this->find("all");
        
        // ID du type "modules", à définir dans la boucle
        $typeModule = null;
        
        // Liste des types de contenu
        $typesList = array();        
        foreach ($types as $type):
            // C'est le type "modules"
            if ($type["PageType"]["slug"] === "modules") {
                $typeModule = $type["PageType"]["id"];
            }
            // On compose la liste des types disponibles
            $typesList[$type["PageType"]["id"]] = $type["PageType"]["name"];
        endforeach;
        
        return array(
            "typeModule" => $typeModule,
            "list" => $typesList
        );
        
    }
    
    /**
     * Quel est l'ID du type "modules" ?
     */
    public function getModuleType() {
        
        return $this->field("id", array(
            "slug" => "modules"
        ));
        
    }
    
}