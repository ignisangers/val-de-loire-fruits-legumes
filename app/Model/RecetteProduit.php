<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RecettesProduit
 *
 * @author carlos-ignis
 */
class RecetteProduit extends AppModel {

    public $hasMany = array(
//        'Recette', 'Produit'
    );
    
    /**
     * liaison entre produit et recette
     * 
     * @param int $recetteId
     * @param array $produitIds
     */
    public function addRecetteProduits($recetteId, $produitIds) {
        //suppression des anciennes associations de le recette pour en recréer des nouvelles
        $this->deleteAll(array(
            'RecetteProduit.recette_id' => $recetteId,
            false
        ));
        //enregistrement des nouvelles associations suite à la soumission du formulaire
        if (!empty($produitIds)) {
            foreach ($produitIds as $produitId) {
                if ($produitId != 0) {
                    $this->create();
                    $this->save(array(
                        'recette_id' => $recetteId,
                        'produit_id' => $produitId,
                    ));
                }
            }
        }
    }

}
