<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of Image
 *
 * @author meowcate
 */
class Image extends AppModel {
    
    
    /**
     * Upload une image et retourne son emplacement
     * @param string $dossier Dossier où placer l'image
     * @param string $image Contenu de $_FILE['file']
     * @return string|bool Emplacement de l'image, false en cas d'erreur
     */
    public function uploadImage($dossier, $image) {
        
        // files storage folder
        $dir = 'uploads'.DS.$dossier.DS;
        $image['type'] = strtolower($image['type']);

        $tmp_name = $image["tmp_name"];
        $name = $image["name"];

        if (!is_dir(IMAGES.$dir)) {
            mkdir(IMAGES.$dir, 0777);
        }
        
        if ($image['type'] == 'image/png' || $image['type'] == 'image/jpg' || $image['type'] == 'image/gif' || $image['type'] == 'image/jpeg' || $image['type'] == 'image/pjpeg') {

            // Le fichier existe déjà, on le renomme
            if (file_exists(IMAGES.$dir . $name)) {
                $numfic = 1;
                $position_p = strrpos($name, ".");
                $longueur = strlen($name);
                $name_base = substr($name, 0, $position_p);
                $ext = substr($name, $position_p, $longueur - $position_p);

                $name = $name_base . "_" . $numfic . $ext;

                while (file_exists(IMAGES.$dir . $name) && $numfic < 1000) {
                    $numfic++;
                    $name = $name_base . "_" . $numfic . $ext;
                }
            }

            // copying
            move_uploaded_file($tmp_name, IMAGES.$dir . $name);
            
            return $dir . $name;
        }
        return '';
    }
}
