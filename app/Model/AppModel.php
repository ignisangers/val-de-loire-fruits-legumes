<?php

/**
 * Application model for Cake.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {

    public function afterFind($results, $primary = false) {

        // On va récupérer la bonne traduction parmi les résultats finaux
        if (!empty($results) && $primary) {
            $results = $this->_recursiveTranslations($results);
        }

        return $results;
    }

    /**
     * Récupération d'un module d'une page
     * 
     * Note : méthode utilisée par défaut pour tous les modules.
     * Chaque module peut disposer de sa propre méthode getModule() si le fonctionnement diffère
     * (pour inclure des données supplémentaires par exemple)
     * 
     * @param int $content_id
     * @return array
     */
    public function getModule($content_id, $type) {

        $this->recursive = -1;
        return $this->findByContentId($content_id, $type);
    }

    /**
     * 
     * @param int $nb : nombre de liens à générer
     * @return array
     */
    public function setPaginationLinks($nb) {

        // URL de la page actuelle
        $router = Router::getParams();

        // On prépère les liens en reprenant comme modèle l'URL de la page actuelle (on remplacera juste la page)
        $url = array(
            "controller" => $router["controller"],
            "action" => $router["action"]
        );
        $args = array_merge($router["pass"], $router["named"]);

        // Composition des liens
        $links = array();
        for ($i = 1; $i <= $nb; $i++):
            // On indique la page dans l'URL
            $args["page"] = $i;
            $href = array_merge($url, $args);
            // On ajoute le lien à la pagination
            $links[$i] = $href;
        endfor;

        return $links;
    }

    /* public function afterSave($created) {

      // Cas des modèles utilisant Translate, dont il faut enregistrer les différentes versions autre que la langue sélectionnée
      // Ex. : on crée un article en fr, on va donc créer les données à traduire dans les autres langues dispos (anglais, etc)
      if($created && is_array($this->actsAs) && array_key_exists('Translate', $this->actsAs)) {
      // On enregistre chaque traduction par défaut
      if(array_key_exists('Uploader.Attachment', $this->actsAs)) {
      $this->Behaviors->unload('Uploader.Attachment');
      }
      foreach(Configure::read('Config.languages') as $code => $language):
      if($code != Configure::read('Config.language')) {
      $this->locale = $code;
      $this->save($this->data[$this->alias], false);
      }
      endforeach;
      }

      return true;

      } */

    /**
     * Fonction récursive pour récupérer les traductions et les écraser (Cake ne le fait pas systématiquement...)
     * @param array $results
     * @return array
     */
    private function _recursiveTranslations($results) {

        foreach ($results as $model => $datas):
            if (is_int($model)) {
                $results[$model] = $this->_recursiveTranslations($datas);
            } else if (preg_match('#Translations#', $model)) {
                $fieldTranslated = str_replace('Translations', '', $model);
                foreach ($datas as $keyLang => $valueLang):
                    if ($valueLang['locale'] == Configure::read('Config.language')) {
                        if (!isset($results[$valueLang['model']])) {
                            $results[$fieldTranslated] = $valueLang['content'];
                        } else {
                            $results[$valueLang['model']][$fieldTranslated] = $valueLang['content'];
                        }
                        break;
                    }
                endforeach;
            } else if (is_array($datas)) {
                foreach ($datas as $field => $values):
                    if (is_int($field)) {
                        $results[$model][$field] = $this->_recursiveTranslations($values);
                    }
                endforeach;
            }
        endforeach;

        return $results;
    }

}
