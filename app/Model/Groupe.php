<?php
/**
 * Description of Groupe
 *
 * @author meowcate
 */

class Groupe extends AppModel {
    
    public $belongsTo = array(
        'ModuleGroupe' => array(
            'className' => 'ModuleGroupe',
            'foreignKey' => 'module_groupe_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    
    public $actsAs = array(
        'Containable',
    );
    
    /*
     * Enregistrement de la photo si présente
     */
    public function beforeSave($options = array()) {
        
        parent::beforeSave($options);
        if (!empty($this->data['Groupe']['photo'])) {
            App::import('Model', 'Image');
            $Image = new Image();
            $image = $Image->uploadImage('groupes', $this->data['Groupe']['photo']);
            $this->data['Groupe']['photo'] = $image;
        }
        return true;
    }
    
    /**
     * Suppression de la photo contenue dans le modèle
     * @param type $cascade
     * @return boolean Pas de suppression si la photo ne peut être retirée
     */
    public function beforeDelete($cascade = true) {
        parent::beforeDelete($cascade);
        
        $fichier = $this->field('photo');
        if (is_file($fichier) && unlink($fichier)) {
            // photo supprimée
            return true;
        } else if (!is_file($photo)) {
            // photo inexistante (ou il s'agit d'un dossier)
            return true;
        }
        // photo existante impossible à supprimer
        return false;
    }
}
