<?php

class ModuleRecette extends AppModel {

    public $belongsTo = array('Content');
    public $recursive = -1;

    /**
     * Override de la méthode dans AppModel
     *
     * On va récupérer les informations du module + les actualités correspondant à ces informations
     * 
     * @param int $content_id
     * @return array
     */
    public function getModule($content_id, $type) {

        // On récupère le module en lui-même d'abord
        $module = $this->findByContentId($content_id);

        // On le complète avec une liste d'actualités correspondantes
        // Si on transmet un paramètre de pagination et qu'on a imposé un maximum à afficher, on crée une pagination
        $urlArgs = Router::getParam("named");
        $page = (isset($urlArgs["page"])) ? $urlArgs["page"] : 1;

        // Récupération des recettes
        $this->Recette = ClassRegistry::init('Recette');
        $conditions = array();
        if (!empty($type)) {
            $conditions = array('type' => $type);
        }
        $recettes["Recettes"] = $this->Recette->find("all", array(
            "conditions" => $conditions,
            "order" => array(
                "Recette.name ASC"
            ),
            "limit" => $module["ModuleRecette"]["afficher_nb"],
            "page" => $page
        ));

        // Création d'une pagination (liste de liens) ?
        $pagination = array();
        $newsLink = array();
        if ($module["ModuleRecette"]["pagination"]) {
            $pagination["Pagination"] = $this->_setPagination($page, $module["ModuleRecette"]["afficher_nb"]);
        } else {
            // Si pas de pagination, on prévoit un lien vers la page Actualités (si elle existe)
            $pageNews = $this->Content->Page->getInfo($this->Recette->pageConditions);
            if (!empty($pageNews)) {
                $newsLink["hrefPageNews"] = array(
                    "controller" => $pageNews["PageType"]["controller"],
                    "action" => $pageNews["PageType"]["action"],
                );
            }
        }

        return array_merge($module, $recettes, $pagination, $newsLink);
    }

    /**
     * Préparation d'une pagination pour un module actualités
     * On utilisera une fonction de AppModel pour composer les liens
     * @param int $currentPage
     * @param int $maxPages
     * @return array
     */
    private function _setPagination($currentPage, $maxPages) {

        // Combien de liens faut-il générer ?
        $nbRecettes = $this->Recette->find("count");
        $nbLinks = ceil($nbRecettes / $maxPages);

        return array(
            "current" => $currentPage * 1,
            "links" => $this->setPaginationLinks($nbLinks) // Fonction AppModel
        );
    }

}
