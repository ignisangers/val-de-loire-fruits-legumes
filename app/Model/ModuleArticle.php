<?php
class ModuleArticle extends AppModel {
    
    public $belongsTo = array('Content');
    public $actsAs = array(
        'Translate' => array(
            'name' => 'nameTranslations',
            'resume' => 'resumeTranslations',
            'content' => 'contentTranslations'
        )
    );
    
}