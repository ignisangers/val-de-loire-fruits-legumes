<?php
class VignetteProduit extends AppModel {
    
//    public $order = array("VignetteProduit.rang" => "ASC");
    public $recursive = -1;
    public $path;
    
    public function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        
        $this->path = WWW_ROOT . 'files' . DS . 'produits/vignette' . DS;
    }
    
    /**
     * Liste des images d'une galerie
     * @param int $id
     * @return array
     */
    public function getVignetteProduit($id) {
        
        return $this->find("all", array(
            "conditions" => array(
                "VignetteProduit.produit_id" => $id
            ),
            "fields" => array(
                "VignetteProduit.id", "VignetteProduit.filename"
            )
        ));
        
    }
    
    /**
     * Mise à jour de l'ordre des images d'une galerie après ajout d'une nouvelle image
     * @param int $gallery_id : ID du module galerie
     * @param int $id : ID de l'image créée
     */
    public function orderAfterCreate($gallery_id, $id) {
        $this->updateAll(array(
//            "VignetteProduit.rang" => "rang + 1"
        ), array(            
            "VignetteProduit.produit_id" => $gallery_id,
            "NOT" => array(
                "VignetteProduit.id" => $id
            ),
        ));
    }
    
    /**
     * On va réhausser d'un cran les images qui suivaient celle qu'on vient de supprimer
     * @param int $id : ID du module galerie contenant l'image supprimée
     * @param int $rang : rang de l'image supprimée
     */
    public function orderAfterDelete($id, $rang) {
        $this->updateAll(array(
//            "VignetteProduit.rang" => "rang - 1"
        ), array(
            "VignetteProduit.produit_id" => $id,
//            "VignetteProduit.rang >" => $rang
        ));
    }
    
}