<?php
class ModuleGallery extends AppModel {
    
    public $belongsTo = array('Content');
    public $hasMany = array('Gallery');
    public $recursive = -1;
    
    /**
     * Override de la méthode dans AppModel
     *
     * On va récupérer les informations du module + les images correspondant à ces informations
     * 
     * @param int $content_id
     * @return array
     */
    public function getModule($content_id) {
        
        // On récupère le module en lui-même d'abord
        $module = $this->findByContentId($content_id);
        
        // On le complète avec une liste d'images correspondantes
        $actualites["Galleries"] = $this->Gallery->find("all", array(
            "conditions" => array(
                "Gallery.module_gallery_id" => $module["ModuleGallery"]["id"]
            )
        ));
        
        return array_merge($module, $actualites);
        
    }
    
}