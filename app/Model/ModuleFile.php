<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of ModuleFile
 *
 * @author meowcate
 */
class ModuleFile extends AppModel {
    
    public $actsAs = array(
        'Containable',
    );
    
    public $belongsTo = array('Content');
    
    public $hasMany = array(
        'File' => array(
            'className' => 'File',
            'foreignKey' => 'module_file_id',
            'dependent' => false,
        )
    );
    
    
    /**
     * Suppression préalable de tous les Files dépendants de ce module
     * @param type $cascade
     */
    public function beforeDelete($cascade = true) {
        
        parent::beforeDelete($cascade);
        $this->File->deleteAll(array('File.module_file_id' => $this->id));
    }
    
    /**
     * Override de la méthode dans AppModel
     *
     * On va récupérer les informations du module + les fichiers correspondant à ces informations
     * 
     * @param int $content_id
     * @return array
     */
    public function getModule($content_id, $type) {
        
        // On récupère le module en lui-même d'abord
        $module = $this->findByContentId($content_id);
        
        // On le complète avec une liste des fichiers correspondants
        $this->File = ClassRegistry::init('File');
        $files["Files"] = $this->File->find("all", array(
            "conditions" => array(
                "File.module_file_id" => $module["ModuleFile"]["id"],
            ),
            "order" => array(
                "File.rang ASC"
            ),
        ));
        return array_merge($module, $files);
    }
}
