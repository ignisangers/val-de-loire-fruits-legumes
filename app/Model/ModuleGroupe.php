<?php

/**
 * Description of ModuleGroupe
 *
 * @author meowcate
 */
class ModuleGroupe extends AppModel {
    
    public $actsAs = array(
        'Containable',
    );
    
    public $belongsTo = array('Content');
    
    public $hasMany = array(
        'Groupe' => array(
            'className' => 'Groupe',
            'foreignKey' => 'module_groupe_id',
            'dependent' => false,
        )
    );
}
