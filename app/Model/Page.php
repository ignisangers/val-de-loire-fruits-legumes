<?php
class Page extends AppModel {
    
    public $actsAs = array(
        'Tree',
        'Containable',
        'Copyable',
        'Translate' => array(
            'name' => 'nameTranslations',
            'alias' => 'aliasTranslations',
            'meta_title' => 'meta_titleTranslations',
            'meta_description' => 'meta_descriptionTranslations'
        )
    );
    public $belongsTo = array('Template', 'PageType');
    public $hasMany = array(
        'Content' => array(
            'dependent' => true
        )
    );
    
    public $validate = array(
        'name' => array(
            'rule' => 'isUnique',
            'allowEmpty' => false,
            'required' => true,
            'message' => 'Ce titre existe déjà pour une autre page.'
        ),
        'alias' => array(
            'rule' => 'isUnique',
            'allowEmpty' => false,
            'required' => true,
            'message' => 'Cette URL existe déjà pour une autre page.'
        ),
        'meta_title' => array(
            'rule' => 'isUnique',
            'allowEmpty' => false,
            'required' => true,
            'message' => 'Un titre (référencement) similaire existe déjà. Vous devez en choisir un autre.'
        )
    );
    
    /**
     * Préparation de l'affichage d'une page :
     * - construction du menu principal
     * - construction du menu annexe
     * - mise en place des balises meta
     * - construction du fil d'ariane
     */
    public function construct($conditions = array(), $title = null, $description = null) {
        
        // Informations permettant de construire la page à partir des fonctions qui suivent
        $page = $this->getInfo($conditions);
        
        // Balises métas
        $metas = array(
            "title" => (!empty($title)) ? $title : $page['Page']['meta_title'],
            "description" => (!empty($description)) ? $description : $page['Page']['meta_description']
        );

        // Menus
        $menus = $this->setMenu($page['Page']['id']);
        
        // Menus annexes
        $annexes = $this->setMenuSecondary();

        // Fil d'ariane
        $breadcrumb = $this->setBreadcrumb($page['Page']['id']);        

        // Page dans les autres langues
        $versions = $this->_formatVersions($page['aliasTranslations']);
        unset($page['aliasTranslations']);        
        
        return array(
            "page" => $page,
            "metas" => $metas,
            "menus" => $menus,
            "annexes" => $annexes,
            "breadcrumb" => $breadcrumb,            
            "versions" => $versions
        );
    }
    
    /**
     * Récupération des informations essentielles de la page
     * @param array $conditions
     * @return array
     */
    public function getInfo($conditions = array()) {
        
        // Conditions par défaut : celle de la page d'accueil
        if (empty($conditions)) {
            $conditions = array(
                'Page.home' => 1,
                'Page.active' => 1
            );
        }
        
        $this->unbindModel(array('hasMany' => array('Content')));
        return $this->find('first', array(
            'recursive' => 1,
            'conditions' => $conditions,
            'fields' => array(
                'Page.id', 'Page.parent_id', 'Page.meta_title', 'Page.meta_description', 'Page.alias', 'Page.home',
                'PageType.*',
                'Template.name'
            )
        ));
        
    }
    
    /**
     * SITE
     * Construction du menu
     * @param int $active : page courante (pas forcément celle à activer)
     * @return array
     */
    public function setMenu($active = 0) {

        // Récupération des pages du menu dans un tableau récursif
        $pages = $this->find('threaded', array(
            'recursive' => 0,
            'conditions' => array(
                'Page.active' => 1,
                'Page.menu' => 1
            ),
            'fields' => array(
                'Page.name', 'Page.alias', 'Page.parent_id', 'Page.home',
                'PageType.slug', 'PageType.controller', 'PageType.action'
            ),
            'order' => array('Page.lft ASC')
        ));

        // Page à activer dans le menu (si la page courante n'est pas au plus haut niveau)
        $filAriane = $this->getPath($active, array('Page.id', 'Page.alias', 'Page.name'), -1);
        if (!empty($filAriane)) {
            $active = $filAriane[0]['Page']['id']; // Niveau le plus haut
        }

        return array(
            "pages" => $pages,
            "pageActive" => $active
        );
    }
    
    /**
     * SITE
     * Construction du menu annexe
     * On va récupérer :
     * - la liste des pages non inclues dans le menu principal
     * - une liste de pages que l'on souhaite faire paraître à nouveau dans ce menu (typiquement la page contact)
     * @return array
     */
    public function setMenuSecondary() {
        
        return $this->find('all', array(
            'recursive' => 0,
            'conditions' => array(
                'OR' => array(
                    // Pages non inclues dans le menu
                    array(
                        'Page.menu' => 0,
                        'Page.active' => 1
                    ),
                    // Pages du menu qu'on veut faire apparaître aussi dans le menu annexe
                    array(
                        'Page.alias' => "contact"
                    )
                )
            ),
            'order' => array('Page.lft ASC'),
            'fields' => array('Page.alias', 'Page.name', 'PageType.*', 'Page.home')
        ));
        
    }
    
    /**
     * SITE
     * Construction du fil d'ariane d'après la page courante
     * @param int $id : ID de la page courante
     * @return array
     */
    public function setBreadcrumb($id = null) {
        
        return $this->getPath($id, array("Page.*", "PageType.slug", "PageType.controller", "PageType.action"), 0);
        
    }
    
    /**
     * Petit formatage des traductions pour les URL de changement de langue
     * @param array $translations
     * @return array
     */
    private function _formatVersions($translations) {

        $versions = array();
        foreach ($translations as $translation):
            $versions[$translation['locale']] = $translation;
        endforeach;

        return $versions;
    }

    /**
     * Liste des sous-pages directes d'une page parente
     * @param int $parent_id
     * @return array
     */
    public function getChildren($parent_id) {
        return $this->find('all', array(
            'recursive' => 0,
            'fields' => array(
                'Page.id', 'Page.parent_id', 'Page.alias', 'Page.name', 'Page.home',
                'PageType.slug', 'PageType.controller', 'PageType.action'
            ),
            'conditions' => array(
                'Page.active' => 1,
                'Page.parent_id' => $parent_id
            ),
            'order' => array('Page.lft ASC')
        ));
    }
    
}