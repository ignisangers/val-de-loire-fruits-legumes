<?php
/**
 * Description of File
 *
 * @author meowcate
 */

class File extends AppModel {
    
    public $belongsTo = array(
        'ModuleFile' => array(
            'className' => 'ModuleFile',
            'foreignKey' => 'module_file_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    public $recursive = -1;
    
    /*
     * Enregistrement du fichier
     */
    public function beforeSave($options = array()) {
        
        parent::beforeSave($options);
        
        if (!empty($this->data['File']['fichier'])) {
            $fichier = $this->uploadFile($this->data['File']['fichier']);
            $this->data['File']['fichier'] = $fichier;
        }
        return true;
    }
    
    
    /**
     * Suppression du fichier contenu dans le modèle
     * @param type $cascade
     * @return boolean Pas de suppression si le fichier ne peut être retiré
     */
    public function beforeDelete($cascade = true) {
        parent::beforeDelete($cascade);
        
        $fichier = $this->field('fichier');
        if (is_file($fichier) && unlink($fichier)) {
            // fichier supprimé
            return true;
        } else if (!is_file($fichier)) {
            // fichier inexistant (ou il s'agit d'un dossier)
            return true;
        }
        // fichier existant impossible à supprimer
        return false;
    }
    
    /**
     * Upload un fichier et retourne son emplacement
     * @param string $file Contenu de $_FILE['file']
     * @return string|bool Emplacement du fichier, false en cas d'erreur
     */
    public function uploadFile($file) {
        
        // files storage folder
        $dir = 'files'.DS.'uploads'.DS;

        $tmp_name = $file["tmp_name"];
        $name = $file["name"];

        if (!is_dir($dir)) {
            mkdir($dir, 0777);
        }
        
        // Le fichier existe déjà, on le renomme
        if (file_exists($dir . $name)) {
            $numfic = 1;
            $position_p = strrpos($name, ".");
            $longueur = strlen($name);
            $name_base = substr($name, 0, $position_p);
            $ext = substr($name, $position_p, $longueur - $position_p);

            $name = $name_base . "_" . $numfic . $ext;

            while (file_exists($dir . $name) && $numfic < 1000) {
                $numfic++;
                $name = $name_base . "_" . $numfic . $ext;
            }
        }
        // copying
        move_uploaded_file($tmp_name, $dir . $name);

        return $dir . $name;
    }
}
