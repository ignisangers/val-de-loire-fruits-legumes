<?php
/**
 * Description of Template
 *
 * @author meowcate
 */
class Template extends AppModel {

    public $uses = array('Page');

    /**
     * Traitement template "interieur-enfants.ctp"
     * Affichage de l'arborescence d'une page parente
     * @param array $page : informations de la page
     */
    public function template_interieur_enfants($page) {
        $this->Page = ClassRegistry::init('Page');
        $children = $this->Page->getChildren($page['Page']['parent_id']);
        return array('children' => $children);
    }

    /**
     * Traitement template "interieur-parent.ctp"
     * Affichage de l'arborescence des pages enfants directes
     * @param array $page : informations de la page
     */
    public function template_interieur_parent($page) {
        $this->Page = ClassRegistry::init('Page');
        $children = $this->Page->getChildren($page['Page']['id']);
        return array('children' => $children);
    }
}
