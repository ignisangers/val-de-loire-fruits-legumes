<?php
/**
 * Description of Actualite
 *
 * @author meowcate
 */
class Actualite extends AppModel {
        
    // Conditions pour la construction des pages actualités (menu, fil d'ariane...)
    // Communes à la page listant les actus et chaque page actu en détail
    public $pageConditions = array(
        'PageType.slug' => 'news',
        'Page.active' => 1
    );    
    public $validate = array(
        'name' => array(
            'rule' => 'isUnique',
            'allowEmpty' => false,
            'required' => true,
            'message' => 'Ce titre existe déjà pour une autre page.'
        ),
        'content' => array(
            'rule' => 'notEmpty',
            'message' => 'Vous devez saisir une actualité.'
        )
    );
    public $recursive = -1;
    
}
