<?php
/**
 * Description of Produit
 *
 * @author carlos
 */
class Produit extends AppModel {
        
    // Conditions pour la construction des pages produits (menu, fil d'ariane...)
    // Communes à la page listant les produits et chaque page produit en détail
    public $pageConditions = array(
        'PageType.slug' => 'produits',
        'Page.active' => 1
    );    
    
    public $hasMany = array(
//        'RecetteProduit' => array(
//            'className' => 'RecetteProduit',
//            'dependent' => true
//        ),
        'GallerieProduit' => array(
            'className' => 'GallerieProduit',
            'dependent' => true
        ),
        'VignetteProduit' => array(
            'className' => 'VignetteProduit',
            'dependent' => true
        )       
        
    );
    public $belongsToMany = array(
        'RecetteProduit' => array(
            'joinTable' => 'recette_produits',
            'className' => 'RecetteProduit',
            'foreignKey' => 'produit_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    
    public $validate = array(
//        'name' => array(
//            'rule' => 'isUnique',
//            'allowEmpty' => false,
//            'required' => true,
//            'message' => 'Ce titre existe déjà pour une autre page.'
//        ),
//        'content' => array(
//            'rule' => 'notEmpty',
//            'message' => 'Vous devez saisir un produit.'
//        )
    );
    public $recursive = -1;
    
}
