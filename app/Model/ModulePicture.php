<?php
class ModulePicture extends AppModel {
    
    public $belongsTo = array('Content');
    public $actsAs = array(
        'Uploader.Attachment' => array(
            'image' => array(
                'dbColumn' => 'image',
                'uploadDir' => 'medias/',
                'overwrite' => true,
                'saveAsFilename' => true,
                'allowEmpty' => true,
                'stopSave' => true,
                /*'transforms' => array(
                    array(
                        'method' => 'crop',
                        'height' => 370,
                        //'quality' => 100,
                        'overwrite' => true,
                        'prepend' => false,
                        'append' => false,
                        'dbColumn' => 'image'
                    )
                )*/
            )
        ),
        'Uploader.FileValidation' => array(
            'image' => array(
                'required' => array(
                    'on' => 'create',
                    'allowEmpty' => false
                )
            )
        ),
        'Translate' => array(
            'name' => 'nameTranslations',
            'caption' => 'captionTranslations',
            'content' => 'contentTranslations'
        )
    );
    
    /**
     * ADMIN
     * À la création d'un module, on crée toutes les traductions
     * @param int $id
     * @param array $datas
     * @return boolean
     */
    /*public function afterSave($created) {
        parent::afterSave($created);
        
        if($created) {
            // On enregistre chaque traduction par défaut
            unset($this->data['ModulePicture']['image']);
            foreach(Configure::read('Config.languages') as $code => $language):
                if($code != Configure::read('Config.language')) {
                    $this->locale = $code;
                    $this->save($this->data['ModulePicture']);
                }
            endforeach;
        }
        
        return true;
    }*/
    
}
?>