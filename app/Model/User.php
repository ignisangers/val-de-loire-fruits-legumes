<?php
App::uses('AuthComponent', 'Controller/Component');
class User extends AppModel {
    
    public $validate = array(
        "nom" => array(
            "rule" => array("notEmpty"),
            "message" => "Vous devez indiquer un nom de famille"
        ),
        "prenom" => array(
            "rule" => array("notEmpty"),
            "message" => "Vous devez indiquer un prénom"
        ),
        "username" => array(
            "rule" => array("notEmpty"),
            "message" => "Vous devez indiquer un pseudonyme de connexion"
        ),
        "password" => array(
            "creation" => array(
                "rule" => array("notEmpty"),
                "message" => "Vous devez choisir un mot de passe",
                "on" => "create"
            )
        )
    );
    public $actsAs = array(
        'Uploader.Attachment' => array(
            'avatar' => array(
                'dbColumn' => 'avatar',
                'uploadDir' => 'img/avatars/',
                'overwrite' => true,
                'saveAsFilename' => true,
                'allowEmpty' => true,
                'stopSave' => true,
                'transforms' => array(
                    array(
                        'method' => 'crop',
                        'width' => 71,
                        'height' => 71,
                        'overwrite' => true,
                        'prepend' => false,
                        'append' => false,
                        'dbColumn' => 'avatar'
                    )
                )
            )
        )/*,
        'Uploader.FileValidation' => array(
            'image' => array(
                'required' => array(
                    'on' => 'create',
                    'allowEmpty' => false
                )
            )
        )*/,
        'Copyable'
    );
    
    public function beforeSave($options = array()) {
        parent::beforeSave($options);
        
        if(isset($this->data[$this->alias]['password'])) {
            $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
        }
        return true;
    }
    
}
?>