/**
 * Module de "Crop" d'une image
 * 
 * Dépendances :
 * - jQuery
 * - jCrop
 * - Ignis.Upload (pour la méthode readFromUpload())
 * 
 * Procédure :
 * - On fournit une image à rogner/tronquer
 * - On récupère la configuration pour jCrop
 * - On initialise jCrop
 * - On avertit l'application que jCrop est prêt (pour ouvrir une modale par exemple)
 * 
 * @author Vincent <vincent@ignis.fr>
 */
var Ignis = Ignis || {};

Ignis.Crop = (function () {
    'use strict';
    /*global jsVars, $*/

    // VARIABLES
    var img,
            imgToCrop,
            validation,
            bounds = {},
            canvas,
            context,
            settings,
            defaults = {
                "cropImageWidth": 200, // Largeur de l'image finale rognée
                "aspectRatio": 1, // Ratio : calcul de la hauteur en fonction de la largeur
                "imgElement": ".crop-image", // Conteneur où afficher la gestion du Crop de l'image
                "hiddenFile": ".crop-hidden", // Input hidden où stocker la base64 finale pour l'enregistrement
                "qualityJpeg": 0.85, // Dégradation de l'image finale (pour optimiser le poids)
                "bgColor": "black",
                "bgOpacity": .5,
                "bgFade": true,
                "trackDocument": true,
                "allowResize": true,
                "allowSelect": true,
                "minSize": 5,
                "boxWidth": 700,
                "boxHeight": 700,
                "selMinSize": [50, 50],
                "setSelect": [0, 0, 100, 100],
                "maxWidthPreview": 530,
                "maxHeightPreview": false
            };

    // MÉTHODES PRIVÉES
    function validate() {
        var match = img["raw"].match(/^data:image\/(.+);/);
        if (match === null || $.inArray(match[1], ["jpeg", "jpg", "png", "gif"]) === -1) {
            return "Le fichier doit être une image valide (jpeg, jpg, png ou gif).";
        }

        return true;
    }

    // MÉTHODES PUBLIQUES
    function initialize(file, type) {
        // On stocke le fichier
        img = file;
        
        // On récupère la configuration        
        if (typeof jsVars.cropConfig["default"] !== "undefined") {
            if ((type === "vignette_produits" || type === "vignette_recettes")) {
                settings = $.extend({}, defaults, jsVars.cropConfig["vignette"]);
            } else {
                settings = $.extend({}, defaults, jsVars.cropConfig["default"]);
            }
        } else {
            settings = defaults;
        }

        // Vérifications sur le fichier
        validation = validate();
        if (validation !== true) {
            alert(validation);
            return;
        }

        // Création dans le DOM de l'image à partir de la base64
        imgToCrop = document.createElement('img');
        imgToCrop.src = img.raw;
        imgToCrop.className = 'imgToCrop';
        $(settings.imgElement).html(imgToCrop);

        $(imgToCrop).Jcrop({
            bgOpacity: settings.bgOpacity,
            trackDocument: settings.trackDocument,
            allowResize: settings.allowResize,
            allowSelect: settings.allowSelect,
            bgFade: settings.bgFade,
            setSelect: settings.setSelect,
            minSize: settings.selMinSize,
            boxWidth: settings.maxWidthPreview,
            boxHeight: settings.maxHeightPreview,
            aspectRatio: settings.aspectRatio,
            onSelect: function (cords) {
                cords.pWidth = imgToCrop.naturalWidth / bounds.x;
                cords.pHeight = imgToCrop.naturalHeight / bounds.y;

                // Canvas qui servira à générer une base64 à enregistrer
                canvas = document.createElement("canvas");
                context = canvas.getContext('2d');

                canvas.width = (typeof settings.cropImageWidth !== 'undefined') ? settings.cropImageWidth : settings.cropImageHeight * settings.aspectRatio;
                canvas.height = (typeof settings.cropImageHeight !== 'undefined') ? settings.cropImageHeight : settings.cropImageWidth / settings.aspectRatio;
                context.drawImage(
                        imgToCrop,
                        Math.round(cords.x * cords.pWidth),
                        Math.round(cords.y * cords.pHeight),
                        Math.round(cords.w * cords.pWidth),
                        Math.round(cords.h * cords.pHeight),
                        0,
                        0,
                        Math.round(canvas.width),
                        Math.round(canvas.height)
                        );

                if (!isNaN(cords.x)) {
                    // On met à jour l'input qui contient la base64 à uploader et enregistrer
                    $(settings.hiddenFile).attr('value', function () {
                        return canvas.toDataURL("image/jpeg", settings.qualityJpeg);
                    });
                }
            }
        }, function () {
            // Use the API to get the real image size
            var boundsArr = this.getBounds();
            bounds.x = boundsArr[0];
            bounds.y = boundsArr[1];// Open modal preview
        });

        // Le Crop est prêt
        $(Ignis.Crop).trigger("crop:ready");
    }

    return {
        /**
         * Initialisation du Crop sur une image données
         * @param {Object} file : l'image à tronquer
         */
        read: function (file, type) {
            initialize(file, type);
        }
    };
}());