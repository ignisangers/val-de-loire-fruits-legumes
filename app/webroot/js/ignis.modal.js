/**
 * Module de gestion d'une modale
 * 
 * Dépendances :
 * - jQuery
 * 
 * Procédure :
 * - On fournit au module une URL à exécuter en AJAX pour récupérer le template de la modale souhaitée (create)
 * - À partir de là on peut l'afficher (open)
 * - Ou bien la fermer définitivement (close)
 * 
 * Note :
 * On ne peut pour l'instant n'utiliser qu'une modale à la fois !
 * 
 * @author Vincent <vincent@ignis.fr>
 */
var Ignis = Ignis || {};

Ignis.Modal = (function () {
    
    // VARIABLES
    var $body = $("body"),
        modalClass = ".modal-group",
        $modal;
    
    // MÉTHODES PRIVÉES
    /**
     * Récupération AJAX du template et création dans le DOM
     * @param {String} url
     * @param {Object} data
     */
    function getTemplate(url, data) {
        $.ajax({
            type: "GET",
            url: url,
            data: data,
            success: function (html) {
                $body.append(html);
                $modal = $(modalClass);
                // On a récupéré le template et ajouté au DOM : on indique que la modale est prête à être ouverte
                $(Ignis.Modal).trigger("modal:created");
            }
        });
    }
    
    // MÉTHODES PUBLIQUES
    return {
        /**
         * Récupération du template de la modale + ajout dans le DOM
         * @param {String} url : URL Cake qui traitera le template
         * @param {Object} data : données supplémentaire à transmettre en arguments
         */
        create: function (url, data) {
            data = (typeof data !== "undefined") ? data : {};
            getTemplate(url, data);
        },
        /**
         * Ouverture de la modale créée
         */
        open: function () {
            $modal.show();
        },
        /**
         * Fermeture de la modale par suppression dans le DOM
         */
        close: function () {
            $modal.remove();
        }
    };
    
}());