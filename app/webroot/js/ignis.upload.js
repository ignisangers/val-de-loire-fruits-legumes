/**
 * Module d'Upload de fichiers
 * 
 * Utilisation de FileReader en JS pour récupérer les fichiers uploadés
 * Compatible : IE10, Firefox 38, Chrome 31, Safari 8, Opera 31
 * 
 * Procédure :
 * - FileReader est-il supporté ?
 * - Récupération du ou des fichiers
 * - Préparation dans un tableau et retour du tableau
 * 
 * @author Vincent <vincent@ignis.fr>
 */
var Ignis = Ignis || {};

Ignis.Upload = (function () {
    'use strict';
    /*global jsVars, $*/
    
    // VARIABLES
    var files,
        reader,
        settings,
        defaults = {
            "maxFileSizeAllowed": 10 // Poids maximum autorisé (Mb)
        };
        
    // MÉTHODES PRIVÉES
    /**
     * Fonction maîtresse
     * @param {Object} input
     */
    function handleFiles(input) {        
        // On réinitialise la liste des fichiers
        files = {};
        
        if (uploadFiles(input) === false) {
            alert("Erreur : aucun fichier trouvé.");
            return;
        }
    }
    
    /**
     * Le navigateur supporte-t-il le File API ?
     * @returns {Boolean}
     */
    function initFileReader() {
        if (!window.FileReader) {
            return false;
        }        
    }
    
    /**
     * Réinitialisation de l'input file d'où viennent les fichiers
     * @param {Object} input
     */
    function clearFileInput(input) {
        input.value = null;

        if (input.value) {
            input.parentNode.replaceChild(input.cloneNode(true), input);
        }
    }
    
    function checkSize(size) {
        var bytesAllowed = (settings.maxFileSizeAllowed*1000000).toFixed(2);
        return (size <= bytesAllowed) ? true : false;
    }
    
    /**
     * Récupération des fichiers par le File API
     * @param {Object} input
     */
    function uploadFiles(input) {
        var i, f, d;
        
        if (!input.files) {
            return false; 
        }
        
        // Récupération des fichiers un par un
        for (i = 0; f = input.files[i]; i++) {
            reader = new FileReader();
            reader.onload = (function (f) {
                if (!checkSize(f.size)) {
                    alert("Un des fichiers sélectionné dépasse le poids maximum de " + settings.maxFileSizeAllowed + " Mo.");
                    return false;
                } else {
                    // Données du fichier
                    d = {
                        name: f.name, // Filename (ex. : xxxx.jpg)
                        size: f.size, // Son poids
                        type: f.type // Son type MIME
                    };
                    return function (e) {
                        d.raw = e.target.result; // On ajoute sa base64
                        files[Object.keys(files).length] = d;
                        
                        // Les fichiers sont maintenant disponibles
                        if (Object.keys(files).length === input.files.length) {                        
                            clearFileInput(input);
                            $(Ignis.Upload).trigger("upload:done");
                        }
                    };
                }
            }(f));
            reader.readAsDataURL(f);
        }
    }
    
    function initialize() {
        // On récupère la configuration
        if (typeof jsVars.cropConfig["default"] !== "undefined") {
            settings = $.extend({}, defaults, jsVars.cropConfig["default"]);
        } else {
            settings = defaults;
        }
        
        // Vérification support File API
        if (initFileReader() === false) {
            alert("Erreur : votre navigateur ne supporte pas la fonctionnalité \"File API\".");
            return;
        }
    }
    
    // On initialise le composant à son chargement
    initialize();
    
    // MÉTHODES PUBLIQUES
    return {
        /**
         * Récupération de fichiers à uploader
         * @param {Object} input : l'input file
         * @returns {Object} : la liste des fichiers récupérés par FileReader
         */
        read: function (input) {            
            handleFiles(input);
        },
        /**
         * Récupération d'un fichier précis de la liste
         * @param {Number} index
         * @returns {Object}
         */
        getUploadedFile: function (index) {
            return files[index];
        }
    };
}());